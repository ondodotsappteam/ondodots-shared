﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class GameHighScoreItem
    {
        public string Picture
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public int Balance
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
    }
}
