﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class ShakeItem
    {
        public int OndoId
        {
            get;
            set;
        }

        public int Role
        {
            get;
            set;
        }

        public int BenefitId
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public float Latitude
        {
            get;
            set;
        }

        public float longitude
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string MapIcon
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public string MiniPicture
        {
            get;
            set;
        }

        public string Stamp
        {
            get;
            set;
        }
    }
}
