﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class MapItem
    {
        public int OndoId
        {
            get;
            set;
        }
                
        public int Type
        {
            get;
            set;
        }

        public int NewPosts
        {
            get;
            set;
        }

        public int Role
        {
            get;
            set;
        }

        public int Distance
        {
            get;
            set;
        }

        public string Parent
        {
            get;
            set;
        }

        public string MapIcon
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int HasRelations
        {
            get;
            set;
        }

        public int ClusterCount
        {
            get;
            set;
        }

        public int UnClustered
        {
            get;
            set;
        }

        public int IsFavoriteInSingleFeed
        {
            get;
            set;
        }
    }
}
