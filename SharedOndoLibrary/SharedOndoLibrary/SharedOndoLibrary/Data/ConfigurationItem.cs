﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class ConfigurationItem
    {
        public int AppId
        {
            get;
            set;
        }

        public string AppName
        {
            get;
            set;
        }

        public int ActionByte
        {
            get;
            set;
        }

        public int StartUpView
        {
            get;
            set;
        }

        public string CheckSum
        {
            get;
            set;
        }

        public string AboutLink
        {
            get;
            set;
        }

        public string TermsLink
        {
            get;
            set;
        }

        public string PrivacyLink
        {
            get;
            set;
        }

        public string MapIconContact
        {
            get;
            set;
        }

        public string MapIconViewer
        {
            get;
            set;
        }

        public string MapIconMember
        {
            get;
            set;
        }

        public string MapIconOwner
        {
            get;
            set;
        }

        public string HeaderColor
        {
            get;
            set;
        }

        public string BarColor
        {
            get;
            set;
        }

        public int ServerType
        {
            get;
            set;
        }

    }
}
