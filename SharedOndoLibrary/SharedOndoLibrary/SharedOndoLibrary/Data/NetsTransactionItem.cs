﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class NetsTransactionItem
    {
        public string Amount
        {
            get;
            set;
        }

        public string Type
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int TransactionId
        {
            get;
            set;
        }

        public string DateTime
        {
            get;
            set;
        }
    }
}
