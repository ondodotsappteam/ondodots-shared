﻿using System;
using System.ComponentModel;
#if __ANDROID__
using Android.Util;
using Android.Graphics;
#endif

namespace SharedOndoLibrary.Data
{
    public class MyOndoList : INotifyPropertyChanged
    {
     
        private string ondoId;
        public string OndoId
        {
            get { return ondoId; }
            set
            {
                if (value != ondoId)
                {
                    ondoId = value;
                    NotifyPropertyChanged("OndoId");
                }
            }
        }

        private string checkSum;
        public string CheckSum
        {
            get { return checkSum; }
            set
            {
                if (value != checkSum)
                {
                    checkSum = value;
                    NotifyPropertyChanged("CheckSum");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
