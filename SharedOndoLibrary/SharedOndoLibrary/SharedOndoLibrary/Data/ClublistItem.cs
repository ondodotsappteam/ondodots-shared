﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class ClublistItem
    {
        public int ActivityId
        {
            get;
            set;
        }

        public int BenefitId
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public int Choice
        {
            get;
            set;
        }
    }
}
