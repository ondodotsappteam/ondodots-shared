﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class CardListItem
    {
        public string Title { get; set; }

        public string Stamp { get; set; }

        public int BenefitId { get; set; }
    }
}
