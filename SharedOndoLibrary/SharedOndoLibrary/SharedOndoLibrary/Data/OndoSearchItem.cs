﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class OndoSearchItem
    {
        public int OndoId
        {
            get;
            set;
        }

        public string SearchText
        {
            get;
            set;
        }

        public string PreText
        {
            get;
            set;
        }

        public string PostText
        {
            get;
            set;
        }

        public string HashSearchText
        {
            get;
            set;
        }

        public string HashPreText
        {
            get;
            set;
        }

        public string HashPostText
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public string MapIcon
        {
            get;
            set;
        }

        public int Role
        {
            get;
            set;
        }

        public int HasRelations
        {
            get;
            set;
        }

        public string Parent
        {
            get;
            set;
        }
    }
}
