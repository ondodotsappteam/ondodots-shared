﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class PostItem
    {
        public int ForeignID
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }
        
        public string Content
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public int Community
        {
            get;
            set;
        }

        public string StartDate
        {
            get;
            set;
        }

        public string EndDate
        {
            get;
            set;
        }
    }
}
