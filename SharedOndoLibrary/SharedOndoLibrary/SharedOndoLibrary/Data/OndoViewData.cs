﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
#if __ANDROID__
using Android.Util;
using Android.Graphics;
#endif

namespace SharedOndoLibrary.Data
{
#if!(WINDOWS_PHONE) && !(WINDOWS)
    public static class OndoViewList
    {
        static ObservableCollection<OndoViewData> Items { get; set; }

        static OndoViewList()
        {
            Items = new ObservableCollection<OndoViewData>();
        }

    }
 
    
    class OndoViewData
    {
    }
#endif
}
