﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
#if MONOTOUCH
using MonoTouch.UIKit;
#endif
#if __ANDROID__
using Android.Util;
using Android.Graphics;
#endif


namespace SharedOndoLibrary.Data
{
    public static class MyOndoViewList
    {
        public static ObservableCollection<MyOndoViewData> Items { get; set; }

        static MyOndoViewList()
        {
            Items = new ObservableCollection<MyOndoViewData>();
        }
    }


    public class MyOndoViewData
    {

        public string ImageUri
        {
            get;
            set;
        }

        public bool IsLotteryActive
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Notification
        {
            get;
            set;
        }

        public bool DisplayNotification
        {
            get
            {
                return !string.IsNullOrEmpty(this.Notification);
            }
        }

        public string Message
        {
            get;
            set;
        }

        public string GroupTag
        {
            get;
            set;
        }

        public string OndoType
        {
            get;
            set;
        }

        public int Role
        {
            get;
            set;
        }

        public string InfoByte
        {
            get;
            set;
        }
        public int NewPosts
        {
            get;
            set;
        }

        public string CheckSum
        {
            get;
            set;
        }

        public string MapIcon
        {
            get;
            set;
        }

        public int Distance
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public double Latitude
        {
            get;
            set;
        }
    }
}
