﻿using System;
using System.ComponentModel;


namespace SharedOndoLibrary.Data
{
    public class UserItemsData : INotifyPropertyChanged
    {
        private string userlogin;
        private int userid;
        private string userpassword;
        private string email;
         //private string devicehwid;
        private int primarysocialnetwork;
        //private int secondarysocialnetwork;
        private int birthday;
        private int gender;
        private string network;
        private string phone;
        private bool isauthenticated;
        private string firstname;
        private string lastname;
        private string bio;
        private string name;
        private string signature;
        private string comment;
        private string longitude;
        private string latitude;
        private string username;
        private string userpicture1;
        private bool pushEnabled;
        private int informationLevel;
        private string pushUri;
        private bool isWelcomeScreendisabled;
        private string serviceUri;
        private bool gameSoundDisabled;
        private string appVersion;
        private string culture;
        private string language;

        public string AccessToken
        {
            get;
            set;
        }

        public bool GameSoundDisabled
        {
            get { return gameSoundDisabled; }
            set
            {
                gameSoundDisabled = value;
                NotifyPropertyChanged("GameSoundDisabled");
            }
        }
        
        public string Culture
        {
            get { return culture; }
            set
            {
                culture = value;
                NotifyPropertyChanged("Culture");
            }
        }
        
        public string ServiceUri
        {
            get { return serviceUri; }
            set
            {
                serviceUri = value;
                NotifyPropertyChanged("ServiceUri");
            }
        }

        public bool IsWelcomeScreenDisabled
        {
            get { return isWelcomeScreendisabled; }
            set
            {
                isWelcomeScreendisabled = value;
                NotifyPropertyChanged("IsWelcomeScreenDisabled");
            }
        }

        public int InformationLevel
        {
            get { return informationLevel; }
            set
            {
                informationLevel = value;
                NotifyPropertyChanged("InformationLevel");
            }
        }

        public bool PushEnabled
        {
            get { return pushEnabled; }
            set
            {
                pushEnabled = value;
                NotifyPropertyChanged("PushEnabled");
            }
        }

        public int UserId
        {
            get { return userid; }
            set
            {
                userid = value;
                NotifyPropertyChanged("UserId");
            }
        }

        public string PushUri
        {
            get { return pushUri; }
            set
            {
                pushUri = value;
                NotifyPropertyChanged("PushUri");
            }
        }

        public string UserLogin
        {
            get { return userlogin; }
            set
            {
                userlogin = value;
                NotifyPropertyChanged("UserLogin");
            }
        }

        //public string UserDeviceHWID
        //{
        //    get { return devicehwid; }
        //    set
        //    {
        //        devicehwid = value;
        //        NotifyPropertyChanged("UserDeviceHWID");
        //    }
        //}

        public string UserPassword
        {
            get { return userpassword; }
            set
            {
                userpassword = value;
                NotifyPropertyChanged("UserPassword");
            }
        }


        public string useremail
        {
            get { return email; }
            set
            {
                email = value;
                NotifyPropertyChanged("useremail");
            }
        }

        public int userprimarysocialnetwork
        {
            get { return primarysocialnetwork; }
            set
            {
                primarysocialnetwork = value;
                NotifyPropertyChanged("userprimarysocialnetwork");
            }
        }

        public int userbirthday
        {
            get { return birthday; }
            set
            {
                birthday = value;
                NotifyPropertyChanged("userbirthday");
            }
        }


        public int usergender
        {
            get { return gender; }
            set
            {
                gender = value;
                NotifyPropertyChanged("usergender");
            }
        }

        public string userphone
        {
            get { return phone; }
            set
            {
                phone = value;
                NotifyPropertyChanged("userphone");
            }
        }

        public bool userisauthenticated
        {
            get { return isauthenticated; }
            set
            {
                isauthenticated = value;
                NotifyPropertyChanged("userisauthenticated");
            }
        }

        public string userpicture
        {
            get { return userpicture1; }
            set
            {
                userpicture1 = value;
                NotifyPropertyChanged("userpicture");
            }
        }


        public string usernetwork
        {
            get { return network; }
            set
            {
                network = value;
                NotifyPropertyChanged("usernetwork");
            }
        }

        public string Logitude
        {
            get { return longitude; }
            set
            {
                longitude = value;
                NotifyPropertyChanged("Longitude");
            }
        }

        public string Latitude
        {
            get { return latitude; }
            set
            {
                latitude = value;
                NotifyPropertyChanged("Latitude");
            }
        }

        /*public BitmapImage Image
        {
            get { return image; }
            set
            {
                image = value;
                NotifyPropertyChanged("Image");
            }
        }*/

        public string FirstName
        {
            get { return firstname; }
            set
            {
                firstname = value;
                NotifyPropertyChanged("FirstName");
            }
        }

        public string LastName
        {
            get { return lastname; }
            set
            {
                lastname = value;
                NotifyPropertyChanged("LastName");
            }
        }

        public string UserBio
        {
            get { return bio; }
            set
            {
                bio = value;
                NotifyPropertyChanged("UserBio");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }

        public string Comment
        {
            get { return comment; }
            set
            {
                comment = value;
                NotifyPropertyChanged("Comment");
            }
        }

        public string Signature
        {
            get { return signature; }
            set
            {
                signature = value;
                NotifyPropertyChanged("Signature");
            }
        }

        public string Username
        {
            get { return username; }
            set
            {
                username = value;
                NotifyPropertyChanged("Username");
            }
        }

        public string AppVersion
        {
            get { return appVersion; }
            set
            {
                appVersion = value;
                NotifyPropertyChanged("AppVersion");
            }
        }

        public string Language
        {
            get { return language; }
            set
            {
                language = value;
                NotifyPropertyChanged("Language");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

}
