﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class PaymentValidationItem
    {
        public int ErrorCode
        {
            get;
            set;
        }

        public int Result
        {
            get;
            set;
        }

        public int OrderId
        {
            get;
            set;
        }

        public string ResultString
        {
            get;
            set;
        }
    }
}
