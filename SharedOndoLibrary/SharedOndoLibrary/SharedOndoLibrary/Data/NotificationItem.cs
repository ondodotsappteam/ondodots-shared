﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class NotificationItem
    {
        public int OndoId
        {
            get;
            set;
        }

        public int NewNotifications
        {
            get;
            set;
        }

        public string CheckSum
        {
            get;
            set;
        }

        public int Total
        {
            get;
            set;
        }

        public int TotalBenefit
        {
            get;
            set;
        }

        public int FavoriteOndo
        {
            get;
            set;
        }

        public int TotalFavorites
        {
            get;
            set;
        }

        public int FavoriteType
        {
            get;
            set;
        }
    }
}
