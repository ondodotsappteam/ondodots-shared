﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class ActivityItem
    {
        public int CanPlay
        {
            get;
            set;
        }

        public int BenefitType
        {
            get;
            set;
        }

        public int Index
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int BenefitHeight
        {
            get;
            set;
        }

        public int BenefitWidth
        {
            get;
            set;
        }

        public int ContentHeight
        {
            get;
            set;
        }

        public int ContentWidth
        {
            get;
            set;
        }

        public int BenefitId
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public string BenefitTitle
        {
            get;
            set;
        }

        public string BenefitPicture
        {
            get;
            set;
        }

        public string CheckSum
        {
            get;
            set;
        }

        public string MiniPicture
        {
            get;
            set;
        }

        public int CommentId
        {
            get;
            set;
        }

        public string Time
        {
            get;
            set;
        }

        public int Visibility
        {
            get;
            set;
        }

        public int Community
        {
            get;
            set;
        }

        public int Voting
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public int RecordType
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public string GeoTag
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

      
        public string UserName
        {
            get;
            set;
        }

        public int NumberOfComments
        {
            get;
            set;
        }

        public int NumberOfNewComments
        {
            get;
            set;
        }

        public string UltimatePicture
        {
            get;
            set;
        }

        public string UltimateLink
        {
            get;
            set;
        }

        public string CheckSumNode
        {
            get;
            set;
        }

        public string NormalPrice
        {
            get;
            set;
        }

        public string OfferPrice
        {
            get;
            set;
        }

        public string Bonus
        {
            get;
            set;
        }

        public string OndoTitle
        {
            get;
            set;
        }

        public int OndoRef
        {
            get;
            set;
        }

        public string OndoMiniPicture
        {
            get;
            set;
        }
    }
}
