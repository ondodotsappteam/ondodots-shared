﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class OndoItem
    {

        public string UltimateLink
        {
            get;
            set;
        }
        
        public string UltimatePicture
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public string MapIcon
        {
            get;
            set;
        }

        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public int Distance
        {
            get;
            set;
        }

        public int ParentType
        {
            get;
            set;
        }

        public string CheckSum
        {
            get;
            set;
        }

        public string CheckSumNode
        {
            get;
            set;
        }

        public int UserIsValid
        {
            get;
            set;
        }

        public int NewPosts
        {
            get;
            set;
        }

        public int ContentHeight
        {
            get;
            set;
        }

        public int ContentWidth
        {
            get;
            set;
        }

        public int Role
        {
            get;
            set;
        }

        public int InfoByte
        {
            get;
            set;
        }

        public int TagAction
        {
            get;
            set;
        }

        public int ParAction
        {
            get;
            set;
        }

        public int Splash
        {
            get;
            set;
        }

        public int OndoError
        {
            get;
            set;
        }

        public int OndoId
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string ContactInfo
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public string CreateTime
        {
            get;
            set;
        }

        public int Visibility
        {
            get;
            set;
        }

        public int InitiatorHandovers
        {
            get;
            set;
        }

        public int ParticipantHandovers
        {
            get;
            set;
        }

        public int ReceiveTimes
        {
            get;
            set;
        }

        public int Initiator
        {
            get;
            set;
        }

        public int Status
        {
            get;
            set;
        }

        public int Community
        {
            get;
            set;
        }

        public string Locality
        {
            get;
            set;
        }

        public string Country
        {
            get;
            set;
        }

        public string StatusMessage
        {
            get;
            set;
        }

        public string MiniPicture
        {
            get;
            set;
        }

        public int VotingScale
        {
            get;
            set;
        }

        public string BarcodeFormat
        {
            get;
            set;
        }

        public string Barcode
        {
            get;
            set;
        }

        public string GeoTag
        {
            get;
            set;
        }

        public string Device
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public string ContentType
        {
            get;
            set;
        }

        public string Link
        {
            get;
            set;
        }

        public int NoOfComments
        {
            get;
            set;
        }

        public int NoOfJoins
        {
            get;
            set;
        }

        public string QRString
        {
            get;
            set;
        }

        public string QRUrl
        {
            get;
            set;
        }

        public string OpeningHoursText
        {
            get;
            set;
        }

        public string OpeningHoursHeader
        {
            get;
            set;
        }

        public string Sponsor
        {
            get;
            set;
        }

        public string ProfilePicture
        {
            get;
            set;
        }

        public string ProfileName
        {
            get;
            set;
        }

        public int RemoteSignup
        {
            get;
            set;
        }

        public int LotteryStatus
        {
            get;
            set;
        }

        public int HasBenefits
        {
            get;
            set;
        }

        public int IsFavoriteInSingleFeed
        {
            get;
            set;
        }
    }
}
