﻿namespace SharedOndoLibrary.Data
{
    public class ViewSettingsData
    {
        private string pin;
        private string own;
        private string participate;

        public string Pin
        {
            get { return pin; }
            set { pin = value; }
        }

        public string Own
        {
            get { return own; }
            set { own = value; }
        }

        public string Participate
        {
            get { return participate; }
            set { participate = value; }
        }
           
    }
}
