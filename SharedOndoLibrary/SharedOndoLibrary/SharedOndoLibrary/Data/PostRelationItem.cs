﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class PostRelationItem
    {
        /*<postcommentrelation xmlns="">
    <relation>
      <CommentId>13778</CommentId>
      <NumberOfComments>0</NumberOfComments>
      <NumberOfNewComments>0</NumberOfNewComments>
    </relation>*/

        public int CommentId
        {
            get;
            set;
        }

        public int NumberOfComments
        {
            get;
            set;
        }

        public int NumberOfNewComments
        {
            get;
            set;
        }

    }
}
