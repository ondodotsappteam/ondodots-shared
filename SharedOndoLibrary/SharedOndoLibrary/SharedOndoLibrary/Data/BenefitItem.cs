﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if(WINDOWS_PHONE)
using System.Windows.Media.Imaging;
#endif

namespace SharedOndoLibrary.Data
{
    public class BenefitItem
    {
        public int CanPlay
        {
            get;
            set;
        }

        public int ActionByte
        {
            get;
            set;
        }

        public int CanEnterCode
        {
            get;
            set;
        }

        public int HasLocations
        {
            get;
            set;
        }

        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public int BarcodeLength
        {
            get;
            set;
        }

        public int BenefitRole
        {
            get;
            set;
        }

        public int Distance
        {
            get;
            set;
        }

        public int LotteryStatus
        {
            get;
            set;
        }

        public int OnlyImage
        {
            get;
            set;
        }

        public int PriceId
        {
            get;
            set;
        }

        public int Community
        {
            get;
            set;
        }

        public int Chances
        {
            get;
            set;
        }

        public int Curfew
        {
            get;
            set;
        }

        public int Items
        {
            get;
            set;
        }

        public int LastScore
        {
            get;
            set;
        }

        public int Balance
        {
            get;
            set;
        }

       
        public int RemainTurns
        {
            get;
            set;
        }

        public int GameStatus
        {
            get;
            set;
        }

        public int Tickets
        {
            get;
            set;
        }

        public string NextGame
        {
            get;
            set;
        }

        public string WinnerChance
        {
            get;
            set;
        }

        public int Participants
        {
            get;
            set;
        }

        public string RemainingTime
        {
            get;
            set;
        }

        public string UserPicture
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public int BenefitId
        {
            get;
            set;
        }

        public string Start
        {
            get;
            set;
        }

        public string End
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public string Conditions
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public string WinnerText
        {
            get;
            set;
        }

        public int NoOfWinners
        {
            get;
            set;
        }

        public int Status
        {
            get;
            set;
        }

        public string AccountId
        {
            get;
            set;
        }

        public string Stamp
        {
            get;
            set;
        }

        public string NewsLetterPicture
        {
            get;
            set;
        }

        public int LotteryNextDraw
        {
            get;
            set;
        }

        public int TotalTickets
        {
            get;
            set;
        }

        public int UserTickets
        {
            get;
            set;
        }

        public int PicWidth
        {
            get;
            set;
        }

        public int PicHeight
        {
            get;
            set;
        }

        public int CategoryByte
        {
            get;
            set;
        }

        public string StampBackground
        {
            get;
            set;
        }

        public string MiniPicture
        {
            get;
            set;
        }

        public string OriginalPicture
        {
            get;
            set;
        }

        public string SocialMediaString
        {
            get;
            set;
        }

        public string UltimateLink
        {
            get;
            set;
        }

        public string UltimatePicture
        {
            get;
            set;
        }

        public string TicketsText
        {
            get;
            set;
        }

        public string NormalPrice
        {
            get;
            set;
        }

        public string OfferPrice
        {
            get;
            set;
        }

        public string Bonus
        {
            get;
            set;
        }

        public string OndoTitle
        {
            get;
            set;
        }
    }
}
