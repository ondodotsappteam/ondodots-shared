﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class BenefitProgramTransactionItem
    {
        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public string ResultString
        {
            get;
            set;
        }

        public int Result
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public int BenefitId
        {
            get;
            set;
        }

        public int Balance
        {
            get;
            set;
        }

        public int Sheets
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public string Conditions
        {
            get;
            set;
        }

        public string BarCodeString
        {
            get;
            set;
        }

        public string Stamp
        {
            get;
            set;
        }

        public string StampRedeem
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public string BarCodePicture
        {
            get;
            set;
        }

        public int Height
        {
            get;
            set;
        }

        public int Width
        {
            get;
            set;
        }

        public string UserPicture
        {
            get;
            set;
        }

        public string BenefitPicture
        {
            get;
            set;
        }

        public int BenefitHeight
        {
            get;
            set;
        }

        public int BenefitWidth
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string Time
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public int BenefitType
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int HasLocations
        {
            get;
            set;
        }
    }
}
