﻿namespace SharedOndoLibrary.Data
{
    public class AppSettings
    {
        private string splashscreen;
        private string gps;
        private string visibility;
        private string pushnotifications;
        private string animations;
        private string sounds;
        private string share;
        private string play;
        private string searchid;
        private string debug;
        private string votingScale;
        private string votingPeriod;
        private string voting;
        private string backgroundImage;
        private string ondocommunity;
		private string isLowMemDevice;

        public string IsLowMemDevice
        {
            get { return isLowMemDevice; }
            set { isLowMemDevice = value; }
        }

        public string OndoCommunity
        {
            get { return ondocommunity; }
            set { ondocommunity = value; }
        }

        public string Visibility
        {
            get { return visibility; }
            set { visibility = value; }
        }

        public string BackgroundImage
        {
            get { return backgroundImage; }
            set { backgroundImage = value; }
        }

        public string Debug
        {
            get { return debug; }
            set { debug = value; }
        }

        public string SearchID
        {
            get { return searchid; }
            set { searchid = value; }
        }

        public string Share
        {
            get { return share; }
            set { share = value; }
        }

        public string Play
        {
            get { return play; }
            set { play = value; }
        }

        public string Sounds
        {
            get { return sounds; }
            set { sounds = value; }
        }

        public string SplashScreen
        {
            get { return splashscreen; }
            set { splashscreen = value; }
        }

        public string Gps
        {
            get { return gps; }
            set { gps = value; }
        }

        public string PushNotifications
        {
            get { return pushnotifications; }
            set { pushnotifications = value; }
        }

        public string Animations
        {
            get { return animations; }
            set { animations = value; }
        }

        public string VotingScale
        {
            get { return votingScale; }
            set { votingScale = value; }
        }


        public string VotingPeriod
        {
            get { return votingPeriod; }
            set { votingPeriod = value; }
        }

        public string Voting
        {
            get { return voting; }
            set { voting = value; }
        }

      

     }
}
