﻿using System;
using System.ComponentModel;

namespace SharedOndoLibrary.Data
{
    public class OndoData : INotifyPropertyChanged
    {
        private string ondoId;
        private string ondoTitle;
        private string ondoDescription;//new
        private string ondoShortDescription;//new
        private string ondoContentType;//new
        private string ondoContent;
        private string ondoType;
        private string ondoSubType;
        private string ondoCreateTime;
        private string ondoVisibility;
        private string ondoInitiatorHandovers;
        private string ondoParticipantHandovers;
        private string ondoReceiveTimes;
        private string ondoInitiator;
        private string ondoStatus;
        private string ondoStatusMessage;
        private bool isDataLoading;
        private string initiatorComment;
        private string initiatorUserName;
        private string initiatorPicture;
        private string ondoMiniPicture;
        private bool hasContent;
        private string ondoVotingScale;
        private string ondoVotingPeriod;
        private string numberOfComments;
        private string numberOfJoins;
        private string lotteryId;
        private string checkSum;
		private string ondocommunity;
        private bool isDeleted;
        private bool isDayLight;
        private string numberOfHits;
        private string link;
        private int tagAction;
        private int parAction;
        private int splash;
        private int infoByte;
        private int role;
        private string contactInfo;
        private string newPosts;
        private string latitude;
        private string longitude;
        private int distance;
        private string mapIcon;
        private string phone;
        private string address;
        private string qrstring;
        private string qrurl;
        private string openinghours;
        private string openinghoursheader;
        private string sponsor;
        private string profilename;
        private string profilepicture;
        private int remotesignup;
        private int lotteryStatus;
        private int isFavoriteInSingleFeed;
      
        public string OpeningHours
        {
            get { return openinghoursheader; }
            set
            {
                if (value != openinghoursheader)
                {
                    openinghoursheader = value;
                    NotifyPropertyChanged("OpeningHoursHeader");
                }
            }
        }

        public string OpeningHoursHeader
        {
            get { return openinghours; }
            set
            {
                if (value != openinghours)
                {
                    openinghours = value;
                    NotifyPropertyChanged("OpeningHoursHeader");
                }
            }
        }

        public string QRString
        {
            get { return qrstring; }
            set
            {
                if (value != qrstring)
                {
                    qrstring = value;
                    NotifyPropertyChanged("QRString");
                }
            }
        }

        public string QRUrl
        {
            get { return qrurl; }
            set
            {
                if (value != qrurl)
                {
                    qrurl = value;
                    NotifyPropertyChanged("QRUrl");
                }
            }
        }

        public string MapIcon
        {
            get { return mapIcon; }
            set
            {
                if (value != mapIcon)
                {
                    mapIcon = value;
                    NotifyPropertyChanged("MapIcon");
                }
            }
        }

        public int Distance
        {
            get { return distance; }
            set
            {
                if (value != distance)
                {
                    distance = value;
                    NotifyPropertyChanged("Distance");
                }
            }
        }

        public int Splash
        {
            get { return splash; }
            set
            {
                if (value != splash)
                {
                    splash = value;
                    NotifyPropertyChanged("Splash");
                }
            }
        }

        public int ParAction
        {
            get { return parAction; }
            set
            {
                if (value != parAction)
                {
                    parAction = value;
                    NotifyPropertyChanged("ParAction");
                }
            }
        }

        public int TagAction
        {
            get { return tagAction; }
            set
            {
                if (value != tagAction)
                {
                    tagAction = value;
                    NotifyPropertyChanged("TagAction");
                }
            }
        }

        public string Link
        {
            get { return link; }
            set
            {
                if (value != link)
                {
                    link = value;
                    NotifyPropertyChanged("Link");
                }
            }
        }

        public string NumberOfHits
        {
            get { return numberOfHits; }
            set
            {
                if (value != numberOfHits)
                {
                    numberOfHits = value;
                    NotifyPropertyChanged("NumberOfHits");
                }
            }
        }

        public bool IsDayLight
        {
            get { return isDayLight; }
            set
            {
                if (value != isDayLight)
                {
                    isDayLight = value;
                    NotifyPropertyChanged("IsDayLight");
                }
            }
        }

        public bool IsDeleted
        {
            get { return isDeleted; }
            set
            {
                if (value != isDeleted)
                {
                    isDeleted = value;
                    NotifyPropertyChanged("IsDeleted");
                }
            }
        }

        public string OndoCommunity
        {
            get { return ondocommunity; }
            set
            {
                if (value != ondocommunity)
                {
                    ondocommunity = value;
                    NotifyPropertyChanged("OndoCommunity");
                }
            }
        }


        public string CheckSum
        {
            get { return checkSum; }
            set
            {
                if (value != checkSum)
                {
                    checkSum = value;
                    NotifyPropertyChanged("CheckSum");
                }
            }
        }

        public string NumberOfJoins
        {
            get { return numberOfJoins; }
            set
            {
                if (value != numberOfJoins)
                {
                    numberOfJoins = value;
                    NotifyPropertyChanged("NumberOfJoins");
                }
            }
        }

        public string NewPosts
        {
            get { return newPosts; }
            set
            {
                if (value != newPosts)
                {
                    newPosts = value;
                    NotifyPropertyChanged("NewPosts");
                }
            }
        }

        public string NumberOfComments
        {
            get { return numberOfComments; }
            set
            {
                if (value != numberOfComments)
                {
                    numberOfComments = value;
                    NotifyPropertyChanged("NumberOfComments");
                }
            }
        }

        public string OndoMiniPicture
        {
            get { return ondoMiniPicture; }
            set
            {
                if (value != ondoMiniPicture)
                {
                    ondoMiniPicture = value;
                    NotifyPropertyChanged("OndoMiniPicture");
                }
            }
        }

        public string InitiatorPicture
        {
            get { return initiatorPicture; }
            set
            {
                if (value != initiatorPicture)
                {
                    initiatorPicture = value;
                    NotifyPropertyChanged("InitiatorPicture");
                }
            }
        }

        public string InitiatorUserName
        {
            get { return initiatorUserName; }
            set
            {
                if (value != initiatorUserName)
                {
                    initiatorUserName = value;
                    NotifyPropertyChanged("InitiatorUserName");
                }
            }
        }

        public string InitiatorComment
        {
            get { return initiatorComment; }
            set
            {
                if (value != initiatorComment)
                {
                    initiatorComment = value;
                    NotifyPropertyChanged("InitiatorComment");
                }
            }
        }

        public bool IsDataLoading
        {
            get { return isDataLoading; }
            set
            {
                if (value != isDataLoading)
                {
                    isDataLoading = value;
                    NotifyPropertyChanged("IsDataLoading");
                }
            }
        }

        public bool HasContent
        {
            get { return hasContent; }
            set
            {
                if (value != hasContent)
                {
                    hasContent = value;
                    NotifyPropertyChanged("HasContent");
                }
            }
        }

    
        public string OndoVotingScale
        {
            get { return ondoVotingScale; }
            set
            {
                if (value != ondoVotingScale)
                {
                    ondoVotingScale = value;
                    NotifyPropertyChanged("OndoVotingScale");
                }
            }
        }

        public string OndoVotingPeriod
        {
            get { return ondoVotingPeriod; }
            set
            {
                if (value != ondoVotingPeriod)
                {
                    ondoVotingPeriod = value;
                    NotifyPropertyChanged("OndoVotingPeriod");
                }
            }
        }

        public string LotteryId
        {
            get { return lotteryId; }
            set
            {
                if (value != lotteryId)
                {
                    lotteryId = value;
                    NotifyPropertyChanged("LotteryId");
                }
            }
        }

        public string OndoId
        {
            get { return ondoId; }
            set
            {
                if (value != ondoId)
                {
                    ondoId = value;
                    NotifyPropertyChanged("OndoId");
                }
            }
        }

        public string Status
        {
            get { return ondoStatus; }
            set
            {
                if (value != ondoStatus)
                {
                    ondoStatus = value;
                    NotifyPropertyChanged("Status");
                }
            }
        }

        public string OndoTitle
        {
            get { return ondoTitle; }
            set
            {
                if (value != ondoTitle)
                {
                    ondoTitle = value;
                    NotifyPropertyChanged("OndoTitle");
                }
            }
        }

        public string OndoDescription
        {
            get { return ondoDescription; }
            set
            {
                if (value != ondoDescription)
                {
                    ondoDescription = value;
                    NotifyPropertyChanged("OndoDescription");
                }
            }
        }

        public string OndoShortDescription
        {
            get { return ondoShortDescription; }
            set
            {
                if (value != ondoShortDescription)
                {
                    ondoShortDescription = value;
                    NotifyPropertyChanged("OndoShortDescription");
                }
            }
        }

        public string OndoContentType
        {
            get { return ondoContentType; }
            set
            {
                if (value != ondoContentType)
                {
                    ondoContentType = value;
                    NotifyPropertyChanged("OndoContenType");
                }
            }
        }
        public string OndoContent
        {
            get { return ondoContent; }
            set
            {
                //if (value != ondoContent)
                {
                    ondoContent = value;
                    NotifyPropertyChanged("OndoContent");
                }
            }
        }

        public string OndoType
        {
            get { return ondoType; }
            set
            {
                if (value != ondoType)
                {
                    ondoType = value;
                    NotifyPropertyChanged("OndoType");
                }
            }
        }

        public string OndoSubType
        {
            get { return ondoSubType; }
            set
            {
                if (value != ondoSubType)
                {
                    ondoSubType = value;
                    NotifyPropertyChanged("OndoSubType");
                }
            }
        }

        public string OndoCreateTime
        {
            get { return ondoCreateTime; }
            set
            {
                if (value != ondoCreateTime)
                {
                    ondoCreateTime = value;
                    NotifyPropertyChanged("OndoCreateTime");
                }
            }
        }

        public string OndoVisibility
        {
            get { return ondoVisibility; }
            set
            {
                if (value != ondoVisibility)
                {
                    ondoVisibility = value;
                    NotifyPropertyChanged("OndoVisibility");
                }
            }
        }

        public string OndoInitiatorHandovers
        {
            get { return ondoInitiatorHandovers; }
            set
            {
                if (value != ondoInitiatorHandovers)
                {
                    ondoInitiatorHandovers = value;
                    NotifyPropertyChanged("OndoInitiatorHandovers");
                }
            }
        }

        public string OndoParticipantHandovers
        {
            get { return ondoParticipantHandovers; }
            set
            {
                if (value != ondoParticipantHandovers)
                {
                    ondoParticipantHandovers = value;
                    NotifyPropertyChanged("OndoParticipantHandovers");
                }
            }
        }

        public string OndoReceiveTimes
        {
            get { return ondoReceiveTimes; }
            set
            {
                if (value != ondoReceiveTimes)
                {
                    ondoReceiveTimes = value;
                    NotifyPropertyChanged("OndoReceiveTimes");
                }
            }
        }

        public string OndoInitiator
        {
            get { return ondoInitiator; }
            set
            {
                if (value != ondoInitiator)
                {
                    ondoInitiator = value;
                    NotifyPropertyChanged("OndoInitiator");
                }
            }
        }

        public string OndoStatus
        {
            get { return ondoStatus; }
            set
            {
                if (value != ondoStatus)
                {
                    ondoStatus = value;
                    NotifyPropertyChanged("OndoStatus");
                }
            }
        }

        public string OndoStatusMessage
        {
            get { return ondoStatusMessage; }
            set
            {
                if (value != ondoStatusMessage)
                {
                    ondoStatusMessage = value;
                    NotifyPropertyChanged("OndoStatusMessage");
                }
            }
        }

       
        public int InfoByte
        {
            get { return infoByte; }
            set
            {
                if (value != infoByte)
                {
                    infoByte = value;
                    NotifyPropertyChanged("InfoByte");
                }
            }
        }

        public int Role
        {
            get { return role; }
            set
            {
                if (value != role)
                {
                    role = value;
                    NotifyPropertyChanged("Role");
                }
            }
        }

        public string ContactInfo
        {
            get { return contactInfo; }
            set
            {
                if (value != contactInfo)
                {
                    contactInfo = value;
                    NotifyPropertyChanged("ContactInfo");
                }
            }
        }

        public string Phone
        {
            get { return phone; }
            set
            {
                if (value != phone)
                {
                    phone = value;
                    NotifyPropertyChanged("Phone");
                }
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                if (value != address)
                {
                    address = value;
                    NotifyPropertyChanged("Address");
                }
            }
        }

        public string Longitude
        {
            get { return longitude; }
            set
            {
                if (value != longitude)
                {
                    longitude = value;
                    NotifyPropertyChanged("Longitude");
                }
            }
        }

        public string Latitude
        {
            get { return latitude; }
            set
            {
                if (value != latitude)
                {
                    latitude = value;
                    NotifyPropertyChanged("Latitude");
                }
            }
        }

        public string Sponsor
        {
            get { return sponsor; }
            set
            {
                if (value != sponsor)
                {
                    sponsor = value;
                    NotifyPropertyChanged("Sponsor");
                }
            }
        }

        public string ProfilePicture
        {
            get { return profilepicture; }
            set
            {
                if (value != profilepicture)
                {
                    profilepicture = value;
                    NotifyPropertyChanged("ProfilePicture");
                }
            }
        }
        public string ProfileName
        {
            get { return profilename; }
            set
            {
                if (value != profilename)
                {
                    profilename = value;
                    NotifyPropertyChanged("ProfileName");
                }
            }
        }

        public int RemoteSignup
        {
            get { return remotesignup; }
            set
            {
                if (value != remotesignup)
                {
                    remotesignup = value;
                    NotifyPropertyChanged("RemoteSignup");
                }
            }
        }

        public int LotteryStatus
        {
            get { return lotteryStatus; }
            set
            {
                if (value != lotteryStatus)
                {
                    lotteryStatus = value;
                    NotifyPropertyChanged("LotteryStatus");
                }
            }
        }


        public int IsFavoriteInSingleFeed
        {
            get { return isFavoriteInSingleFeed; }
            set
            {
                if (value != isFavoriteInSingleFeed)
                {
                    isFavoriteInSingleFeed = value;
                    NotifyPropertyChanged("IsFavoriteInSingleFeed");
                }
            }
        }
        
       

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

       

       

    }

}
