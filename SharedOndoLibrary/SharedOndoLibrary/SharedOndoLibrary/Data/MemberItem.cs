﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class MemberItem
    {
        public string Picture
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public int UserId
        {
            get;
            set;
        }

        public int Role
        {
            get;
            set;
        }
    }
}
