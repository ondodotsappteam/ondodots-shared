﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class WalletItem
    {
        public string BalanceText
        {
            get;
            set;
        }

        public string Cardnr
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public int CanDelete
        {
            get;
            set;
        }

        public int CanEnterCode
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public string OndoTitle
        {
            get;
            set;
        }

        public int OndoId
        {
            get;
            set;
        }

        public int Role
        {
            get;
            set;
        }

        public int BenefitType
        {
            get;
            set;
        }

        public int BenefitId
        {
            get;
            set;
        }

        public int Balance
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public string CheckSumNode
        {
            get;
            set;
        }

        public int Index
        {
            get;
            set;
        }

        public int NewPosts
        {
            get;
            set;
        }
    }
}
