﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class StartGameIResultItem
    {
        public int Result { get; set; }

        public string ResultString { get; set; }

        public string MerchantId { get; set; }

        public string MiniPicture { get; set; }

        public string ReceiptMessage { get; set; }

        public int OrderId { get; set; }

        public string Title { get; set; }

        public double Amount { get; set; }
    }
}
