﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class OndoPaymentItem
    {
        public string MerchantId { get; set; }

        public string TransactionId { get; set; }

        public int OrderId { get; set; }

        public string Amount { get; set; }

        public string MinimumAmount { get; set; }

        public int ErrorCode { get; set; }

        public int OndoId { get; set; }

        public int IsSynchronized { get; set; }

        public string ReceiptMessage { get; set; }

        public string DateTime { get; set; }

        public int UserId { get; set; }

        public int Type { get; set; }

        public string Title { get; set; }

        public string Picture { get; set; }
    }
}
