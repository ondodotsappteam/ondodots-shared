﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class UserTransactionItem
    {
        public string UnitText
        {
            get;
            set;
        }

        public int UnitType
        {
            get;
            set;
        }

        public int Points
        {
            get;
            set;
        }

        public int OndoId
        {
            get;
            set;
        }

        public int Type
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string DateTime
        {
            get;
            set;
        }

        public int Balance
        {
            get;
            set;
        }

        public int Role
        {
            get;
            set;
        }
    }
}
