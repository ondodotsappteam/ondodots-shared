﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class VerifyUserItem
    {
        public int Result
        {
            get;
            set;
        }

        public string ResultString
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }
    }
}
