﻿using System;
using System.ComponentModel;

namespace SharedOndoLibrary.Data
{
    public class OndoExchangeData : INotifyPropertyChanged
    {
        string exchangeId;
        string ondoId;
        string fromUserId;
        string toUserId;
        string latitude;
        string longitude;
        string altitude;
        string picture;
        string exchangeTime;
        string anonymous;
        string guid;
        string userlogin;
        string userComment;
        string userVisibility;
        string contentid;
        bool isThisInitiator;
        string myPicture;
        string voting;
        string content;
        string foreignId;
        string foreignIdType;
        string recordType;
        bool hasContent;
		bool isDayLight;
        string numberOfComments;
        private string checkSum;
        private string numberOfParticipants;
        private string geoposition;
        private string initJoin;
		public bool IsDayLight
        {
            get { return isDayLight; }
            set
            {
                if (value != isDayLight)
                {
                    isDayLight = value;
                    NotifyPropertyChanged("IsDayLight");
                }
            }
        }

        public string InitJoin
        {
            get { return initJoin; }
            set
            {
                if (value != initJoin)
                {
                    initJoin = value;
                    NotifyPropertyChanged("InitJoin");
                }
            }
        }

        public string NumberOfParticipantsData
        {
            get { return numberOfParticipants; }
            set
            {
                if (value != numberOfParticipants)
                {
                    numberOfParticipants = value;
                    NotifyPropertyChanged("NumberOfParticipants");
                }
            }
        }

        public string CheckSum
        {
            get { return checkSum; }
            set
            {
                if (value != checkSum)
                {
                    checkSum = value;
                    NotifyPropertyChanged("CheckSum");
                }
            }
        }

        public string NumberOfComments
        {
            get { return numberOfComments; }
            set
            {
                if (value != numberOfComments)
                {
                    numberOfComments = value;
                    NotifyPropertyChanged("NumberOfComments");
                }
            }
        }

        public string RecordType
        {
            get { return recordType; }
            set
            {
                if (value != recordType)
                {
                    recordType = value;
                    NotifyPropertyChanged("RecordType");
                }
            }
        }

        public string ForeignIDType
        {
            get { return foreignIdType; }
            set
            {
                if (value != foreignIdType)
                {
                    foreignIdType = value;
                    NotifyPropertyChanged("ForeignIDType");
                }
            }
        }

        public string ForeignID
        {
            get { return foreignId; }
            set
            {
                if (value != foreignId)
                {
                    foreignId = value;
                    NotifyPropertyChanged("ForeignID");
                }
            }
        }

        public string MyPicture
        {
            get { return myPicture; }
            set
            {
                if (value != myPicture)
                {
                    myPicture = value;
                    NotifyPropertyChanged("MyPicture");
                }
            }
        }

        public bool IsThisInitiator
        {
            get { return isThisInitiator; }
            set
            {
                if (value != isThisInitiator)
                {
                    isThisInitiator = value;
                    NotifyPropertyChanged("IsThisInitiator");
                }
            }
        }

        public bool HasContent
        {
            get { return hasContent; }
            set
            {
                if (value != hasContent)
                {
                    hasContent = value;
                    NotifyPropertyChanged("HasContent");
                }
            }
        }

        public string Content
        {
            get { return content; }
            set
            {
                if (value != content)
                {
                    content = value;
                    NotifyPropertyChanged("Content");
                }
            }
        }

        public string ContentId
        {
            get { return contentid; }
            set
            {
                if (value != contentid)
                {
                    contentid = value;
                    NotifyPropertyChanged("ContentId");
                }
            }
        }

        public string UserComment
        {
            get { return userComment; }
            set
            {
                if (value != userComment)
                {
                    userComment = value;
                    NotifyPropertyChanged("UserComment");
                }
            }
        }

        public string UserVisibility
        {
            get { return userVisibility; }
            set
            {
                if (value != userVisibility)
                {
                    userVisibility = value;
                    NotifyPropertyChanged("UserVisibility");
                }
            }
        }

        public string UserLogin
        {
            get { return userlogin; }
            set
            {
                if (value != userlogin)
                {
                    userlogin = value;
                    NotifyPropertyChanged("UserLogin");
                }
            }
        }

        public string GeoPosition
        {
            get { return geoposition; }
            set
            {
                if (value != geoposition)
                {
                    geoposition = value;
                    NotifyPropertyChanged("GeoPosition");
                }
            }
        }

        public string Latitude
        {
            get { return latitude; }
            set
            {
                if (value != latitude)
                {
                    latitude = value;
                    NotifyPropertyChanged("Latitude");
                }
            }
        }

        public string Longitude
        {
            get { return longitude; }
            set
            {
                if (value != longitude)
                {
                    longitude = value;
                    NotifyPropertyChanged("Longitude");
                }
            }
        }

        public string Altitude
        {
            get { return altitude; }
            set
            {
                if (value != altitude)
                {
                    altitude = value;
                    NotifyPropertyChanged("Altitude");
                }
            }
        }

        public string Picture
        {
            get { return picture; }
            set
            {
                if (value != picture)
                {
                    picture = value;
                    NotifyPropertyChanged("Picture");
                }
            }
        }

        public string ExchangeId
        {
            get { return exchangeId; }
            set
            {
                if (value != exchangeId)
                {
                    exchangeId = value;
                    NotifyPropertyChanged("ExchangeId");
                }
            }
        }

        public string OndoId
        {
            get { return ondoId; }
            set
            {
                if (value != ondoId)
                {
                    ondoId = value;
                    NotifyPropertyChanged("OndoId");
                }
            }
        }

        public string FromUserId
        {
            get { return fromUserId; }
            set
            {
                if (value != fromUserId)
                {
                    fromUserId = value;
                    NotifyPropertyChanged("FromUserId");
                }
            }
        }

        public string ToUserId
        {
            get { return toUserId; }
            set
            {
                if (value != toUserId)
                {
                    toUserId = value;
                    NotifyPropertyChanged("ToUserId");
                }
            }
        }

        public string ExchangeTime
        {
            get { return exchangeTime; }
            set
            {
                if (value != exchangeTime)
                {
                    exchangeTime = value;
                    NotifyPropertyChanged("ExchangeTime");
                }
            }
        }

        public string Anonymous
        {
            get { return anonymous; }
            set
            {
                if (value != anonymous)
                {
                    anonymous = value;
                    NotifyPropertyChanged("Anonymous");
                }
            }
        }

        public string Guid
        {
            get { return guid; }
            set
            {
                if (value != guid)
                {
                    guid = value;
                    NotifyPropertyChanged("Guid");
                }
            }
        }

        public string Voting
        {
            get { return voting; }
            set
            {
                if (value != voting)
                {
                    voting = value;
                    NotifyPropertyChanged("Voting");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
