﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class TransactionDetailsItem
    {
        public string Title
        {
            get;
            set;
        }

        public string MiniPicture
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }
    }
}
