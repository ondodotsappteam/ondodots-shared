﻿using System;
using System.ComponentModel;

namespace SharedOndoLibrary.Data
{
    public class CommentsData : INotifyPropertyChanged
    {

        private string picture;
        private string text;
        private bool isUserComment;
        private bool isNormalComment;
        private string time;
        private string userName;
        private string checkSum;
        private bool isThisInitiator;
		private int commentId;
        private int userId;
        private bool hasMiniPicture;
        private string ownPicture;
        private string ownName;
        private string visibilityUrl;
        private string miniPicture;
        private string numberOfComments;
        private int voting;
        private string votUrl;

        public bool IsNotFirstItem
        {
            get;
            set;
        }

        public string PostText
        {
            get;
            set;
        }

        public string HyperUri
        {
            get;
            set;
        }

        public string VoteUrl
        {
            get { return votUrl; }
            set
            {
                if (value != votUrl)
                {
                    votUrl = value;
                    NotifyPropertyChanged("VoteUrl");
                }
            }
        }

        public int Voting
        {
            get { return voting; }
            set
            {
                if (value != voting)
                {
                    voting = value;
                    NotifyPropertyChanged("Voting");
                }
            }
        }

        public string NumberOfComments
        {
            get { return numberOfComments; }
            set
            {
                if (value != numberOfComments)
                {
                    numberOfComments = value;
                    NotifyPropertyChanged("NumberOfComments");
                }
            }
        }

        public string OwnPicture
        {
            get { return ownPicture; }
            set
            {
                if (value != ownPicture)
                {
                    ownPicture = value;
                    NotifyPropertyChanged("OwnPicture");
                }
            }
        }

        public string MiniPicture
        {
            get { return miniPicture; }
            set
            {
                if (value != miniPicture)
                {
                    miniPicture = value;
                    NotifyPropertyChanged("MiniPicture");
                }
            }
        }

        public string OwnName
        {
            get { return ownName; }
            set
            {
                if (value != ownName)
                {
                    ownName = value;
                    NotifyPropertyChanged("OwnName");
                }
            }
        }

        public bool HasMiniPicture
        {
            get { return hasMiniPicture; }
            set
            {
                if (value != hasMiniPicture)
                {
                    hasMiniPicture = value;
                    NotifyPropertyChanged("HasMiniPicture");
                }
            }
        }

        public int UserId
        {
            get { return userId; }
            set
            {
                if (value != userId)
                {
                    userId = value;
                    NotifyPropertyChanged("UserId");
                }
            }
        }

        public string VisibilityUrl
        {
            get { return visibilityUrl; }
            set
            {
                if (value != visibilityUrl)
                {
                    visibilityUrl = value;
                    NotifyPropertyChanged("Community");
                }
            }
        }

        public int CommentId
        {
            get { return commentId; }
            set
            {
                if (value != commentId)
                {
                    commentId = value;
                    NotifyPropertyChanged("CommentId");
                }
            }
        }

        public string CheckSum
        {
            get { return checkSum; }
            set
            {
                if (value != checkSum)
                {
                    checkSum = value;
                    NotifyPropertyChanged("CheckSum");
                }
            }
        }

        public bool IsThisInitiator
        {
            get { return isThisInitiator; }
            set
            {
                if (value != isThisInitiator)
                {
                    isThisInitiator = value;
                    NotifyPropertyChanged("IsThisInitiator");
                }
            }
        }
        

        public bool IsNormalComment
        {
            get { return isNormalComment; }
            set
            {
                if (value != isNormalComment)
                {
                    isNormalComment = value;
                    NotifyPropertyChanged("IsNormalComment");
                }
            }
        }

        public bool IsUserComment
        {
            get { return isUserComment; }
            set
            {
                if (value != isUserComment)
                {
                    isUserComment = value;
                    NotifyPropertyChanged("IsUserComment");
                }
            }
        }

        public string UserName
        {
            get { return userName; }
            set
            {
                if (value != userName)
                {
                    userName = value;
                    NotifyPropertyChanged("UserName");
                }
            }
        }

        public string Picture
        {
            get { return picture; }
            set
            {
                if (value != picture)
                {
                    picture = value;
                    NotifyPropertyChanged("Picture");
                }
            }
        }

        public string Time
        {
            get { return time; }
            set
            {
                if (value != time)
                {
                    time = value;
                    NotifyPropertyChanged("Time");
                }
            }
        }

        public string Text
        {
            get { return text; }
            set
            {
                if (value != text)
                {
                    text = value;
                    NotifyPropertyChanged("Text");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
