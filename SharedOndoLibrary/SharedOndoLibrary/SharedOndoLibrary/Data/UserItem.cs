﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class UserItem
    {
        public int UserId
        {
            get;
            set;
        }

        public string Phone
        {
            get;
            set;
        }

        public string Culture
        {
            get;
            set;
        }

        public string Login
        {
            get;
            set;
        }

        public int InformationLevel
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public int Gender
        {
            get;
            set;
        }

        public int Auth
        {
            get;
            set;
        }

        public int BirthYear
        {
            get;
            set;
        }

        public string PushUri
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string AccessToken
        {
            get;
            set;
        }

        public bool PushDisabled
        {
            get;
            set;
        }

        public bool GameSoundDisabled
        {
            get;
            set;
        }

        public string Language
        {
            get;
            set;
        }
    }
}
