﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class BenefitProgramNetsItem
    {

        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }
        
        public int CardStatus
        {
            get;
            set;
        }

        public int ResultValue
        {
            get;
            set;
        }

        public int BarcodeLength
        {
            get;
            set;
        }

        public int OndoResult
        {
            get;
            set;
        }

        public string Cardnr
        {
            get;
            set;
        }

        public string Balance
        {
            get;
            set;
        }

        public string ResultString
        {
            get;
            set;
        }

        public string BarCodePicture
        {
            get;
            set;
        }

        public string ExpiryDate
        {
            get;
            set;
        }

        public string Title
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public string Conditions
        {
            get;
            set;
        }

        public int HasLocations
        {
            get;
            set;
        }

        public int PicWidth
        {
            get;
            set;
        }

        public int PicHeight
        {
            get;
            set;
        }

        public int ActionByte
        {
            get;
            set;
        }
    }
}
