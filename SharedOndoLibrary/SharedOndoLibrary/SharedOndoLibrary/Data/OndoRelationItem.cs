﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class OndoRelationItem
    {
        public int OndoId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string MiniPicture { get; set; }
    }
}
