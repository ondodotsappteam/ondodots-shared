﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class BenefitProgramItem
    {
        public int Type
        {
            get;
            set;
        }

        public int PointValue
        {
            get;
            set;
        }

        public string Currency
        {
            get;
            set;
        }

        public int ValidDays
        {
            get;
            set;
        }

        public int BenefitType
        {
            get;
            set;
        }
    }
}
