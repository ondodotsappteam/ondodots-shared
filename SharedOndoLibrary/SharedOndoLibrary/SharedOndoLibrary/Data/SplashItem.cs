﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class SplashItem
    {
        public string SplashTitle
        {
            get;
            set;
        }

        public string SplashDescription
        {
            get;
            set;
        }

        public string SplashPicture
        {
            get;
            set;
        }
    }
}
