﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class GameResultItem
    {
        public int Score
        {
            get;
            set;
        }

        public int LastScore
        {
            get;
            set;
        }

        public int RemainTurns
        {
            get;
            set;
        }

        public int Balance
        {
            get;
            set;
        }

        public int GameStatus
        {
            get;
            set;
        }

        public string NextGame
        {
            get;
            set;
        }

        public int Items
        {
            get;
            set;
        }
      
    }
}
