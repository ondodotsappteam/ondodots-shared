﻿namespace SharedOndoLibrary.Helpers
{
    public static class GeoDataHelper
    {
        private static string _locality;
        public static string Locality
        {
            get { return _locality; }
            set { _locality = value; }
        }

        private static string _country;
        public static string Country
        {
            get { return _country; }
            set { _country = value; }
        }
    }
}
