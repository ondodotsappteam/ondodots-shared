﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Helpers
{
    public class GameStateEventHelper
    {
        private GameState _currentState = GameState.None;

        public GameState Event(GameEvent gameEvent)
        {
            switch (gameEvent)
            {
                case GameEvent.Continue:
                    if (_currentState == GameState.Continue)
                    {
                        _currentState = GameState.RollIntro1;
                    }
                    else
                    {
                        _currentState = GameState.Continue;
                    }
                    break;
                case GameEvent.GetServer:
                    if (_currentState == GameState.None)
                    {
                        _currentState = GameState.GettingServerResult;
                    }
                    else if (_currentState == GameState.GettingServerResult)
                    {
                        _currentState = GameState.GettingServerResult;
                    }
                    else if (_currentState == GameState.Ended)
                    {
                        _currentState = GameState.GettingServerResult;
                    }
                    break;
                case GameEvent.ServerCompleted:
                    if (_currentState == GameState.GettingServerResult)
                    {
                        _currentState = GameState.ServerResultReady;
                    }
                    break;
                case GameEvent.StartIntro:
                    if (_currentState == GameState.ServerResultReady)
                    {
                        _currentState = GameState.RollIntro1;
                    }
                    break;
                case GameEvent.Roll1Done:
                    if (_currentState == GameState.RollIntro1)
                    {
                        _currentState = GameState.RollIntro2;
                    }
                    break;
                case GameEvent.Roll2Done:
                    if (_currentState == GameState.RollIntro2)
                    {
                        _currentState = GameState.RollIntro1;
                    }
                    break;
                case GameEvent.Shake:
                    if (_currentState == GameState.RollIntro1)
                    {
                        _currentState = GameState.Rolling;
                    }
                    else if (_currentState == GameState.RollIntro2)
                    {
                        _currentState = GameState.Rolling;
                    }
                    else if (_currentState == GameState.ShowingResult)
                    {
                        _currentState = GameState.Catching;
                    }
                    break;
                case GameEvent.RollEnded:
                    if (_currentState == GameState.Rolling)
                    {
                        _currentState = GameState.ShowingResult;
                    }
                    break;
                case GameEvent.CatchEnd:
                    if (_currentState == GameState.Catching)
                    {
                        _currentState = GameState.Rolling;
                    }
                    break;
                case GameEvent.Won:
                    if (_currentState == GameState.ShowingResult)
                    {
                        _currentState = GameState.ChoicePending;
                    }
                    else if (_currentState == GameState.None)
                    {
                        _currentState = GameState.ChoicePending;
                    }
                    break;
                case GameEvent.TapChoice:
                    if (_currentState == GameState.ChoicePending)
                    {
                        _currentState = GameState.SettingChoice;
                    }
                    break;
                case GameEvent.ServerChoiceDone:
                    if (_currentState == GameState.SettingChoice)
                    {
                        _currentState = GameState.Ended;
                    }
                    break;
                case GameEvent.End:
                    if (_currentState == GameState.ShowingResult)
                    {
                        _currentState = GameState.Ended;
                    }
                    else if (_currentState == GameState.Won)
                    {
                        _currentState = GameState.Ended;
                    }
                    else if (_currentState == GameState.None)
                    {
                        _currentState = GameState.Ended;
                    }
                    else if (_currentState == GameState.Continue)
                    {
                        _currentState = GameState.Ended;
                    }
                    break;
                case GameEvent.Over:
                    if (_currentState == GameState.None)
                    {
                        _currentState = GameState.Over;
                    }
                    break;
                default:
                    break;

            }

            return _currentState;          
        }
    }

    public enum GameEvent
    {
        Continue,
        GetServer,
        ServerCompleted,
        StartIntro,
        Roll1Done,
        Roll2Done,
        Shake,
        CatchEnd,
        Over,
        RollEnded,
        Won,
        TapChoice,
        ServerChoiceDone,
        End
    }

    public enum GameState
    {
        None,
        GettingServerResult,
        ServerResultReady,
        RollIntro1,
        RollIntro2,
        Rolling,
        ShowingResult,
        Catching,
        Won,
        ChoicePending,
        SettingChoice,
        Continue,
        Ended,
        Over
    }
}
