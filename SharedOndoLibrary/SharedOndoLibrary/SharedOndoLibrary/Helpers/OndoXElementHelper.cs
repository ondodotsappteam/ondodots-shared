﻿using System;
using System.Xml.Linq;

#if WINDOWS_PHONE
using System.Device.Location;
using System.Windows.Media.Imaging;
#elif MONOTOUCH
using CoreLocation;
using System.Xml;
#elif __ANDROID__
using Android.Locations;
using System.Xml;
#endif
using SharedOndoLibrary.Data;
using System.Text.RegularExpressions;
using System.Reflection;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SharedOndoLibrary.Helpers
{
    public class OndoXElementHelper
    {
        #region enums
        public enum eStartGameResult
        {
            OK = 0,
            InputError,
            InvalidPhone,
            UserNotKnown,
            UserMustVerifyPhone,
            UserHasAlreadyPaid,
            BenefitNotFound,
            MaxUserLimit
        }

        public enum eUserVoucherStatus
        {
            Unknown = 0,
            Issued = 1,
            Used = 2,
            Expired = 3,
            ExpiredByUse = 4
        }

        public enum ePaymentType
        {
            Unknown = 0,
            MobilePay = 1,
            PayPal = 2
        }

        public enum ePasswordRequestResult
        {
            OK = 0,
            ExceptionThrow = 1,
            UnkownUser = 2,
            UnknownApp = 3,
            UserHasInvalidEmailAndPhone = 4,
            RequestTooOld,
            RequestNotFound,
            IncorrectPasswordFormat,
            IncorrectGuidFormat,
            IncorrectEmailFormat
        }

        public enum eAppStartUpView
        {
            Map = 0,
            Wallet = 1,
            Favorites  = 2,
            Ondo
        }

        [Flags]
        public enum eAppConfigurationActionByte:int
        {
            None = 0,
            AddContactOndosToFavorites = 1,
            OnlyOneFavorite = 2,
            HideFilterButton = 4,
            HideScanButton = 8,
            ShareAvailableForOwnerOnly = 16,
            HideNewsletterOption = 32,
            HideWalletButton = 64,
            VerbosePushMessages = 128,
            HidePlacesButton = 256
        }


        public enum eBenefitProgramActionByte
        {
            None = 0,
            UseFunctionKey = 1,
            CanEnterCode = 2,
            IsCrypted = 4,
            IsIncludedInClubList = 8,
            SupportECards = 16,
            AddLotteryTicket = 32,
            NoCardValidation = 64,
            AddPointsInReport = 128
        }

        public enum eActionByteBit
        {
            None,
            OndoFeed = 1,
            Facebook = 2,
            Twitter = 4,
            AddBenefitByJoin = 8
        }

        [Flags]
        public enum eVerifyUserBits : int
        {
            Gender = 1,
            Name = 2,
            BirthYear = 4,
            Phone = 8,
            Email = 16,
            Picture = 32,
            NewCard = 64,
            PushUri = 128
        }

        public enum eTranslateLanguage
        {
            Arabic,                  
            Bulgarian,
            Catalan,
            ChineseSimplified,       
            ChineseTraditional,      
            Czech,
            Danish,                  
            Dutch,
            English,                 
            Estonian,
            Finnish,
            French,                  
            German,                  
            Greek,
            HaitianCreole,
            Hebrew,
            Hindi,
            HmongDaw,
            Hungarian,
            Indonesian,
            Italian,
            Japanese,
            Korean,
            Latvian,
            Lithuanian,
            Malay,
            Maltese,
            Norwegian,               
            Persian,
            Polish,
            Portuguese,
            Romanian,
            Russian,
            Slovak,
            Slovenian,
            Spanish,                 
            Swedish,                 
            Thai,
            Turkish,
            Ukrainian,
            Urdu,
            Vietnamese,
            Welsh
        }

        public enum eBenefitProgramRole
        {
            Viewer = 0,
            Participant,
            Owner
        }

        public enum eUnitType
        {
            Unknovn,
            Mark,
            Sheet,
            Card,
            Voucher
        }

        public enum eParentIdType
        {
            Ondo = 0,
            Url = 1,
            Benefit = 2,
            MobilePay = 3
        }

        public enum eBenefitDistance
        {
            All,
            OneHundredKilometers,
            FiftyKilometers,
            TenKilometers,
            FiveKilometers,
            OneKilometer,
            FiveHundredMeters,
            TwoHundredMeters,
            OneHundredMeters
        }

        public enum eTranslateToCulture
        {
            usEng,
            deDE,
            esES,
            frFR,
            svSE,
            nbNO,
            daDK,
            arAR
        }

        public enum eBenefitProgramStatus
        {
            Pending,
            Ready,
            Active,
            Hold
        }

        public enum eTransactionResult
        {
            OK,
            Unknown,
            UserNotValid,
            PointsNotValid,
            BenefitNotValid,
            BarCodeNotValid,
            NoValidUserData,
            NotOwner,
            MoreThanOneOndo,
            NoAccount,
            BenefitIdDontMatchBarcode,
            BenefitNotFound,
            NewAccountCreated,
            NotMember,
            BalanceTooLow,
            BenefitCannotBeUsedHere,
            PhoneNotValid,
            CardIsBlocked
        }

        public enum eProgramType
        {
            TenMarks,
            FiveMarks,
            ThreeMarks,
            OneMark
        }

        public enum eBadgePosition
        {
            None = 0,
            ButtomRight = 1,
            ButtomLeft = 2,
            TopRight = 4,
            TopLeft = 8
        }

        public enum eSocialMediaBit
        {
            None = 0,
            OndoFeed = 1,
            Facebook = 2,
            Twitter = 4,
            RelationsMedia = 8
        }

        public enum ePlannedPostType
        {
            Unknown = 0,
            Own = 1,
            OndoDots = 2,
            Benefit = 3,
            Admin = 4
        }

        public enum eGameUserStatus
        {
            Unknown = 0,
            Waiting = 1,
            Done = 2,
            ChoicePending = 3,
            End = 4
        };

        enum eCurfewPeriod
        {
            OneHour = 0,
            FifteenMin = 1,
            FiveMin = 2
        }

        public enum eInformationLevel
        {
            None = 0,
            NewsLetter = 1
        }

        public enum eParticipants
        {
            All = 1,
            New = 2
        }

        public enum eBenefitStateBit
        {
            Expired = 1,
            Running = 2,
            Future = 4
        }
        public enum ePostType
        {
            Normal = 0,
            Benefit = 1
        }

        public enum eRole
        {
            Viewer = 0,
            Participant = 1,
            Owner = 2
        }

        public enum eStatisticsPeriod
        {
            Day = 0,
            Week = 1,
            Month = 2,
            Quarter = 3
        }

        public enum eFileType
        {
            jpeg = 1,
            text
        }

        public enum eTagAction
        {
            No = 0,
            NPS = 1,
            Feed = 2,
            Benefits = 3
        }

        public enum eParAction
        {
            No = 0,
            NPS = 1,
            Feed = 2,
            Benefits = 3
        }

        public enum eBlobDirectory
        {
            Content = 1,
            Profile,
            Benefit,
            Splash
        }

        public enum eSplash
        {
            No = 0,
            Yes = 1
        }

        public enum eVisibility
        {
            NotVisible = 1,
            Visible = 2
        };

        public enum eCommunity
        {
            Initiator = 0,
            Group = 1,
            EveryOne = 2
        };

        public enum eRelationShip
        {
            Zero = 0,
            One = 1,
            Many = 2
        };

        public enum eStatus
        {
            Pending = 1,
            Active = 2,
            Closed = 3
        };

        public enum eType
        {
            Commercial = 1,
            Share = 2,
            Influence = 3,
            Collect = 4,
            Contact = 5,
            Infrastructure = 6,
            Organization = 7,
            Clustered = 8
        };

        public enum eSubType
        {
            Race = 1,
            TreasureHunt = 2,
            MultilevelMarketing = 3,
            Charity = 4,
            FundRaising = 5,
            InstantCommunity = 6,
            InterestGroup = 7,
            HomeWork = 8,
            MyIdolCollection = 9,
            Complain = 10,
            Feedback = 11,
            RoarFromCrowd = 12,
            Lottery = 13
        };

        public enum eCategory
        {
            Commercial = 0,
            NonCommercial = 1
        };

        public enum eMimeType
        {
            JPG = 1,
            BMP = 2,
            ASC = 3
        };

        public enum eBenefitType
        {
            None = 0,
            Text = 1,
            Lottery,
            Event,
            DiceGame,
            OndoLottery,
            Voucher,
            CollectAndGet,
            AdhocLottery,
            GiftCard,
            Payment,
            UserVoucher,
            DiceGamePayment
        }

        public enum eBenefitTypeBit
        {
            Text = 1,
            Lottery = 2,
            Event = 4,
            DiceGame = 8
        }

        public enum eGenderBit
        {
            Unknown = 1,
            Female = 2,
            Male = 4
        }

        public enum eBenefitStatus
        {
            Pending = 0,
            Active = 1,
            Closed = 2
        }

        #endregion

        #region parsetoxml

        public XElement CreatePrivateOndoXElement(string Title,
                                                    string Description,
                                                    string Content,
                                                    eType Type,
                                                    int Initiator,
                                                    eCommunity Community,
                                                    double Latitude,
                                                    double Longitude)
        {
            XElement elmOndo = null;

            if ((Type != eType.Commercial) && (Community != eCommunity.EveryOne))
            {
                elmOndo = new XElement("ondo");
                elmOndo.Add(
                            new XElement("Title", Title.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                            new XElement("Content", Content),
                            new XElement("Type", ((int)Type).ToString()),
                            new XElement("Initiator", Initiator.ToString()),
                            new XElement("MiniPicture", ""), // Automatically generated on server
                            new XElement("Description", Description.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                            new XElement("ContentType", "url"),
                            new XElement("TagAction", "0"),
                            new XElement("ParAction", "0"),
                            new XElement("Community", ((int)Community).ToString()),
                            new XElement("Splash", "0"),
                            new XElement("Status", "0"),
                            new XElement("InitiatorHandovers", "500"),
                            new XElement("ParticipantHandovers", "500"),
                            new XElement("Latitude", Latitude.ToString().Replace(',', '.')),
                            new XElement("Longitude", Longitude.ToString().Replace(',', '.')),
                            new XElement("ReceiveTimes", "1"),
                            new XElement("MapIcon", "90001"),
                            new XElement("Sponsor", "http://www.ondodots.com/sponsor/ondodots.html") 
                            );
            }
            return elmOndo;
        }


#if WINDOWS_PHONE || WINDOWS
        public XElement CreateOndoXElement(string OndoTitle,
                                            string OndoDescription,
                                            string OndoContent,
                                            eType OndoType,
                                            eSubType OndoSubType,
                                            string OndoVisibility,
                                            string OndoInitiatorHandovers,
                                            string OndoParticipantHandovers,
                                            string OndoReceiveTimes,
                                            int OndoInitiator,
                                            eStatus OndoStatus,
                                            string OndoStatusMessage,
                                            string OndoMiniPicture,
                                            string OndoVotingScale,
                                            string OndoVotingPeriod,
                                            string OndoBarcodeFormat,
                                            string OndoBarcode,
                                            string OndoGeoTag,
                                            string OndoCommunity,
                                            string OndoCountry,
                                            string OndoLocality)
        {
#elif MONOTOUCH || WINDOWS
        public XElement CreateOndoXElement(string OndoTitle,
                                            string OndoDescription,
                                            string OndoContent,
                                            eType OndoType,
                                            eSubType OndoSubType,
                                            string OndoVisibility,
                                            string OndoInitiatorHandovers,
                                            string OndoParticipantHandovers,
                                            string OndoReceiveTimes,
                                            int OndoInitiator,
                                            eStatus OndoStatus,
                                            string OndoStatusMessage,
                                            string OndoMiniPicture,
                                            string OndoVotingScale,
                                            string OndoVotingPeriod,
                                            string OndoBarcodeFormat,
                                            string OndoBarcode,
                                            string OndoGeoTag,
                                            string OndoCommunity,
                                            string OndoCountry,
                                            string OndoLocality)
        {
#elif __ANDROID__
				public XElement CreateOndoXElement(string OndoTitle,
				                                   string OndoDescription,
				                                   string OndoContent,
				                                   eType OndoType,
				                                   eSubType OndoSubType,
				                                   string OndoVisibility,
				                                   string OndoInitiatorHandovers,
				                                   string OndoParticipantHandovers,
				                                   string OndoReceiveTimes,
				                                   int OndoInitiator,
				                                   eStatus OndoStatus,
				                                   string OndoStatusMessage,
				                                   string OndoMiniPicture,
				                                   string OndoVotingScale,
				                                   string OndoVotingPeriod,
				                                   string OndoBarcodeFormat,
				                                   string OndoBarcode,
				                                   string OndoGeoTag,
				                                   string OndoCommunity,
				                                   string OndoCountry,
				                                   string OndoLocality)
				{
#endif
            string country = GeoDataHelper.Country;
            string locality = GeoDataHelper.Locality;
            string geoTagCombine = "";
#if WINDOWS_PHONE || WINDOWS
            if (OndoGeoTag != null)
                geoTagCombine = OndoGeoTag;
#endif

            XElement elmOndo = new XElement("ondo");
            elmOndo.Add(
                        new XElement("Title", OndoTitle.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Content", OndoContent),
                        new XElement("Type", ((int)OndoType).ToString()),
                        new XElement("Visibility", OndoVisibility),
                        new XElement("InitiatorHandovers", OndoInitiatorHandovers),
                        new XElement("ParticipantHandovers", OndoParticipantHandovers),
                        new XElement("ReceiveTimes", OndoReceiveTimes),
                        new XElement("Initiator", OndoInitiator.ToString()),
                        new XElement("Status", ((int)OndoStatus).ToString()),
                        new XElement("Community", OndoCommunity),
                        new XElement("Locality", OndoLocality),
                        new XElement("Country", OndoCountry),
                        new XElement("StatusMessage", OndoStatusMessage),
                        new XElement("MiniPicture", OndoMiniPicture),
                        new XElement("VotingScale", OndoVotingScale),
                        new XElement("BarcodeFormat", OndoBarcodeFormat),
                        new XElement("Barcode", OndoBarcode),
                        new XElement("GeoTag", geoTagCombine),
                        new XElement("Device", "Windows Phone"),
                        new XElement("Description", OndoDescription.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("ContentType", "url"),
                        new XElement("Link", ""),
                        new XElement("TagAction", "0"),
                        new XElement("ParAction", "0"),
                        new XElement("Splash", "0")
                        );

            return elmOndo;
        }

        public XElement CreateOndoXElement(OndoItem ondoItem)
        {
            XElement elmOndo = new XElement("ondo");
            elmOndo.Add(
                        new XElement("OndoId", ondoItem.OndoId.ToString()),
                        new XElement("Title", ondoItem.Title.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Content", ondoItem.Content),
                        new XElement("Type", ((int)ondoItem.Type).ToString()),
                        new XElement("Visibility", ondoItem.Visibility),
                        new XElement("InitiatorHandovers", ondoItem.InitiatorHandovers),
                        new XElement("ParticipantHandovers", ondoItem.ParticipantHandovers),
                        new XElement("ReceiveTimes", ondoItem.ReceiveTimes),
                        new XElement("Initiator", ondoItem.Initiator.ToString()),
                        new XElement("Status", ((int)ondoItem.Status).ToString()),
                        new XElement("Community", ondoItem.Community),
                        new XElement("Locality", ondoItem.Locality),
                        new XElement("Country", ondoItem.Country),
                        new XElement("StatusMessage", ondoItem.StatusMessage),
                        new XElement("MiniPicture", ondoItem.MiniPicture),
                        new XElement("VotingScale", ondoItem.VotingScale),
                        new XElement("BarcodeFormat", ondoItem.BarcodeFormat),
                        new XElement("Barcode", ondoItem.Barcode),
                        new XElement("GeoTag", ondoItem.GeoTag),
                        new XElement("Device", ondoItem.Device),
                        new XElement("Description", ondoItem.Description.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("ContentType", ondoItem.ContentType),
                        new XElement("Link", ondoItem.Link),
                        new XElement("TagAction", ondoItem.TagAction.ToString()),
                        new XElement("ParAction", ondoItem.ParAction.ToString()),
                        new XElement("Splash", ondoItem.Splash),
                        new XElement("ContHeight", ondoItem.ContentHeight),
                        new XElement("ProfilePicture", ondoItem.ProfilePicture),
                        new XElement("ProfileName", ondoItem.ProfileName.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("ContWidth", ondoItem.ContentWidth),
                        new XElement("ParentType", ondoItem.ParentType),
                        new XElement("Sponsor", ondoItem.Sponsor),
                        new XElement("MapIcon", ondoItem.MapIcon)
                        );

            return elmOndo;
        }


#if __ANDROID__
        public XElement CreateJoin(string ondoId,  Location location , string fromUser,  string userid, string username,
                                   string OndoCommunity, string OndoVisibility, int InfoByte, int Voting, string Text)
#elif MONOTOUCH
		public XElement CreateJoin(string ondoId,  CLLocation location , string fromUser, string userid, string username,
					                           string OndoCommunity, string OndoVisibility, int InfoByte, int Voting, string Text)
#elif WINDOWS_PHONE
        public XElement CreateJoin(string ondoId, string position, string fromUser, string userid, string username,
                                    string OndoCommunity, string OndoVisibility, int InfoByte, int Voting, string Text)

#elif WINDOWS
        public XElement CreateJoin(string ondoId, string position, string fromUser, 
                                    string OndoCommunity, string OndoVisibility, int InfoByte, string username, int userid, int Voting, string Text)
#endif

        {

#if __ANDROID__
            string geoTagCombine = location.Latitude + ":" + location.Longitude + ":" + location.Altitude;
         

#elif MONOTOUCH
			string geoTagCombine = location.Coordinate.Latitude + ":" + location.Coordinate.Longitude;
#elif WINDOWS_PHONE
            string geoTagCombine = "";
            if (position != null)
                geoTagCombine = position;
#elif WINDOWS
            string geoTagCombine = position;
#endif
            XElement joins = new XElement("Joins");

            joins.Add(new XElement("Join"));
            XElement exchange = joins.Element("Join");
            exchange.Add(
                new XElement("ForeignID", ondoId),
                new XElement("ForeignIDType", 1), // Ondo
                new XElement("RecordType", 1),  // Join
                new XElement("UserID", userid.ToString()),
                new XElement("FromUser", fromUser),
                new XElement("GeoTag", geoTagCombine),
                new XElement("Content", ""),
                new XElement("ContentType", "image/jpeg"),
                new XElement("Voting", Voting.ToString()),
                new XElement("Text", Text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                new XElement("Visibility", OndoVisibility),
                new XElement("Community", OndoCommunity),
                new XElement("InfoByte", InfoByte.ToString())
                );
            return exchange;

            return joins;
        }


#if __ANDROID__ || MONOTOUCH
        public XElement CreateJoin(string ondoId, string fromUser,  string userid, string username,
                                   string OndoCommunity, string OndoVisibility, int InfoByte, int Voting, string Text)
// Case : No geotag available
        {


            XElement joins = new XElement("Joins");

                joins.Add(new XElement("Join"));
                XElement exchange = joins.Element("Join");
                exchange.Add(
                    new XElement("ForeignID", ondoId),
                    new XElement("ForeignIDType", 1), // Ondo
                    new XElement("RecordType", 1),  // Join
                    new XElement("UserID", userid.ToString()),
                    new XElement("FromUser", fromUser),
                    new XElement("GeoTag", ""),
                    new XElement("Content", ""),
                    new XElement("ContentType", "image/jpeg"),
                    new XElement("Voting", Voting.ToString()),
                    new XElement("Text", Text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                    new XElement("Visibility", OndoVisibility),
                    new XElement("Community", OndoCommunity),
                    new XElement("InfoByte", InfoByte.ToString())
                    );
                return exchange;
            
            return joins;
        }

#endif



#if __ANDROID__
        public XElement CreateInitJoin(string ondoId, Location location, UserItemsData userdata, int InfoByte, string senderid)
#elif MONOTOUCH
	   public XElement CreateInitJoin(string ondoId, CLLocation location, UserItemsData userdata, int InfoByte, string senderid)
#elif WINDOWS_PHONE
        public XElement CreateInitJoin(string ondoId, string position, UserItemsData userdata, int InfoByte, string senderid)
#elif WINDOWS
        public XElement CreateInitJoin(string ondoId, string position, UserItemsData userdata,
                                        string OndoCommunity, int InfoByte, string senderid)
#endif
        {
            string geoTagCombine = "";

#if __ANDROID__
            if (location != null)
            {
                geoTagCombine = location.Latitude + ":" + location.Longitude + ":" + location.Altitude;
            }
         
#elif MONOTOUCH
            
#elif WINDOWS_PHONE
            if (position != null)
                geoTagCombine = position;
#endif

            XElement joins = new XElement("Joins");

            if (userdata != null)
            {
                joins.Add(new XElement("Join"));
                XElement exchange = joins.Element("Join");
                exchange.Add(
                    new XElement("ForeignID", ondoId),
                    new XElement("ForeignIDType", 1), // Ondo
                    new XElement("RecordType", "1"),
                    new XElement("UserID", userdata.UserId),
                    new XElement("FromUser", senderid),
                    new XElement("GeoTag", geoTagCombine),
                    new XElement("Content", ""),
                    new XElement("ContentType", "image/jpeg"),
                    new XElement("Text", userdata.Username + " er nu medlem"),
                    new XElement("Voting", "-1"),
                    new XElement("Visibility", "1"),
                    new XElement("Community", "0"),
                    new XElement("InfoByte", InfoByte.ToString())
                    );
                return exchange;
            }
            return joins;
        }

        public XElement CreateInitJoin(int ondoId, string position, string username, int userId, int InfoByte, int senderid)
        {
            string geoTagCombine = "";
            XElement joins = new XElement("Joins");

            joins.Add(new XElement("Join"));
            XElement exchange = joins.Element("Join");
            exchange.Add(
                new XElement("ForeignID", ondoId),
                new XElement("ForeignIDType", 1), // Ondo
                new XElement("RecordType", "1"),
                new XElement("UserID", userId),
                new XElement("FromUser", senderid),
                new XElement("GeoTag", geoTagCombine),
                new XElement("Content", ""),
                new XElement("ContentType", "image/jpeg"),
                new XElement("Text", username + " er nu medlem"),
                new XElement("Voting", "-1"),
                new XElement("Visibility", "1"),
                new XElement("Community", "0"),
                new XElement("InfoByte", InfoByte.ToString())
                );
            return exchange;

        }

#if !WINDOWS
        public XElement CreateComment(string foreignID, string text)
        {
            OndoStorageHandler handler = new OndoStorageHandler();
            UserItemsData userdata = handler.GetUserData();

            XElement comments = new XElement("Comments");

            if (userdata != null)
            {
                comments.Add(new XElement("Comment"));
                XElement exchange = comments.Element("Comment");
                exchange.Add(
                    new XElement("ForeignID", foreignID),
                    new XElement("ForeignIDType", 2), // Comment
                    new XElement("RecordType", 3),  // Text comment
                    new XElement("UserId", userdata.UserId),
                    new XElement("FromUser", "0"),
                    new XElement("GeoTag", ""),
                    new XElement("Content", ""),
                    new XElement("ContentType", "image/jpeg"),
                    new XElement("Text", text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                    new XElement("Voting", "-1"),
                    new XElement("Visibility", "0"),
                    new XElement("Community", "0"),
                    new XElement("InfoByte", "0")
               );
                return exchange;
            }
            return comments;
        }

        public XElement CreateComment(string userId, string foreignId, string text)
        {
            XElement comments = new XElement("Comments");
            comments.Add(new XElement("Comment"));
            XElement exchange = comments.Element("Comment");
            exchange.Add(
                new XElement("ForeignID", foreignId),
                new XElement("ForeignIDType", 2), // Comment
                new XElement("RecordType", 3),  // Text comment
                new XElement("UserId", userId),
                new XElement("FromUser", "0"),
                new XElement("GeoTag", ""),
                new XElement("Content", ""),
                new XElement("ContentType", "image/jpeg"),
                new XElement("Text", text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                new XElement("Voting", "-1"),
                new XElement("Visibility", "0"),
                new XElement("Community", "0"),
                new XElement("InfoByte", "0")
            );
            return exchange;
        }
#elif WINDOWS
        public XElement CreateComment(string foreignID, string userId, string text)
        {
            XElement comments = new XElement("Comments");

            comments.Add(new XElement("Comment"));
            XElement exchange = comments.Element("Comment");
            exchange.Add(
                new XElement("ForeignID", foreignID),
                new XElement("ForeignIDType", 2), // Comment
                new XElement("RecordType", 3),  // Text comment
                new XElement("UserId", userId),
                new XElement("FromUser", "0"),
                new XElement("GeoTag", ""),
                new XElement("Content", ""),
                new XElement("ContentType", "image/jpeg"),
                new XElement("Text", text.Replace("'", "")),
                new XElement("Voting", "-1"),
                new XElement("Visibility", "0"),
                new XElement("Community", "0"),
                new XElement("InfoByte", "0")
            );
            return exchange;
        }
#endif

#if __ANDROID__
        public XElement CreatePost(string foreignID, string content, string text, Location location, string userId, string OndoCommunity, string OndoVisibility, int Voting)
#elif MONOTOUCH
					public XElement CreatePost(string foreignID, string content, string text, CLLocation location, string userId,
					                           string OndoCommunity, string OndoVisibility, int Voting)
#elif WINDOWS_PHONE
        public XElement CreatePost(string foreignID, string content, string text, string position, string userId,
                                    string OndoCommunity, string OndoVisibility, int Voting)
#elif WINDOWS
        public XElement CreatePost(string foreignID, string content, string text, string position, string userId,
                                    string OndoCommunity, string OndoVisibility, int Voting)
#endif
        {
            string geoTagCombine = "";

#if __ANDROID__
            geoTagCombine = location.Latitude + ":" + location.Longitude + ":" + location.Altitude;
      
#elif MONOTOUCH
			geoTagCombine = location.Coordinate.Latitude + ":" + location.Coordinate.Longitude;
               //Not used at the moment
          
#elif WINDOWS_PHONE
            if (position != null)
                geoTagCombine = position;
#elif WINDOWS 
            geoTagCombine = position;
#endif

            XElement comments = new XElement("Posts");

            comments.Add(new XElement("Post"));
            XElement exchange = comments.Element("Post");
            exchange.Add(
                new XElement("ForeignID", foreignID),
                new XElement("ForeignIDType", 1), // Ondo
                new XElement("RecordType", 2),  // Post comment
                new XElement("UserId", userId),
                new XElement("FromUser", "0"),
                new XElement("GeoTag", geoTagCombine),
                new XElement("Content", content),
                new XElement("ContentType", "image/jpeg"),
                new XElement("Voting", Voting.ToString()),
                new XElement("Text", text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                new XElement("Visibility", OndoVisibility),
                new XElement("Community", OndoCommunity),
                new XElement("InfoByte", "0")
                );
            return exchange;

        }

        public XElement CreatePost(PostItem post)
        {
            XElement comments = new XElement("Posts");

            comments.Add(new XElement("Post"));
            XElement postItem = comments.Element("Post");
            postItem.Add(
                new XElement("ForeignID", post.ForeignID),
                new XElement("ForeignIDType", 1), // Ondo
                new XElement("RecordType", 2),  // Post comment
                new XElement("UserId", post.UserId),
                new XElement("FromUser", "0"),
                new XElement("GeoTag", ""),
                new XElement("Content", post.Content),
                new XElement("ContentType", "image/jpeg"),
                new XElement("Voting", "0"),
                new XElement("Text", post.Text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                new XElement("Visibility", "0"),
                new XElement("Community", post.Community),
                new XElement("InfoByte", "0"),
                new XElement("StartDate", post.StartDate),
                new XElement("EndDate", post.EndDate)
                );
            return postItem;
        }

#if __ANDROID__ || MONOTOUCH
        public XElement CreatePost(string foreignID, string content, string text, string userId, string OndoCommunity, string OndoVisibility, int Voting)
// Case : No geotag available
        {
          

            XElement comments = new XElement("Posts");

            comments.Add(new XElement("Post"));
            XElement exchange = comments.Element("Post");
            exchange.Add(
                new XElement("ForeignID", foreignID),
                new XElement("ForeignIDType", 1), // Ondo
                new XElement("RecordType", 2),  // Post comment
                new XElement("UserId", userId),
                new XElement("FromUser", "0"),
                new XElement("GeoTag", ""),
                new XElement("Content", content),
                new XElement("ContentType", "image/jpeg"),
                new XElement("Voting", Voting.ToString()),
                new XElement("Text", text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                new XElement("Visibility", OndoVisibility),
                new XElement("Community", OndoCommunity),
                new XElement("InfoByte", "0")
                );
            return exchange;

        }
#endif

#if __ANDROID__
        public XElement CreateInfluence(string foreignID, string contentId, Location location, string fromUser, string text, string vote, 
                                        string UserId, string OndoCommunity, string OndoVisibility, int InfoByte)
#elif MONOTOUCH
		public XElement CreateInfluence(string foreignID, string contentId, CLLocation location, string fromUser, string text, string vote, 
					                    string UserId, string OndoCommunity, string OndoVisibility, int InfoByte)
#elif WINDOWS_PHONE
        public XElement CreateInfluence(string foreignID, string contentId, string position, string fromUser, string text, string vote,
                                        string UserId, string OndoCommunity, string OndoVisibility, int InfoByte)
#elif WINDOWS
        public XElement CreateInfluence(string foreignID, string contentId, string position, string fromUser, string text, string vote,
                                        string UserId, string OndoCommunity, string OndoVisibility, int InfoByte)
#endif
        {
            string geoTagCombine = "";

#if __ANDROID__
            geoTagCombine = location.Latitude + ":" + location.Longitude + ":" + location.Altitude;
           
        
#elif MONOTOUCH
			geoTagCombine = location.Coordinate.Latitude + ":" + location.Coordinate.Longitude  + ":" + location.Altitude;
            
    
#elif WINDOWS_PHONE
            if (position != null)
                geoTagCombine = position;
#elif WINDOWS
            geoTagCombine = position;
#endif

            XElement comments = new XElement("Comments");

            comments.Add(new XElement("Comment"));
            XElement exchange = comments.Element("Comment");
            exchange.Add(
                new XElement("ForeignID", foreignID),
                new XElement("ForeignIDType", 1), // Ondo
                new XElement("RecordType", 4),  // Influence
                new XElement("UserID", UserId),
                new XElement("FromUser", fromUser),
                new XElement("GeoTag", geoTagCombine),
                new XElement("Content", contentId),
                new XElement("ContentType", "image/jpeg"),
                new XElement("Text", text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                new XElement("Voting", vote),
                new XElement("Visibility", OndoVisibility),
                new XElement("Community", OndoCommunity),
                new XElement("InfoByte", InfoByte.ToString())
                );
            return exchange;
        }

        private string CurrentVersion
        {
            get
            {
                try
                {
                    Regex regex = new Regex(@", Version=(?<Version>\d+.\d+.\d+.\d+)", RegexOptions.Singleline);
                    Match match = regex.Match(Assembly.GetExecutingAssembly().FullName);
                    if (match.Success)
                    {
                        return match.Groups["Version"].Value;
                    }
                }
                catch (Exception e)
                {

                }
                return String.Empty;
            }
        }


        public XElement CreateLottery(string ondoid, string lotteryname, string lotterydescription, string lotterypictureid, string drawtime, string drawwinnertext)
        {
            XElement drawings = new XElement("drawings");
            drawings.Add(new XElement("draw"));

            XElement draw = drawings.Element("draw");
            draw.Add(
                new XElement("drawtime", drawtime),
                new XElement("drawwinnertext", drawwinnertext)
                );

            OndoStorageHandler handler = new OndoStorageHandler();
            UserItemsData userdata = handler.GetUserData();

            XElement lottery = new XElement("lottery");
            if (userdata != null)
            {
                lottery.Add(
                    new XElement("lotteryname", lotteryname),
                    new XElement("lotterydescription", lotterydescription.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                    new XElement("ondoid", ondoid),
                    new XElement("lotterypictureid", lotterypictureid),
                    drawings
                    );


            }
            return lottery;
        }

        public XElement CreateBenefitXml(string ondoid, string title, string text, string base64TextPicture, string endtime, 
                                         string conditions, eBenefitType type, eCommunity community, int onlyImage)
        {
            if ((type == eBenefitType.Event || type == eBenefitType.Text) && community != eCommunity.Initiator)
            {
                XElement benefit = new XElement("benefit");

                benefit.Add(
                            new XElement("EndDate", endtime),
                            new XElement("StartDate", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss")),
                            new XElement("Picture", base64TextPicture),
                            new XElement("Title", title.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                            new XElement("Text", text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                            new XElement("Conditions", conditions.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                            new XElement("Type", ((int)type).ToString()),
                            new XElement("WinnerText", ""),
                            new XElement("NoOfWinners", "1"),
                            new XElement("Status", ((int)eBenefitStatus.Pending).ToString()),
                            new XElement("AccountId", "0"),
                            new XElement("Community", ((int)community).ToString()),
                            new XElement("Curfew", "0"),
                            new XElement("Chances", "0"),
                            new XElement("Items", "0"),
                            new XElement("PriceId", "0"),
                            new XElement("OnlyImage", onlyImage.ToString()),
                            new XElement("Participants", "1")
                            );

                return benefit;
            }
            else
            {
                return null;
            }
        }


        public XElement CreateBenefitXml(BenefitItem benefitItem)
        {
            XElement benefit = new XElement("benefit");

            benefit.Add(
                        new XElement("BenefitId", benefitItem.BenefitId),
                        new XElement("StartDate", benefitItem.Start),
                        new XElement("EndDate", benefitItem.End),
                        new XElement("Picture", benefitItem.Picture),
                        new XElement("Title", benefitItem.Title.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Text", benefitItem.Text.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Conditions", benefitItem.Conditions.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Type", benefitItem.Type),
                        new XElement("WinnerText", benefitItem.WinnerText.Replace("&", "&amp;").Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("NoOfWinners", benefitItem.NoOfWinners),
                        new XElement("Status", benefitItem.Status),
                        new XElement("AccountId", benefitItem.AccountId),
                        new XElement("Participants", benefitItem.Participants),
                        new XElement("Items", benefitItem.Items),
                        new XElement("Chances", benefitItem.Chances),
                        new XElement("Curfew", benefitItem.Curfew),
                        new XElement("PicHeight", benefitItem.PicHeight),
                        new XElement("PicWidth", benefitItem.PicWidth),
                        new XElement("Community", benefitItem.Community),
                        new XElement("PriceId", benefitItem.PriceId),
                        new XElement("OnlyImage", benefitItem.OnlyImage),
                        new XElement("Distance", benefitItem.Distance),
                        new XElement("ActionByte", benefitItem.ActionByte),
                        new XElement("CategoryByte", benefitItem.CategoryByte)
                       );

            return benefit;
        }

        public XElement CreateUser(string login, string username, string password, string firstname, string lastname, int gender,
                                    string auth, int birthyear, string pushuri, string picture, int userid, int informationLevel, 
                                    string accessToken, string phone, string culture)
        {
            XElement OndoUser = new XElement("user");

            OndoUser.Add(
                        new XElement("Login", login),
                        new XElement("UserName", (username == null) ? "" : username.Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Password", password),
                        new XElement("FirstName", (firstname == null) ? "" : firstname.Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("LastName", (lastname == null) ? "" : lastname.Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Gender", gender.ToString()),
                        new XElement("Auth", (auth == null) ? "" : auth),
                        new XElement("BirthYear", birthyear.ToString()),
                        new XElement("PushUri", (pushuri == null) ? "" : pushuri),
                        new XElement("UserId", userid.ToString()),
                        new XElement("Picture", (picture == null) ? "" : picture),
                        new XElement("AccessToken", (accessToken == null) ? "" : accessToken),
                        new XElement("InformationLevel", informationLevel),
                        new XElement("Phone", (phone == null) ? "" : phone),
                        new XElement("Culture", (culture == null) ? "" : culture));

            return OndoUser;
        }

        public XElement CreateUser(UserItem userItem)
        {
            XElement OndoUser = new XElement("user");

            OndoUser.Add(
                        new XElement("Login", userItem.Login),
                        new XElement("UserName", (userItem.UserName == null) ? "" : userItem.UserName.Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Password", userItem.Password),
                        new XElement("FirstName", (userItem.FirstName == null) ? "" : userItem.FirstName.Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("LastName", (userItem.LastName == null) ? "" : userItem.LastName.Replace("'", "&apos;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;")),
                        new XElement("Gender", userItem.Gender.ToString()),
                        new XElement("Auth", userItem.Auth.ToString()),
                        new XElement("BirthYear", userItem.BirthYear.ToString()),
                        new XElement("PushUri", userItem.PushUri),
                        new XElement("UserId", userItem.UserId.ToString()),
                        new XElement("Picture", userItem.Picture),
                        new XElement("AccessToken", userItem.AccessToken),
                        new XElement("InformationLevel", userItem.InformationLevel),
                        new XElement("Phone", userItem.Phone),
                        new XElement("Culture", userItem.Culture));

            return OndoUser;
        }

        public XElement CreatePaymentXml(OndoPaymentItem paymentItem)
        {
            if (paymentItem != null)
            {
                XElement mobile = new XElement("mobilepay");

                if (paymentItem.TransactionId == null)
                {
                    paymentItem.TransactionId = "";
                }
                if (paymentItem.ReceiptMessage == null)
                {
                    paymentItem.ReceiptMessage = "";
                }
                if (paymentItem.MerchantId == null)
                {
                    paymentItem.MerchantId = "";
                }

                mobile.Add(
                            new XElement("Amount", paymentItem.Amount),
                            new XElement("ErrorCode", paymentItem.ErrorCode),
                            new XElement("IsSynchronized", paymentItem.IsSynchronized),
                            new XElement("MerchantId", paymentItem.MerchantId),
                            new XElement("OndoId", paymentItem.OndoId),
                            new XElement("OrderId", paymentItem.OrderId),
                            new XElement("TransactionId", paymentItem.TransactionId),
                            new XElement("ReceiptMessage", paymentItem.ReceiptMessage),
                            new XElement("UserId", paymentItem.UserId)
                           );

                return mobile;
            }
            else
            {
                return null;
            }
        }

        public XElement CreatePaymentXml(BenefitPaymentItem paymentItem)
        {
            if (paymentItem != null)
            {
                XElement mobile = new XElement("mobilepay");

                if (paymentItem.TransactionId == null)
                {
                    paymentItem.TransactionId = "";
                }
                if (paymentItem.ReceiptMessage == null)
                {
                    paymentItem.ReceiptMessage = "";
                }
                if (paymentItem.MerchantId == null)
                {
                    paymentItem.MerchantId = "";
                }

                mobile.Add(
                            new XElement("Amount", paymentItem.Amount),
                            new XElement("ErrorCode", paymentItem.ErrorCode),
                            new XElement("IsSynchronized", paymentItem.IsSynchronized),
                            new XElement("MerchantId", paymentItem.MerchantId),
                            new XElement("BenefitId", paymentItem.BenefitId),
                            new XElement("OrderId", paymentItem.OrderId),
                            new XElement("TransactionId", paymentItem.TransactionId),
                            new XElement("ReceiptMessage", paymentItem.ReceiptMessage),
                            new XElement("UserId", paymentItem.UserId)
                           );

                return mobile;
            }
            else
            {
                return null;
            }
        }

        public XElement CreateCardClubRelation(int userId, int activityid, int benefitId, string cardnr)
        {
            if (userId <= 0 || activityid < 0 || benefitId <= 0 || string.IsNullOrEmpty(cardnr))
            {
                return null;
            }
            XElement relation = new XElement("relation");

            relation.Add(
                    new XElement("ActivityId", activityid),
                    new XElement("BenefitId", benefitId),
                    new XElement("CardNr", cardnr),
                    new XElement("UserId", userId)
                    );

            return relation;
        }

#if WINDOWS_PHONE
        public List<OndoStatisticsItem> ParseOndoStatistics(XElement statistics)
        {
            List<OndoStatisticsItem> Items = new List<OndoStatisticsItem>();
            foreach (XElement x in statistics.Elements("Statistic"))
            {
                try
                {
                    Items.Add(new OndoStatisticsItem()
                    {
                        Detract = x.Element("Detract").Value,
                        Passive = x.Element("Passive").Value,
                        Promo = x.Element("Promo").Value,
                        Unit = x.Element("Unit").Value
                    });
                }
                catch
                {
                }
            }

            return Items;
        }

        public List<NPSPostItem> ParseNPSPosts(XElement statistics)
        {
            List<NPSPostItem> Items = new List<NPSPostItem>();
            foreach (XElement x in statistics.Elements("Post"))
            {
                try
                {
                    Items.Add(new NPSPostItem()
                    {
                        NumberOfComments = x.Element("NumberOfComments").Value,
                        Picture = x.Element("Picture").Value,
                        Text = x.Element("Text").Value,
                        UserName = x.Element("UserName").Value,
                        Voting = x.Element("Voting").Value,
                        CommentId = x.Element("CommentId").Value,
                        Time = x.Element("Time").Value
                    });
                }
                catch
                {
                    try
                    {
                        Items.Add(new NPSPostItem()
                        {
                            NumberOfComments = x.Element("NumberOfComments").Value,
                            Picture = x.Element("Picture").Value,
                            Text = x.Element("Text").Value,
                            UserName = x.Element("UserName").Value,
                            Voting = x.Element("Voting").Value,
                            CommentId = x.Element("CommentId").Value
                        });
                    }
                    catch
                    {
                    }
                }
            }

            return Items;
        }

#endif

#endregion

#if !(WINDOWS_PHONE) && !(WINDOWS)
public XmlNode GetXmlNode(XElement element)
        {
            using (XmlReader xmlReader = element.CreateReader())
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(xmlReader);
                return xmlDoc;
            }
        }

        public XElement GetXElement(XmlNode node)
        {
            XDocument xDoc = new XDocument();

            using (XmlWriter xmlWriter = xDoc.CreateWriter())
                node.WriteTo(xmlWriter);

            return (XElement)xDoc.Root;
        }
#endif

        #region fromxml

        public OndoPaymentItem ParseOndoPayment(XElement payment)
        {
            OndoPaymentItem Item = new OndoPaymentItem();

            try
            {
                Item.Amount = (payment.Element("Amount").Value.ToString() == "") ? "" : payment.Element("Amount").Value;
                Item.MinimumAmount = (payment.Element("MinimumAmount").Value.ToString() == "") ? "" : payment.Element("MinimumAmount").Value;
                Item.ErrorCode = (payment.Element("ErrorCode").Value.ToString() == "") ? -1 : int.Parse(payment.Element("ErrorCode").Value);
                Item.IsSynchronized = (payment.Element("IsSynchronized").Value.ToString() == "") ? -1 : int.Parse(payment.Element("IsSynchronized").Value);
                Item.MerchantId = payment.Element("MerchantId").Value.ToString();
                Item.OndoId = (payment.Element("OndoId").Value.ToString() == "") ? -1 : int.Parse(payment.Element("OndoId").Value);
                Item.OrderId = (payment.Element("OrderId").Value.ToString() == "") ? -1 : int.Parse(payment.Element("OrderId").Value);
                Item.TransactionId = payment.Element("TransactionId").Value.ToString();
                Item.ReceiptMessage = payment.Element("ReceiptMessage").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.UserId = (payment.Element("UserId").Value.ToString() == "") ? -1 : int.Parse(payment.Element("UserId").Value);
                Item.Type = (payment.Element("Type").Value.ToString() == "") ? -1 : int.Parse(payment.Element("Type").Value);
	         	Item.Picture = (payment.Element("Picture").Value.ToString() == "") ? "" : payment.Element("Picture").Value;
		        Item.Title = (payment.Element("Title").Value.ToString() == "") ? "" : payment.Element("Title").Value;
		        Item.DateTime= (payment.Element("DateTime").Value.ToString() == "") ? "" : payment.Element("DateTime").Value;
		}
            catch(Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseOndoPayment:" + e.Message + ": " + e.InnerException);
            }

            return Item;
        }

        public BenefitPaymentItem ParseBenefitPayment(XElement payment)
        {
            BenefitPaymentItem Item = new BenefitPaymentItem();

            try
            {
                Item.Amount = (payment.Element("Amount").Value.ToString() == "") ? "" : payment.Element("Amount").Value;
                Item.MinimumAmount = (payment.Element("MinimumAmount").Value.ToString() == "") ? "" : payment.Element("MinimumAmount").Value;
                Item.ErrorCode = (payment.Element("ErrorCode").Value.ToString() == "") ? -1 : int.Parse(payment.Element("ErrorCode").Value);
                Item.IsSynchronized = (payment.Element("IsSynchronized").Value.ToString() == "") ? -1 : int.Parse(payment.Element("IsSynchronized").Value);
                Item.MerchantId = payment.Element("MerchantId").Value.ToString();
                Item.BenefitId = (payment.Element("BenefitId").Value.ToString() == "") ? -1 : int.Parse(payment.Element("BenefitId").Value);
                Item.OrderId = (payment.Element("OrderId").Value.ToString() == "") ? -1 : int.Parse(payment.Element("OrderId").Value);
                Item.TransactionId = payment.Element("TransactionId").Value.ToString();
                Item.ReceiptMessage = payment.Element("ReceiptMessage").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.UserId = (payment.Element("UserId").Value.ToString() == "") ? -1 : int.Parse(payment.Element("UserId").Value);
                Item.Type = (payment.Element("Type").Value.ToString() == "") ? -1 : int.Parse(payment.Element("Type").Value);
		        Item.Picture = (payment.Element("Picture").Value.ToString() == "") ? "" : payment.Element("Picture").Value;
		        Item.Title = (payment.Element("Title").Value.ToString() == "") ? "" : payment.Element("Title").Value;
		        Item.DateTime= (payment.Element("DateTime").Value.ToString() == "") ? "" : payment.Element("DateTime").Value;

		     }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseBenefitPayment:" + e.Message + ": " + e.InnerException);
            }

            return Item;
        }

        public List<BenefitItem> ParseBenefits(XElement benefits)
        {
            List<BenefitItem> Items = new List<BenefitItem>();
            if (benefits.ToString().Contains("Participants"))
            {
                foreach (XElement x in benefits.Elements("benefit"))
                {
                    try
                    {
                        Items.Add(new BenefitItem()
                        {
                            Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Text = x.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            BenefitId = int.Parse(x.Element("BenefitId").Value),
                            Type = int.Parse(x.Element("Type").Value),
                            AccountId = x.Element("AccountId").Value,
                            Conditions = x.Element("Conditions").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            End = x.Element("EndDate").Value,
                            NoOfWinners = int.Parse(x.Element("NoOfWinners").Value),
                            Picture = x.Element("Picture").Value,
                            Start = x.Element("StartDate").Value,
                            Status = int.Parse(x.Element("Status").Value),
                            WinnerText = x.Element("WinnerText").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Stamp = x.Element("Stamp").Value,
                            Participants = int.Parse(x.Element("Participants").Value),
                            BenefitRole = int.Parse(x.Element("BenefitRole").Value),
                            HasLocations = int.Parse(x.Element("HasLocations").Value),
                            CanEnterCode = (x.Element("CanEnterCode").Value.ToString() == "") ? -1 : int.Parse(x.Element("CanEnterCode").Value),
                            MiniPicture = x.Element("MiniPicture").Value,
                            StampBackground = x.Element("StampBackground").Value.ToString(),
                            CanPlay = (x.Element("CanPlay").Value.ToString() == "") ? -1 : int.Parse(x.Element("CanPlay").Value),
                        });
                    }
                    catch (Exception e)
                    {
                        Items.Add(new BenefitItem()
                        {
                            Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Text = x.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            BenefitId = int.Parse(x.Element("BenefitId").Value),
                            Type = int.Parse(x.Element("Type").Value),
                            AccountId = x.Element("AccountId").Value,
                            Conditions = x.Element("Conditions").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            End = x.Element("EndDate").Value,
                            NoOfWinners = int.Parse(x.Element("NoOfWinners").Value),
                            Picture = x.Element("Picture").Value,
                            Start = x.Element("StartDate").Value,
                            Status = int.Parse(x.Element("Status").Value),
                            WinnerText = x.Element("WinnerText").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Stamp = x.Element("Stamp").Value,
                            HasLocations = 0,
                            BenefitRole = -1
                        });

                        OndoStorageHandler handler = new OndoStorageHandler();
                        handler.logEvent("ParseBenefits:"+ e.Message + ": " + e.InnerException);
                    }
                }
            }
            else
            {
                foreach (XElement x in benefits.Elements("benefit"))
                {
                    try
                    {
                        Items.Add(new BenefitItem()
                        {
                            Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Text = x.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            BenefitId = int.Parse(x.Element("BenefitId").Value),
                            Type = int.Parse(x.Element("Type").Value),
                            AccountId = x.Element("AccountId").Value,
                            Conditions = x.Element("Conditions").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            End = x.Element("EndDate").Value,
                            NoOfWinners = int.Parse(x.Element("NoOfWinners").Value),
                            Picture = x.Element("Picture").Value,
                            Start = x.Element("StartDate").Value,
                            Status = int.Parse(x.Element("Status").Value),
                            WinnerText = x.Element("WinnerText").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Stamp = x.Element("Stamp").Value
                        });
                    }
                    catch (Exception e)
                    {
                        OndoStorageHandler handler = new OndoStorageHandler();
                        handler.logEvent("ParseBenefits:" + e.Message + ": " + e.InnerException);
                    }
                }
            }

            return Items;
        }

        public BenefitItem ParseBenefit(XElement benefit)
        {
            BenefitItem Item = new BenefitItem();

            try
            {
                float fLongitude;
                float fLatitude;
                float.TryParse(benefit.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                float.TryParse(benefit.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                Item.Latitude = fLatitude;
                Item.Longitude = fLongitude;
            }
            catch
            {
            }

            try
            {
                Item.Title = benefit.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Text = benefit.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.BenefitId = int.Parse(benefit.Element("BenefitId").Value);
                Item.Type = int.Parse(benefit.Element("Type").Value);
                Item.AccountId = benefit.Element("AccountId").Value;
                Item.Conditions = benefit.Element("Conditions").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.End = benefit.Element("EndDate").Value;
                Item.NoOfWinners = int.Parse(benefit.Element("NoOfWinners").Value);
                Item.Picture = benefit.Element("Picture").Value;
                Item.Start = benefit.Element("StartDate").Value;
                Item.Status = int.Parse(benefit.Element("Status").Value);
                Item.Stamp = benefit.Element("Stamp").Value;
                Item.WinnerText = benefit.Element("WinnerText").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.UserPicture = benefit.Element("UserPicture").Value;
                Item.UserName = benefit.Element("UserName").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&");
                Item.RemainingTime = benefit.Element("RemainingTime").Value;
                Item.Tickets = int.Parse(benefit.Element("Tickets").Value);
                Item.GameStatus = int.Parse(benefit.Element("GameStatus").Value);
                Item.LastScore = int.Parse(benefit.Element("LastScore").Value);
                Item.RemainTurns = int.Parse(benefit.Element("RemainTurns").Value);
                Item.Balance = int.Parse(benefit.Element("Balance").Value);
                Item.Items = int.Parse(benefit.Element("Items").Value);
                Item.NextGame = benefit.Element("NextGame").Value;
                Item.PicHeight = int.Parse(benefit.Element("PicHeight").Value);
                Item.PicWidth = int.Parse(benefit.Element("PicWidth").Value);
                Item.OnlyImage = int.Parse(benefit.Element("OnlyImage").Value);
                Item.LotteryStatus = int.Parse(benefit.Element("LotteryStatus").Value);
                Item.BarcodeLength = (benefit.Element("BarcodeLength").Value.ToString() == "") ? 0 : int.Parse(benefit.Element("BarcodeLength").Value);
                Item.BenefitRole = (benefit.Element("BenefitRole").Value.ToString() == "") ? -1 : int.Parse(benefit.Element("BenefitRole").Value);
                Item.CanEnterCode = (benefit.Element("CanEnterCode").Value.ToString() == "") ? -1 : int.Parse(benefit.Element("CanEnterCode").Value);
                Item.HasLocations = (benefit.Element("HasLocations").Value.ToString() == "") ? -1 : int.Parse(benefit.Element("HasLocations").Value);
                Item.StampBackground = benefit.Element("StampBackground").Value.ToString();
                Item.UltimatePicture = benefit.Element("UltimatePicture").Value;
                Item.UltimateLink = benefit.Element("UltimateLink").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.TicketsText = benefit.Element("TicketsText").Value;
                Item.NormalPrice = benefit.Element("NormalPrice").Value;
                Item.OfferPrice = benefit.Element("OfferPrice").Value;
                Item.Bonus = benefit.Element("Bonus").Value;
                Item.OndoTitle = benefit.Element("OndoTitle").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").
                                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.CanPlay = (benefit.Element("CanPlay").Value.ToString() == "") ? -1 : int.Parse(benefit.Element("CanPlay").Value);
    }
            catch(Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
              
                try
                {
                    Item.Title = benefit.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                    Item.Text = benefit.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                    Item.BenefitId = int.Parse(benefit.Element("BenefitId").Value);
                    Item.Type = int.Parse(benefit.Element("Type").Value);
                    Item.AccountId = benefit.Element("AccountId").Value;
                    Item.Conditions = benefit.Element("Conditions").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                    Item.End = benefit.Element("EndDate").Value;
                    Item.NoOfWinners = int.Parse(benefit.Element("NoOfWinners").Value);
                    Item.Picture = benefit.Element("Picture").Value;
                    Item.Start = benefit.Element("StartDate").Value;
                    Item.Status = int.Parse(benefit.Element("Status").Value);
                    Item.Stamp = benefit.Element("Stamp").Value;
                    Item.WinnerText = benefit.Element("WinnerText").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                    Item.BarcodeLength = 0;
                }
                catch (Exception ex)
                {
                    handler.logEvent("ParseBenefit2:" + ex.Message + ": " + ex.InnerException);
                }
            }

            return Item;
        }

        public UserItem ParseUser(XElement user)
        {
            UserItem Item = new UserItem();
            try
            {
                Item.UserId = int.Parse(user.Element("UserId").Value);
                Item.Login = user.Element("Login").Value;
                Item.UserId = int.Parse(user.Element("UserId").Value);
                Item.Password = user.Element("Password").Value;
                Item.FirstName = user.Element("FirstName").Value;
                Item.LastName = user.Element("LastName").Value;
                Item.Picture = user.Element("Picture").Value;
                Item.Gender = int.Parse(user.Element("Gender").Value);
                Item.Auth = int.Parse(user.Element("Auth").Value);
                Item.BirthYear = int.Parse(user.Element("BirthYear").Value);
                Item.PushUri = user.Element("PushUri").Value;
                Item.UserName = user.Element("UserName").Value;
                Item.InformationLevel = int.Parse(user.Element("InformationLevel").Value);
                Item.Phone = user.Element("Phone").Value;
                Item.Culture = user.Element("Culture").Value;
                Item.AccessToken = user.Element("AccessToken").Value;
            }
            catch(Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseUser:" + e.Message + ": " + e.InnerException);
            }

            return Item;
        }

        public List<ActivityItem> ParseActivity(XElement activities)
        {
            List<ActivityItem> Items = new List<ActivityItem>();

            string checksum = activities.Element("CheckSum").Value;

            foreach (XElement x in activities.Elements("activity"))
            {
                try
                {
                    Items.Add(new ActivityItem()
                    {
                        CommentId = int.Parse(x.Element("CommentId").Value),
                        Community = int.Parse(x.Element("Community").Value),
                        Content = x.Element("Content").Value,
                        GeoTag = x.Element("GeoTag").Value,
                        NumberOfComments = int.Parse(x.Element("NumberOfComments").Value),
                        Picture = x.Element("Picture").Value,
                        RecordType = int.Parse(x.Element("RecordType").Value),
                        Text = x.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Time = x.Element("Time").Value,
                        UserId = int.Parse(x.Element("UserId").Value),
                        UserName = x.Element("UserName").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Visibility = int.Parse(x.Element("Visibility").Value),
                        MiniPicture = x.Element("MiniPicture").Value,
                        Voting = int.Parse(x.Element("Voting").Value),
                        CheckSum = checksum,
                        Type = int.Parse(x.Element("Type").Value),
                        BenefitId = int.Parse(x.Element("BenefitId").Value),
                        ContentHeight = int.Parse(x.Element("ContentHeight").Value),
                        ContentWidth = int.Parse(x.Element("ContentWidth").Value),
                        NumberOfNewComments = int.Parse(x.Element("NumberOfNewComments").Value),
                        BenefitPicture = x.Element("BenefitPicture").Value,
                        BenefitTitle = x.Element("BenefitTitle").Value,
                        BenefitHeight = int.Parse(x.Element("BenefitHeight").Value),
                        BenefitWidth = int.Parse(x.Element("BenefitWidth").Value),
                        UltimatePicture = x.Element("UltimatePicture").Value,
                        UltimateLink = x.Element("UltimateLink").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        CheckSumNode = x.Element("CheckSumNode").Value,
                        Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Index = (x.Element("Index").Value.ToString() == "") ? -1 : int.Parse(x.Element("Index").Value),
                        Bonus = x.Element("Bonus").Value,
                        NormalPrice = x.Element("NormalPrice").Value,
                        OfferPrice = x.Element("OfferPrice").Value,
                        OndoTitle = x.Element("OndoTitle").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        BenefitType = (x.Element("BenefitType").Value.ToString() == "") ? -1 : int.Parse(x.Element("BenefitType").Value),
                        CanPlay = (x.Element("CanPlay").Value.ToString() == "") ? -1 : int.Parse(x.Element("CanPlay").Value),
	                	OndoMiniPicture = ((x.Element("OndoMiniPicture").Value == "")||(x.Element("OndoMiniPicture").Value == null)) ? "" : (x.Element("OndoMiniPicture").Value),
                        OndoRef = (x.Element("OndoRef").Value.ToString() == "") ? -1 : int.Parse(x.Element("OndoRef").Value),
                    });
                }
                catch
                {
                   
                    try
                    {
                        Items.Add(new ActivityItem()
                        {
                            CommentId = int.Parse(x.Element("CommentId").Value),
                            Community = int.Parse(x.Element("Community").Value),
                            Content = x.Element("Content").Value,
                            GeoTag = x.Element("GeoTag").Value,
                            NumberOfComments = int.Parse(x.Element("NumberOfComments").Value),
                            Picture = x.Element("Picture").Value,
                            RecordType = int.Parse(x.Element("RecordType").Value),
                            Text = x.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Time = x.Element("Time").Value,
                            UserId = int.Parse(x.Element("UserId").Value),
                            UserName = x.Element("UserName").Value,
                            Visibility = int.Parse(x.Element("Visibility").Value),
                            MiniPicture = x.Element("MiniPicture").Value,
                            Voting = int.Parse(x.Element("Voting").Value),
                            CheckSum = checksum,
                            Type = int.Parse(x.Element("Type").Value),
                            BenefitId = int.Parse(x.Element("BenefitId").Value),
                            ContentHeight = 0,
                            ContentWidth = 0
                        });

                    }
                    catch
                    {
                        
                    }
                }
            }

            return Items;
        }

        public List<ActivityItem> ParseActivityComments(XElement activities)
        {
            List<ActivityItem> Items = new List<ActivityItem>();

            try
            {
                XElement post = activities.Element("post");

                Items.Add(new ActivityItem()
                {
                    CommentId = int.Parse(post.Element("CommentId").Value),
                    UserId = int.Parse(post.Element("UserId").Value),
                    Voting = int.Parse(post.Element("Voting").Value),
                    Community = int.Parse(post.Element("Community").Value),
                    MiniPicture = post.Element("MiniPicture").Value,
                    Picture = post.Element("Picture").Value,
                    UserName = post.Element("UserName").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                    NumberOfComments = int.Parse(post.Element("NumberOfComments").Value),
                    Text = post.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                    Time = post.Element("Time").Value
                });
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseActivityComments:" + e.Message + ": " + e.InnerException);
            }
            string checksum = activities.Element("CheckSum").Value;


            foreach (XElement x in activities.Elements("comment"))
            {
                try
                {
                    Items.Add(new ActivityItem()
                    {
                        CommentId = int.Parse(x.Element("CommentId").Value),
                        Picture = x.Element("Picture").Value,
                        Text = x.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Time = x.Element("Time").Value,
                        UserId = int.Parse(x.Element("UserId").Value),
                        UserName = x.Element("UserName").Value,
                        CheckSum = checksum,
                        // not used elements
                        MiniPicture = "",
                        NumberOfComments = -1,
                        Community = -1,
                        Voting = -1
                    });
                }
                catch (Exception ex)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseActivityComments:" + ex.Message + ": " + ex.InnerException);
                }
            }

            return Items;
        }

        public List<ActivityItem> ParseComments(XElement comments)
        {
            List<ActivityItem> Items = new List<ActivityItem>();

            XElement post = comments.Element("post");

            try
            {
                Items.Add(new ActivityItem()
                {
                    CommentId = int.Parse(post.Element("CommentId").Value),
                    UserId = int.Parse(post.Element("UserId").Value),
                    Voting = int.Parse(post.Element("Voting").Value),
                    Community = int.Parse(post.Element("Community").Value),
                    MiniPicture = post.Element("MiniPicture").Value,
                    Picture = post.Element("Picture").Value,
                    UserName = post.Element("UserName").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                    NumberOfComments = int.Parse(post.Element("NumberOfComments").Value),
                    Text = post.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                    Time = post.Element("Time").Value,
                    Index = (post.Element("Index").Value.ToString() == "") ? 0 : int.Parse(post.Element("Index").Value)
                });
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseComments:" + e.Message + ": " + e.InnerException);
            }

            string checksum = comments.Element("CheckSum").Value;
            foreach (XElement x in comments.Elements("comment"))
            {
                try
                {
                    Items.Add(new ActivityItem()
                    {
                        CommentId = int.Parse(x.Element("CommentId").Value),
                        Picture = x.Element("Picture").Value,
                        Text = x.Element("Text").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Time = x.Element("Time").Value,
                        UserId = int.Parse(x.Element("UserId").Value),
                        UserName = x.Element("UserName").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        CheckSum = checksum,
                        Index = (x.Element("Index").Value.ToString() == "") ? 0 : int.Parse(x.Element("Index").Value)
                    });
                }
                catch(Exception ex)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseComments:" + ex.Message + ": " + ex.InnerException);
                }
            }

            return Items;
        }

        public OndoItem ParseOndo(XElement ondo)
        {
                      
            OndoItem Item = new OndoItem();

          
            //XElement ondo = xmlNode.Element("ondo");
   
            try
            {

                float fLongitude;// = float.Parse(longitude);
                float fLatitude;// = float.Parse(latitude);
                float.TryParse(ondo.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                float.TryParse(ondo.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                Item.Barcode = ondo.Element("Barcode").Value;
                Item.BarcodeFormat = ondo.Element("BarcodeFormat").Value;
                Item.Community = int.Parse(ondo.Element("Community").Value);
                Item.Content = ondo.Element("Content").Value;
                Item.ContentType = ondo.Element("ContentType").Value;
                Item.Country = ondo.Element("Country").Value;
                Item.CreateTime = ondo.Element("CreateTime").Value;
                Item.Description = ondo.Element("Description").Value.Replace("&amp;", "&").Replace("amp;", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Device = ondo.Element("Device").Value;
                Item.GeoTag = ondo.Element("GeoTag").Value;
                Item.Initiator = int.Parse(ondo.Element("Initiator").Value);
                Item.InitiatorHandovers = int.Parse(ondo.Element("InitiatorHandovers").Value);
                Item.Link = ondo.Element("Link").Value;
                Item.Locality = ondo.Element("Locality").Value;
                Item.MiniPicture = ondo.Element("MiniPicture").Value;
                Item.NoOfComments = int.Parse(ondo.Element("NoOfComments").Value);
                Item.NoOfJoins = int.Parse(ondo.Element("NoOfJoins").Value);
                Item.OndoError = int.Parse(ondo.Element("OndoError").Value);
                Item.OndoId = int.Parse(ondo.Element("OndoId").Value);
                Item.ParticipantHandovers = int.Parse(ondo.Element("ParticipantHandovers").Value);
                Item.ReceiveTimes = int.Parse(ondo.Element("ReceiveTimes").Value);
                Item.Status = int.Parse(ondo.Element("Status").Value);
                Item.StatusMessage = ondo.Element("StatusMessage").Value;
                Item.Title = ondo.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Type = int.Parse(ondo.Element("Type").Value);
                Item.Visibility = int.Parse(ondo.Element("Visibility").Value);
                Item.VotingScale = int.Parse(ondo.Element("VotingScale").Value);
                Item.TagAction = int.Parse(ondo.Element("TagAction").Value);
                Item.ParAction = int.Parse(ondo.Element("ParAction").Value);
                Item.Splash = int.Parse(ondo.Element("Splash").Value);
                Item.InfoByte = int.Parse(ondo.Element("InfoByte").Value);
                Item.Role = int.Parse(ondo.Element("Role").Value);
                Item.ContactInfo = ondo.Element("ContactInfo").Value;
                Item.Address = ondo.Element("Address").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                if (ondo.Element("ContHeight").Value == null)Item.ContentHeight = 0;
                else Item.ContentHeight = int.Parse(ondo.Element("ContHeight").Value); 
                if (ondo.Element("ContWidth").Value == null)Item.ContentWidth = 0;
                else Item.ContentWidth = int.Parse(ondo.Element("ContWidth").Value); 
                //Item.CheckSum = int.Parse(ondo.Element("CheckSum").Value);
                Item.Phone = ondo.Element("Phone").Value;
                Item.Latitude = fLatitude;
                Item.Longitude = fLongitude;
                Item.QRString = ondo.Element("QRString").Value;
                //            Item.QRUrl = ondo.Element("QRUrl").Value;
                Item.OpeningHoursText = ondo.Element("OpeningHoursText").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&");
                Item.OpeningHoursHeader = ondo.Element("OpeningHoursHeader").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&");
                Item.Sponsor = ondo.Element("Sponsor").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"); ;
                Item.ProfileName = ondo.Element("ProfileName").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&");
                Item.ProfilePicture = ondo.Element("ProfilePicture").Value;
                if ((ondo.Element("RemoteSignup").Value == null)||ondo.Element("RemoteSignup").Value == "")
                {
                    Item.RemoteSignup = 0;
                }
                else
                {
                    Item.RemoteSignup = int.Parse(ondo.Element("RemoteSignup").Value);
                }
                if (ondo.Element("ParentType").Value == null) Item.ParentType = 0;
                else Item.ParentType = int.Parse(ondo.Element("ParentType").Value);
                Item.LotteryStatus = int.Parse(ondo.Element("LotteryStatus").Value);
                Item.UltimateLink = ondo.Element("UltimateLink").Value;
                Item.UltimatePicture = ondo.Element("UltimatePicture").Value;
                Item.MapIcon = ondo.Element("MapIcon").Value;
                Item.HasBenefits = (ondo.Element("HasBenefits").Value.ToString() == "") ? 0 : int.Parse(ondo.Element("HasBenefits").Value);
                Item.IsFavoriteInSingleFeed = (ondo.Element("IsFavoriteInSingleFeed").Value.ToString() == "") ? -1 : int.Parse(ondo.Element("IsFavoriteInSingleFeed").Value);
            }
            catch (Exception e)
            {
                try
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseOndo:" + e.Message + ": " + e.InnerException);

                    float fLongitude;// = float.Parse(longitude);
                    float fLatitude;// = float.Parse(latitude);
                    float.TryParse(ondo.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                    float.TryParse(ondo.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);

                    Item.Barcode = ondo.Element("Barcode").Value;
                    Item.BarcodeFormat = ondo.Element("BarcodeFormat").Value;
                    Item.Community = int.Parse(ondo.Element("Community").Value);
                    Item.Content = ondo.Element("Content").Value;
                    Item.ContentType = ondo.Element("ContentType").Value;
                    Item.Country = ondo.Element("Country").Value;
                    Item.CreateTime = ondo.Element("CreateTime").Value;
                    Item.Description = ondo.Element("Description").Value.Replace("&amp;", "&").Replace("amp;", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                    Item.Device = ondo.Element("Device").Value;
                    Item.GeoTag = ondo.Element("GeoTag").Value;
                    Item.Initiator = int.Parse(ondo.Element("Initiator").Value);
                    Item.InitiatorHandovers = int.Parse(ondo.Element("InitiatorHandovers").Value);
                    Item.Link = ondo.Element("Link").Value;
                    Item.Locality = ondo.Element("Locality").Value;
                    Item.MiniPicture = ondo.Element("MiniPicture").Value;
                    Item.NoOfComments = int.Parse(ondo.Element("NoOfComments").Value);
                    Item.NoOfJoins = int.Parse(ondo.Element("NoOfJoins").Value);
                    Item.OndoError = int.Parse(ondo.Element("OndoError").Value);
                    Item.OndoId = int.Parse(ondo.Element("OndoId").Value);
                    Item.ParticipantHandovers = int.Parse(ondo.Element("ParticipantHandovers").Value);
                    Item.ReceiveTimes = int.Parse(ondo.Element("ReceiveTimes").Value);
                    Item.Status = int.Parse(ondo.Element("Status").Value);
                    Item.StatusMessage = ondo.Element("StatusMessage").Value;
                    Item.Title = ondo.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                    Item.Type = int.Parse(ondo.Element("Type").Value);
                    Item.Visibility = int.Parse(ondo.Element("Visibility").Value);
                    Item.VotingScale = int.Parse(ondo.Element("VotingScale").Value);
                    Item.TagAction = int.Parse(ondo.Element("TagAction").Value);
                    Item.ParAction = int.Parse(ondo.Element("ParAction").Value);
                    Item.Splash = int.Parse(ondo.Element("Splash").Value);
                    Item.InfoByte = int.Parse(ondo.Element("InfoByte").Value);
                    Item.Role = int.Parse(ondo.Element("Role").Value);
                    Item.ContactInfo = ondo.Element("ContactInfo").Value;
                    Item.Latitude = fLatitude;
                    Item.Longitude = fLongitude;
                }
                catch
                {
                }
            }

            return Item;
        }

        public List<OndoItem> ParseOndos(XElement ondos)
        {
            List<OndoItem> Items = new List<OndoItem>();
            string checksum = ondos.Element("CheckSum").Value;

            foreach (XElement x in ondos.Elements("ondo"))
            {
                try
                {
                    if (x.HasElements)
                    {
                        float fLongitude = 0;// = float.Parse(longitude);
                        float fLatitude = 0;// = float.Parse(latitude);
                        try
                        {
                            float.TryParse(x.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                            float.TryParse(x.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                        }
                        catch
                        {
                        }
                        Items.Add(new OndoItem()
                        {
                            OndoId = int.Parse(x.Element("OndoId").Value),
                            Type = int.Parse(x.Element("Type").Value),
                            MiniPicture = x.Element("MiniPicture").Value,
                            Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Description = x.Element("Description").Value.Replace("&amp;", "&").Replace("amp;", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Role = int.Parse(x.Element("Role").Value),
                            InfoByte = int.Parse(x.Element("InfoByte").Value),
                            Latitude = fLatitude,
                            Longitude = fLongitude,
                            Distance = int.Parse(x.Element("Distance").Value),
                            NewPosts = int.Parse(x.Element("NewPosts").Value),
                            CheckSum = checksum,
                            CheckSumNode = x.Element("CheckSumNode").Value
                        });
                    }
                }
                catch(Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseOndos:" + e.Message + ": " + e.InnerException);
                }
            }

            return Items;
        }

        public SplashItem ParseSplash(XElement splash)
        {
            SplashItem Item = new SplashItem();

            try
            {
                Item.SplashTitle = splash.Element("SplashTitle").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&");
                Item.SplashDescription = splash.Element("SplashDescription").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&");
                Item.SplashPicture = splash.Element("SplashPicture").Value;
            }
            catch
            {
            }

            return Item;
        }

        public List<MapItem> ParseSpotlightForMap(XElement ondos)
        {
            List<MapItem> Items = new List<MapItem>();

            foreach (XElement x in ondos.Elements("ondo"))
            {
                try
                {
                    float fLongitude;// = float.Parse(longitude);
                    float fLatitude;// = float.Parse(latitude);
                    float.TryParse(x.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                    float.TryParse(x.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                    Items.Add(new MapItem()
                    {
                        OndoId = int.Parse(x.Element("OndoId").Value),
                        Picture = x.Element("Picture").Value,
                        Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Latitude = fLatitude,
                        Longitude = fLongitude,
                        Distance = int.Parse(x.Element("Distance").Value),
                        MapIcon = x.Element("MapIcon").Value,
                        Role = int.Parse(x.Element("Role").Value),
                        Description = x.Element("Description").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        NewPosts = int.Parse(x.Element("NewPosts").Value),
                        Type = int.Parse(x.Element("Type").Value),
                        HasRelations = int.Parse(x.Element("HasRelations").Value),
                        Parent = x.Element("Parent").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122")
                    });
                }
                catch(Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseSpotlightForMap:" + e.Message + ": " + e.InnerException);
                    float fLongitude;// = float.Parse(longitude);
                    float fLatitude;// = float.Parse(latitude);
                    float.TryParse(x.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                    float.TryParse(x.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                    Items.Add(new MapItem()
                    {
                        OndoId = int.Parse(x.Element("OndoId").Value),
                        Picture = x.Element("Picture").Value,
                        Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Latitude = fLatitude,
                        Longitude = fLongitude,
                        Distance = int.Parse(x.Element("Distance").Value),
                        MapIcon = x.Element("MapIcon").Value,
                        Role = int.Parse(x.Element("Role").Value),
                        Description = x.Element("Description").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        NewPosts = int.Parse(x.Element("NewPosts").Value),
                        Type = int.Parse(x.Element("Type").Value),
                        HasRelations = 0,
                        Parent = ""
                    });
                }
            }

            return Items;
        }

        public List<MapItem> ParseSpotlightForClusteredMap(XElement ondos)
        {
            List<MapItem> Items = new List<MapItem>();

            int unclustered;
            try
            {
                unclustered = int.Parse(ondos.Element("UnClustered").Value);
            }
            catch (Exception e)
            {
                return Items;
            }

            foreach (XElement x in ondos.Elements("ondo"))
            {
                try
                {
                    float fLongitude;// = float.Parse(longitude);
                    float fLatitude;// = float.Parse(latitude);
                    float.TryParse(x.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                    float.TryParse(x.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                    Items.Add(new MapItem()
                    {
                        OndoId = int.Parse(x.Element("OndoId").Value),
                        Picture = x.Element("Picture").Value,
                        Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Latitude = fLatitude,
                        Longitude = fLongitude,
                        Distance = int.Parse(x.Element("Distance").Value),
                        MapIcon = x.Element("MapIcon").Value,
                        Role = int.Parse(x.Element("Role").Value),
                        Description = x.Element("Description").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        NewPosts = int.Parse(x.Element("NewPosts").Value),
                        Type = int.Parse(x.Element("Type").Value),
                        HasRelations = int.Parse(x.Element("HasRelations").Value),
                        ClusterCount = int.Parse(x.Element("ClusterCount").Value),
                        UnClustered = unclustered,
                        Parent = x.Element("Parent").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        IsFavoriteInSingleFeed = (x.Element("IsFavoriteInSingleFeed").Value.ToString() == "") ? -1 : int.Parse(x.Element("IsFavoriteInSingleFeed").Value)
                    });
                }
                catch(Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseSpotlightForMap:" + e.Message + ": " + e.InnerException);
                    float fLongitude;// = float.Parse(longitude);
                    float fLatitude;// = float.Parse(latitude);
                    float.TryParse(x.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                    float.TryParse(x.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                    Items.Add(new MapItem()
                    {
                        OndoId = int.Parse(x.Element("OndoId").Value),
                        Picture = x.Element("Picture").Value,
                        Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Latitude = fLatitude,
                        Longitude = fLongitude,
                        Distance = int.Parse(x.Element("Distance").Value),
                        MapIcon = x.Element("MapIcon").Value,
                        Role = int.Parse(x.Element("Role").Value),
                        Description = x.Element("Description").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                        Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        NewPosts = int.Parse(x.Element("NewPosts").Value),
                        Type = int.Parse(x.Element("Type").Value),
                        HasRelations = 0,
                        ClusterCount = 0,
                        UnClustered = 0,
                        Parent = ""
                    });

                }
            }

            return Items;
        }

        public List<ShakeItem> ParseShakeItems(XElement shakelist)
        {
            List<ShakeItem> Items = new List<ShakeItem>();

            foreach (XElement x in shakelist.Elements("shakeitem"))
            {
                try
                {
                    Items.Add(new ShakeItem()
                    {
                        OndoId = int.Parse(x.Element("OndoId").Value),
                        BenefitId = int.Parse(x.Element("BenefitId").Value)
                    });
                }
                catch(Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseShakeItems:" + e.Message + ": " + e.InnerException);
                }
            }

            return Items;
        }

        public GameResultItem ParseGameResult(XElement gameresult)
        {
            GameResultItem Item = new GameResultItem();

            try
            {
                Item.Balance = int.Parse(gameresult.Element("Balance").Value);
                Item.LastScore = int.Parse(gameresult.Element("LastScore").Value);
                Item.RemainTurns = int.Parse(gameresult.Element("RemainTurns").Value);
                Item.Score = int.Parse(gameresult.Element("Score").Value);
                Item.GameStatus = int.Parse(gameresult.Element("GameStatus").Value);
                Item.NextGame = gameresult.Element("NextGame").Value;
                Item.Items = int.Parse(gameresult.Element("Items").Value);
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseGameResult:" + e.Message + ": " + e.InnerException);
            }

            return Item;
        }

        public StartGameIResultItem ParseStartGameResult(XElement startgameresult)
        {
             StartGameIResultItem Item = new StartGameIResultItem();

            try
            {
                double amount;
                double.TryParse(startgameresult.Element("Amount").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out amount);

                Item.Amount = amount;
                Item.MerchantId = startgameresult.Element("MerchantId").Value;
                Item.MiniPicture = startgameresult.Element("MiniPicture").Value;
                Item.OrderId = int.Parse(startgameresult.Element("OrderId").Value);
                Item.Result = int.Parse(startgameresult.Element("Result").Value);
                Item.ResultString = startgameresult.Element("ResultString").Value;
                Item.Title = startgameresult.Element("Title").Value;
                Item.ReceiptMessage = startgameresult.Element("ReceiptMessage").Value;
    }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseStartGameResult:" + e.Message + ": " + e.InnerException);
            }

            return Item;
        }

        public List<GameHighScoreItem> ParseGameHighScore(XElement highscore)
        {
            List<GameHighScoreItem> Items = new List<GameHighScoreItem>();

            foreach (XElement x in highscore.Elements("User"))
            {
                try
                {
                    Items.Add(new GameHighScoreItem()
                    {
                        Balance = int.Parse(x.Element("Balance").Value),
                        UserId = int.Parse(x.Element("UserId").Value),
                        Picture = x.Element("Picture").Value,
                        UserName = x.Element("UserName").Value
                    });
                }
                catch (Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseGameHighScore:" + e.Message + ": " + e.InnerException);
                }
            }
            return Items;
        }

        public List<MemberItem> ParseMembers(XElement memberItem)
        {
            List<MemberItem> Items = new List<MemberItem>();

            foreach (XElement x in memberItem.Elements("Member"))
            {
                try
                {
                    Items.Add(new MemberItem()
                    {
                        UserId = int.Parse(x.Element("UserId").Value),
                        Picture = x.Element("Picture").Value,
                        UserName = x.Element("UserName").Value,
                        Role = int.Parse(x.Element("Role").Value)
                    });
                }
                catch (Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseMembers:" + e.Message + ": " + e.InnerException);
                }
            }
            return Items;
        }

        public List<NotificationItem> ParseNotificationList(XElement notificationList)
        {
            List<NotificationItem> Items = new List<NotificationItem>();
            int count = 0;

            string checksum = notificationList.Element("CheckSum").Value;
            int total = int.Parse(notificationList.Element("Total").Value);
            int totalB = int.Parse(notificationList.Element("TotalBenefit").Value);
            int favorite = (notificationList.Element("FavoriteOndo").Value.ToString() == "") ? 0 : int.Parse(notificationList.Element("FavoriteOndo").Value);
            int totalFav = (notificationList.Element("TotalFavorites").Value.ToString() == "") ? 0 : int.Parse(notificationList.Element("TotalFavorites").Value);
            int favoriteType = (notificationList.Element("FavoriteType").Value.ToString() == "") ? 0 : int.Parse(notificationList.Element("FavoriteType").Value);

            foreach (XElement x in notificationList.Elements("ondo"))
            {
                try
                {
                    Items.Add(new NotificationItem()
                    {
                        OndoId = int.Parse(x.Element("OndoId").Value),
                        NewNotifications = int.Parse(x.Element("NewNotifications").Value),
                        CheckSum = checksum,
                        Total = total,
                        TotalBenefit = totalB,
                        FavoriteOndo = favorite,
                        TotalFavorites = totalFav,
                        FavoriteType = favoriteType
                    });
                }
                catch (Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseNotificationList:" + e.Message + ": " + e.InnerException);
                }

                count++;
            }

            if (count == 0)
            {
                Items.Add(new NotificationItem()
                {
                    OndoId = -1,
                    NewNotifications = 0,
                    CheckSum = checksum,
                    Total = 0,
                    TotalBenefit = 0,
                    FavoriteOndo = 0,
                    TotalFavorites = 0,
                    FavoriteType = 0
                });
            }

            return Items;
        }

        public List<WalletItem> ParseWalletItems(XElement wallet)
        {
            List<WalletItem> Items = new List<WalletItem>();
            foreach (XElement x in wallet.Elements("walletitem"))
            {
                try
                {
                    Items.Add(new WalletItem()
                    {
                        Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                                         Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Picture = x.Element("Picture").Value,
                        Balance = int.Parse(x.Element("Balance").Value),
                        BenefitId = int.Parse(x.Element("BenefitId").Value),
                        Type = int.Parse(x.Element("Type").Value),
                        BenefitType = int.Parse(x.Element("BenefitType").Value),
                        Role = int.Parse(x.Element("Role").Value),
                        OndoId = int.Parse(x.Element("OndoId").Value),
                        OndoTitle = x.Element("OndoTitle").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                                         Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        BalanceText = x.Element("BalanceText").Value,
                        CheckSumNode = x.Element("CheckSumNode").Value,
                        CanDelete = (x.Element("CanDelete").Value.ToString() == "") ? 0 : int.Parse(x.Element("CanDelete").Value),
                        Cardnr = x.Element("Cardnr").Value,
                        Index = (x.Element("Index").Value.ToString() == "") ? -1 : int.Parse(x.Element("Index").Value),
                        CanEnterCode = (x.Element("CanEnterCode").Value.ToString() == "") ? -1 : int.Parse(x.Element("CanEnterCode").Value),
                        NewPosts = (x.Element("NewPosts").Value.ToString() == "") ? -1 : int.Parse(x.Element("NewPosts").Value)
                    });
                }
                catch
                {
                }
            }

            return Items;
        }

        /*
         - <ondoshakebenefit xmlns="">
  <Title>TB1Test XXXXXXXXXXXXXXXXX</Title> 
  <Description>This text should explain the purpose of the Ondo you are creating XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XX</Description> 
  <Latitude>56.102</Latitude> 
  <Longitude>9.556</Longitude> 
  <UserName>TB1Test og lidt XXXXXXXXX</UserName> 
  <Picture>http://ondodotsservice.blob.core.windows.net/content/f20064c9-6391-4c2d-847e-45f3168fd578.jpg?10</Picture> 
  <MiniPicture>http://ondodotsservice.blob.core.windows.net/benefit/a66e6f04-f03e-4e3b-afff-ce353ecb0a3d.jpg</MiniPicture> 
  </ondoshakebenefit>
         */


        public ShakeItem ParseShakeItem(XElement shakeitem)
        {
            ShakeItem Item = new ShakeItem();

            try
            {
                float fLongitude;// = float.Parse(longitude);
                float fLatitude;// = float.Parse(latitude);
                float.TryParse(shakeitem.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                float.TryParse(shakeitem.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);

                Item.Title = shakeitem.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Description = shakeitem.Element("Description").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Latitude = fLatitude;
                Item.longitude = fLongitude;
                Item.MiniPicture = shakeitem.Element("MiniPicture").Value;
                Item.Stamp = shakeitem.Element("Stamp").Value;
                Item.Picture = shakeitem.Element("Picture").Value;
                Item.UserName = shakeitem.Element("UserName").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Role = int.Parse(shakeitem.Element("Role").Value);
                Item.MapIcon = shakeitem.Element("MapIcon").Value;
                
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseShakeItem:" + e.Message + ": " + e.InnerException);
            }


            return Item;
        }

        public List<PostRelationItem> ParsePostRelationItems(XElement postrelations)
        {
            List<PostRelationItem> Items = new List<PostRelationItem>();
            foreach (XElement x in postrelations.Elements("relation"))
            {
                try
                {
                    Items.Add(new PostRelationItem()
                    {
                        CommentId = int.Parse(x.Element("CommentId").Value),
                        NumberOfComments = int.Parse(x.Element("NumberOfComments").Value),
                        NumberOfNewComments = int.Parse(x.Element("NumberOfNewComments").Value),
                    });
                }
                catch
                {
                }
            }

            return Items;
        }

        public BenefitProgramItem ParseBenefitProgramItem(XElement program)
        {
            // (e == null) ? null : e.x
            BenefitProgramItem Item = new BenefitProgramItem();

            try
            {
                Item.BenefitType = (program.Element("BenefitType").Value.ToString() == "") ? 0 : int.Parse(program.Element("BenefitType").Value);
                Item.PointValue = (program.Element("PointValue").Value.ToString() == "") ? 0 : int.Parse(program.Element("PointValue").Value);
                Item.ValidDays = (program.Element("ValidDays").Value.ToString() == "") ? 0 : int.Parse(program.Element("ValidDays").Value);
                Item.Type = (program.Element("Type").Value.ToString() == "") ? 0 : int.Parse(program.Element("Type").Value);
                Item.Currency = program.Element("Currency").Value.ToString();
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseBenefitProgramItem:" + e.Message + ": " + e.InnerException);
            }


            return Item;
        }

        public BenefitProgramTransactionItem ParseBenefitProgramTransactionItem(XElement transaction)
        {
            BenefitProgramTransactionItem Item = new BenefitProgramTransactionItem();

            string test = "";

            try
            {
                float fLongitude;
                float fLatitude;
                float.TryParse(transaction.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                float.TryParse(transaction.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                Item.Latitude = fLatitude;
                Item.Longitude = fLongitude;
            }
            catch
            {
            }

            try
            {
                Item.Result = (transaction.Element("Result").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("Result").Value);
                test = "Result";
                Item.Balance = (transaction.Element("Balance").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("Balance").Value);
                test = "Balance";
                Item.Height = (transaction.Element("Height").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("Height").Value);
                test = "Height";
                Item.Width = (transaction.Element("Width").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("Width").Value);
                test = "Width";
                Item.Picture = transaction.Element("Picture").Value.ToString();
                test = "Picture";
                Item.Text = transaction.Element("Text").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"); ;
                test = "Text";
                Item.BarCodePicture = transaction.Element("BarCodePicture").Value.ToString();
                test = "BarCodePicture";
                Item.UserName = transaction.Element("UserName").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"); ;
                test = "UserName";
                Item.UserPicture = transaction.Element("UserPicture").Value.ToString();
                test = "UserPicture";
                Item.BenefitType = (transaction.Element("BenefitType").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("BenefitType").Value);
                test = "BenefitType";
                Item.Type = (transaction.Element("Type").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("Type").Value);
                test = "Type";
                Item.Stamp = transaction.Element("Stamp").Value.ToString();
                test = "Stamp";
                Item.Time = transaction.Element("Time").Value.ToString();
                test = "Time";
                Item.BenefitPicture = transaction.Element("BenefitPicture").Value.ToString();
                test = "BenefitPicture";
                Item.BenefitHeight = (transaction.Element("BenefitHeight").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("BenefitHeight").Value);
                test = "BenefitHeight";
                Item.BenefitWidth = (transaction.Element("BenefitWidth").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("BenefitWidth").Value);
                test = "BenefitWidth";
                Item.Sheets = (transaction.Element("Sheets").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("Sheets").Value);
                test = "Sheets";
                Item.Title = transaction.Element("Title").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                test = "Title";
                Item.BarCodeString = transaction.Element("BarCodeString").Value.ToString();
                test = "BarCodeString";
                Item.Conditions = transaction.Element("Conditions").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                test = "Conditions";
                Item.StampRedeem = transaction.Element("StampRedeem").Value.ToString();
                test = "StampRedeem";
                Item.ResultString = transaction.Element("ResultString").Value.ToString();
                test = "ResultString";
                Item.BenefitId = (transaction.Element("BenefitId").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("BenefitId").Value);
                test = "BenefitId";
                Item.UserId = (transaction.Element("UserId").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("UserId").Value);
                test = "UserId";
                Item.HasLocations = (transaction.Element("HasLocations").Value.ToString() == "") ? -1 : int.Parse(transaction.Element("HasLocations").Value);
                test = "HasLocations";
            }
            catch (Exception e)
            {
                Item.Result = -1;
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("*******ITEM*********" + test);
                handler.logEvent("ParseBenefitProgramTransactionItem:" + e.Message + ": " + e.InnerException);
            }


            return Item;
        }

        public List<UserTransactionItem> ParseUserTransactions(XElement transactions)
        {
            // (e == null) ? null : e.x
            List<UserTransactionItem> Items = new List<UserTransactionItem>();

            try
            {
                foreach (XElement x in transactions.Elements("transaction"))
                {
                    Items.Add(new UserTransactionItem()
                    {
                        OndoId = (x.Element("OndoId").Value.ToString() == "") ? -1 : int.Parse(x.Element("OndoId").Value),
                        Points = (x.Element("Points").Value.ToString() == "") ? -1 : int.Parse(x.Element("Points").Value),
                        DateTime = x.Element("DateTime").Value.ToString(),
                        Title = x.Element("Title").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Type = (x.Element("Type").Value.ToString() == "") ? -1 : int.Parse(x.Element("Type").Value),
                        Balance = (x.Element("Balance").Value.ToString() == "") ? 0 : int.Parse(x.Element("Balance").Value),
                        UnitType = (x.Element("UnitType").Value.ToString() == "") ? 0 : int.Parse(x.Element("UnitType").Value),
                        UnitText = x.Element("UnitText").Value.ToString(),
                        Role = (x.Element("Role").Value.ToString() == "") ? -1 : int.Parse(x.Element("Role").Value)
                    });
                }
                
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseBenefitProgramItem:" + e.Message + ": " + e.InnerException);
            }


            return Items;
        }

        public List<OndoSearchItem> ParseOndoSearchItems(XElement searchItems)
        {
            // (e == null) ? null : e.x
            List<OndoSearchItem> Items = new List<OndoSearchItem>();

            try
            {
                foreach (XElement x in searchItems.Elements("ondo"))
                {
                    double dLongitude;
                    double dLatitude;
                    double.TryParse(x.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out dLatitude);
                    double.TryParse(x.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out dLongitude);
                    Items.Add(new OndoSearchItem()
                    {
                        OndoId = (x.Element("OndoId").Value.ToString() == "") ? -1 : int.Parse(x.Element("OndoId").Value),
                        Picture = x.Element("Picture").Value.ToString(),
                        Title = x.Element("Title").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        PreText = x.Element("PreText").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        PostText = x.Element("PostText").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        SearchText = x.Element("SearchText").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Type = (x.Element("Type").Value.ToString() == "") ? -1 : int.Parse(x.Element("Type").Value),
                        Latitude = dLatitude,
                        Longitude = dLongitude,
                        MapIcon = x.Element("MapIcon").Value.ToString(),
                        Role = (x.Element("Role").Value.ToString() == "") ? -1 : int.Parse(x.Element("Role").Value),
                        HasRelations = (x.Element("HasRelations").Value.ToString() == "") ? -1 : int.Parse(x.Element("HasRelations").Value),
                        Parent = x.Element("Parent").Value.ToString(),
                        HashPreText = x.Element("HashPreText").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                           Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        HashPostText = x.Element("HashPostText").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        HashSearchText = x.Element("HashSearchText").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                    });
                }

            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseBenefitProgramItem:" + e.Message + ": " + e.InnerException);
            }


            return Items;
        }

        public VerifyUserItem ParseVerifyUserItem(XElement verifyItem)
        {
            // (e == null) ? null : e.x
            VerifyUserItem Item = new VerifyUserItem();

            try
            {
                Item.Result = (verifyItem.Element("Result").Value.ToString() == "") ? 0 : int.Parse(verifyItem.Element("Result").Value);
                Item.ResultString = verifyItem.Element("ResultString").Value.ToString();
                Item.Phone = verifyItem.Element("Phone").Value.ToString();
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseVerifyUserItem:" + e.Message + ": " + e.InnerException);
            }


            return Item;
        }

        public ConfigurationItem ParseConfigurationItem(XElement configurationItem)
        {
            // (e == null) ? null : e.x
            ConfigurationItem Item = new ConfigurationItem();

            try
            {
                Item.AppId = (configurationItem.Element("AppId").Value.ToString() == "") ? 0 : int.Parse(configurationItem.Element("AppId").Value);
                Item.AppName = configurationItem.Element("AppName").Value.ToString();
                Item.ActionByte = (configurationItem.Element("ActionByte").Value.ToString() == "") ? 0 : int.Parse(configurationItem.Element("ActionByte").Value);
                Item.CheckSum = configurationItem.Element("CheckSum").Value.ToString();
                Item.StartUpView = (configurationItem.Element("StartUpView").Value.ToString() == "") ? 0 : int.Parse(configurationItem.Element("StartUpView").Value);
                Item.AboutLink = configurationItem.Element("AboutLink").Value.ToString();
                Item.PrivacyLink = configurationItem.Element("PrivacyLink").Value.ToString();
                Item.TermsLink = configurationItem.Element("TermsLink").Value.ToString();
                Item.MapIconContact = configurationItem.Element("MapIconContact").Value.ToString();
                Item.MapIconMember = configurationItem.Element("MapIconMember").Value.ToString();
                Item.MapIconViewer = configurationItem.Element("MapIconViewer").Value.ToString();
                Item.MapIconOwner = configurationItem.Element("MapIconOwner").Value.ToString();
                Item.HeaderColor = configurationItem.Element("HeaderColor").Value.ToString();
                Item.BarColor = configurationItem.Element("BarColor").Value.ToString();
                Item.ServerType = (configurationItem.Element("ServerType").Value.ToString() == "") ? 0 : int.Parse(configurationItem.Element("ServerType").Value);
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseConfigurationItem:" + e.Message + ": " + e.InnerException);
            }

            return Item;
        }

        public TransactionDetailsItem ParseTransactionDetails(XElement transactionDetailsItem)
        {
            TransactionDetailsItem Item = new TransactionDetailsItem();

            try
            {
                Item.MiniPicture = transactionDetailsItem.Element("MiniPicture").Value.ToString();
                Item.Title = transactionDetailsItem.Element("Title").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");;
                Item.Text = transactionDetailsItem.Element("Text").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"); ;
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseTransactionDetails:" + e.Message + ": " + e.InnerException);
            }

            return Item;
        }

		public PaymentValidationItem ParseTransactionValidation(XElement transactionDetailsItem)
        {
            PaymentValidationItem Item = new PaymentValidationItem();

            try
            {
                Item.ResultString = transactionDetailsItem.Element("ResultString").Value.ToString();
                Item.ErrorCode = (transactionDetailsItem.Element("ErrorCode").Value.ToString() == "") ? -1 : int.Parse(transactionDetailsItem.Element("ErrorCode").Value);
                Item.OrderId = (transactionDetailsItem.Element("OrderId").Value.ToString() == "") ? -1 : int.Parse(transactionDetailsItem.Element("OrderId").Value);
                Item.Result = (transactionDetailsItem.Element("Result").Value.ToString() == "") ? -1 : int.Parse(transactionDetailsItem.Element("Result").Value);
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseTransactionValidation:" + e.Message + ": " + e.InnerException);
            }

            return Item;
        }

        public List<ClubItem> ParseClublist(XElement list)
        {
            List<ClubItem> Items = new List<ClubItem>();
            foreach (XElement x in list.Elements("activityid"))
            {
                try
                {
                    if (x.HasElements)
                    {
                        Items.Add(new ClubItem()
                        {
                            ActivityId = int.Parse(x.Element("ActivityId").Value),
                            BenefitId = int.Parse(x.Element("BenefitId").Value),
                            Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Description = x.Element("Description").Value.Replace("&amp;", "&").Replace("amp;", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Choice = (x.Element("Choice").Value.ToString() == "") ? -1 : int.Parse(x.Element("Choice").Value)
                        });
                    }
                }
                catch (Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseClublist:" + e.Message + ": " + e.InnerException);
                }
            }

            return Items;
        }

        public List<OndoRelationItem> ParseRelationItems(XElement list)
        {
            List<OndoRelationItem> Items = new List<OndoRelationItem>();
            foreach (XElement x in list.Elements("ondo"))
            {
                try
                {
                    if (x.HasElements)
                    {
                        Items.Add(new OndoRelationItem()
                        {
                            OndoId = int.Parse(x.Element("OndoId").Value),
                            Title = x.Element("Title").Value.Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            Description = x.Element("Description").Value.Replace("&amp;", "&").Replace("amp;", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                            Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                            MiniPicture = x.Element("MiniPicture").Value
                        });
                    }
                }
                catch (Exception e)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
                    handler.logEvent("ParseRelationItems:" + e.Message + ": " + e.InnerException);
                }
            }

            return Items;
        }

#endregion

#region nets

public BenefitProgramNetsItem ParseBenefitProgramNetsItem(XElement netsItem)
        {
            BenefitProgramNetsItem Item = new BenefitProgramNetsItem();
         
            try
            {
                float fLongitude;
                float fLatitude;
                float.TryParse(netsItem.Element("Latitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLatitude);
                float.TryParse(netsItem.Element("Longitude").Value.Replace(',', '.'), System.Globalization.NumberStyles.Any, new System.Globalization.CultureInfo("en-US"), out fLongitude);
                Item.Latitude = fLatitude;
                Item.Longitude = fLongitude;
            }
            catch
            {
            }


            try
            {
                Item.CardStatus = (netsItem.Element("CardStatus").Value.ToString() == "") ? -1 : int.Parse(netsItem.Element("CardStatus").Value);
                Item.ResultValue = (netsItem.Element("ResultValue").Value.ToString() == "") ? -1 : int.Parse(netsItem.Element("ResultValue").Value);
                Item.ResultString = netsItem.Element("ResultString").Value.ToString();
                Item.BarCodePicture = netsItem.Element("BarCodePicture").Value.ToString();
                Item.OndoResult = (netsItem.Element("OndoResult").Value.ToString() == "") ? -1 : int.Parse(netsItem.Element("OndoResult").Value);
                Item.Balance = netsItem.Element("Balance").Value.ToString();
                Item.ExpiryDate = netsItem.Element("ExpiryDate").Value.ToString();
                Item.Title = netsItem.Element("Title").Value.ToString().ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Text = netsItem.Element("Text").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Conditions = netsItem.Element("Conditions").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                    Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122");
                Item.Cardnr = netsItem.Element("Cardnr").Value.ToString();
                Item.BarcodeLength = (netsItem.Element("BarcodeLength").Value.ToString() == "") ? -1 : int.Parse(netsItem.Element("BarcodeLength").Value);
                Item.HasLocations = (netsItem.Element("HasLocations").Value.ToString() == "") ? -1 : int.Parse(netsItem.Element("HasLocations").Value);
                Item.PicHeight = int.Parse(netsItem.Element("PicHeight").Value);
                Item.PicWidth = int.Parse(netsItem.Element("PicWidth").Value);
                Item.ActionByte = int.Parse(netsItem.Element("ActionByte").Value);
            }
            catch
            {
            }

            return Item;
        }

        public List<NetsTransactionItem> ParseUserNetsTransactions(XElement transactions)
        {
            // (e == null) ? null : e.x
            List<NetsTransactionItem> Items = new List<NetsTransactionItem>();

            try
            {
                foreach (XElement x in transactions.Elements("transaction"))
                {
                    Items.Add(new NetsTransactionItem()
                    {
                        Amount = x.Element("Amount").Value.ToString(),
                        TransactionId = (x.Element("TransactionId").Value.ToString() == "") ? -1 : int.Parse(x.Element("TransactionId").Value),
                        DateTime = x.Element("DateTime").Value.ToString(),
                        Title = x.Element("Title").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122"),
                        Type = x.Element("Type").Value.ToString()
                    });
                }
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseBenefitProgramItem:" + e.Message + ": " + e.InnerException);
            }


            return Items;
        }

        public List<CardListItem> ParseCardList(XElement cardlist)
        {
            // (e == null) ? null : e.x
            List<CardListItem> Items = new List<CardListItem>();

            try
            {
                foreach (XElement x in cardlist.Elements("program"))
                {
                    Items.Add(new CardListItem()
                    {
                        Stamp = x.Element("Stamp").Value.ToString(),
                        BenefitId = (x.Element("BenefitId").Value.ToString() == "") ? -1 : int.Parse(x.Element("BenefitId").Value),
                        Title = x.Element("Title").Value.ToString().Replace("&amp;", "&").Replace("&lt;", "<").Replace("&gt;", ">").Replace("&quot;", "\"").Replace("&apos;", "'").Replace("&amp;", "&").
                                Replace("(R)", "\u00AE").Replace("(C)", "\u00A9").Replace("(TM)", "\u2122")
                    });
                }
            }
            catch (Exception e)
            {
                OndoStorageHandler handler = new OndoStorageHandler();
                handler.logEvent("ParseUCardList:" + e.Message + ": " + e.InnerException);
            }

            return Items;
        }
#endregion

#region Clustering

#if !(WINDOWS_PHONE) && !(WINDOWS)
public void ParseClusteringServerResponse(object obj, ObservableCollection<MapItem> items) 
        {
                       
            try
            {
            var error = obj.GetType().GetProperty("Error").GetValue(obj, null);
            System.Xml.XmlNode result = (System.Xml.XmlNode)obj.GetType().GetProperty("Result").GetValue(obj, null);
       
            try
            {
                if (error == null)
                {
                    OndoXElementHelper helper = new OndoXElementHelper();
                    XElement elem = helper.GetXElement(result);

                    List<MapItem> tmpItems = helper.ParseSpotlightForClusteredMap(elem);

                    foreach (MapItem ondoElement in tmpItems)
                    {
                        items.Add(new MapItem()
                        {
                            OndoId = ondoElement.OndoId,
                            Title = ondoElement.Title,
                            Picture = ondoElement.Picture,
                            Longitude = ondoElement.Longitude,
                            Latitude = ondoElement.Latitude,
                            Distance = ondoElement.Distance,
                            MapIcon = ondoElement.MapIcon,
                            Role = ondoElement.Role,
                            Description = ondoElement.Description,
                            Type = ondoElement.Type,
                            HasRelations = ondoElement.HasRelations,
                            ClusterCount = ondoElement.ClusterCount,
                            UnClustered = ondoElement.UnClustered,
                            NewPosts = Convert.ToInt32(ondoElement.NewPosts),
                            IsFavoriteInSingleFeed = ondoElement.IsFavoriteInSingleFeed


                        });
                    }

#if __ANDROID__
                    if (items.Count > 0)
                    {
                        OndoStorageHandler handler = new OndoStorageHandler();
                        handler.StoreMapTagItems(tmpItems);
                    }
#endif
                }

                else
                {

                }
            }
            catch
            {
            }
         }
         catch {}

           
        }
#endif
        #endregion

    }
}
