#if MONOTOUCH
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace SharedOndoLibrary.Helpers
{
    public class CategoryHelper
    {
        public void GetCategory(string key, out string major, out string sub)
        {
            try
            {

                eMajorCategory majortype = (eMajorCategory)Enum.Parse(typeof(eMajorCategory), ((key.Substring(0, 2)).ToString() + "000"));

                major = majortype.ToString();
            }
            catch
            {
                major = eMajorCategory.Other.ToString();
            }


            try
            {

                eSubCategory subtype = (eSubCategory)Enum.Parse(typeof(eSubCategory), key);

                sub = subtype.ToString();
            }
            catch
            {
                sub = eSubCategory.Unknown.ToString();
            }
        }

        public bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            return true;
        }

        public bool IsMajor(string value)
        {
            bool return_val;

            if (value.Length == 4)
            {
                return_val = value.Substring(1, 3) == "000" ? true : false;
            }
            else
            {
                return_val = value.Substring(2, 3) == "000" ? true : false;
            }
            return return_val;
        }

        public enum eSubCategory
        {

            IslandOrSmallCommunity = 01001,
            CityOrTradeOfCommerce = 01002,
            Sport = 01003,
            Festival = 01004,
            ProductOrSupplier = 01005,
            OtherSpot = 01999,

            HouseAndgarden = 02001,
            ClothesShoesAndBags = 02002,
            ITAndElectronic = 02003,
            GroceryStore = 02004,
            SportHobbyAndLeasure = 02005,
            DelicaciesAndWine = 02006,
            BeautyCareAndWellness = 02007,
            OpticianWatchesAndJewelery = 02008,


            Merchandise = 02999,


            Restaurant = 03001,
            Cafe = 03002,
            Bar = 03003,
            Fastfood = 03004,
            IceCream = 03005,
            OtherFood = 03999,

            Hotel = 04001,
            Motel = 04002,
            BedAndBreakfast = 04003,
            Hostel = 04004,
            Camping = 04005,
            OtherAccomodation = 04999,

            Museum = 05001,
            ThemePark = 05002,
            MusicTheaterCinema = 05003,
            PointOfInterest = 05999,

            SportClub = 06001,
            OtherAssociation = 06999,


            Craftsman = 07001,
            ProfessionalServices = 07002,
            OtherServices = 07999,


            Parking = 88001,
            Restroom = 88002,
            Information = 88003,
            Disability = 88004,
            FirstAid = 88005,
            Meetingpoint = 88006,
            Shower = 88007,
            Kitchen = 88008,
            Nurseryroom = 88009,
            Tapwater = 88010,
            Camping1 = 88011,
            Recycle = 88012,
            Playground = 88013,
            PublicBeach = 88014,
            Praying = 88015,
            TrainBusAndTransport = 88016,
            ATM = 88017,
            Other = 88999,

            Private = 89001,
            Public = 89002,
            Social = 89003,
            School = 89004,
            Unknown = 89999,

            FellowShip = 90001,


        }

        public enum eMajorCategory
        {
            SpotPoint = 01000,
            Store = 02000,
            FoodAndDrinks = 03000,
            Accomodation = 04000,
            ArtAndEntertainment = 05000,
            AssociationsAndClubs = 06000,
            Business = 07000,
            Infrastructure = 88000,
            Other = 89000,
            OwnOndo = 90000
        }
    }
}
#endif