﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using SharedOndoLibrary.Data;


namespace SharedOndoLibrary.Constants
{
    public class OndoConstants
    {
        public int YoungsterAge = 13;
        public int FirstTimeUserage = 30;
        public int FirstTimeUseryear = 1980;
        public DateTime StartingDate = DateTime.Parse("1/1/1980");

        public int MinUserNameLength = 2;
        public int MaxUserNameLength = 30;

        public int MinPasswordLength = 6;
        public int MaxPasswordLength = 20;
      
        public int MaxUserBioLength = 80;

        public int MaxAge = 99;

        /* Currently position of Saint Helena*/
        public const double NoGPSSignalLatitude = -15.960834;
        public const double NoGPSSignalLongitude = -5.709114;

        public readonly Regex WebPattern = new Regex(@"^(|https?:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)$");


#if __ANDROID__
        public const string MatchEmailPattern = @"^\w[\w.-]*@(\w[\w-]*\.)+[a-z]{2,4}$";
#else

        //  public const string MatchEmailPattern = @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@" + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\." + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|" + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

        public const string MatchEmailPattern = @"^\w[\w.-]*@(\w[\w-]*\.)+[a-z]{2,4}$";
#endif


        public const string MatchUserNamePattern = @"/^[a-z0-9_-]{3,16}$/";

        public const string MatchUrl = @"/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/";
        public const string MatchWebAdress = @"^(|https?:\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?)$";

        public string ServerKey = "#bispensgipsgebis#";
        public string QRCodeKey = "fnDrigjAsem";
        public string QRCodeSalt = "jsfhFhfser";

        static OndoData ondoData = new OndoData();

        public OndoData CurrentOndoData
        {
            get { return ondoData; }
            set { ondoData = value; }
        }
#if __ANDROID__
        public bool IsEmail(string email)
        {
            Regex pattern = new Regex(MatchEmailPattern, RegexOptions.IgnoreCase | RegexOptions.ECMAScript);
           
            if (pattern.IsMatch(email))
            {
                return true;
            }

            return false;
        }
#else
        public bool IsEmail(string email)
        {
            if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
            else return false;
        }

        public bool IsUri(string uri)
        {
            if (uri != null) return Regex.IsMatch(uri, MatchUrl);
            else return false;
        }

        public bool IsWebAddress(string web)
        {
            if (web != null) return Regex.IsMatch(web, MatchWebAdress);
            else return false;
        }

#endif
    }
}