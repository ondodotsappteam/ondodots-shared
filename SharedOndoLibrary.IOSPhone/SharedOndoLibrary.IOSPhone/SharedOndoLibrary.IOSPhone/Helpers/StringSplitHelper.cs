﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Helpers
{
    public class StringSplitHelper
    {
        public void StringSplitHelp(string stringToSearchIn, string searchString, out string pre, out string post)
        {
            string[] text = stringToSearchIn.Split(new string[] { searchString }, StringSplitOptions.None);
            pre = "";
            post = "";
            string title = stringToSearchIn;

            if (text.Length > 1)
            {
                pre = text[0];
                post = text[1];
            }
            else if (searchString.Length > 0)
            {
                pre = "";
                post = text[0];
            }
            if (text.Length > 2)
            {
                for (int ii = 2; ii < text.Length; ii++)
                {
                    post += searchString + text[ii];
                }
            }
        }



        public string formatHyperLink(string text, out int offset, bool openlink)
        {

            offset = 0;

            string hyperlink = "";

            string helpstring = text.ToLower();

            int helpindex = 0;


            // Check if text contains hyperlink


            if (helpstring.StartsWith("http://") || helpstring.StartsWith("https://") || (helpstring.StartsWith("www.")))
            {
                if (text.IndexOf(",") != -1)
                {
                    hyperlink = text.Substring(0, text.IndexOf(","));
                }
                else if (text.IndexOf(" ") != -1)
                {
                    helpindex = text.IndexOf(" ");
                    if (helpindex > 0)
                    {
                        if (text.Substring(helpindex - 1, 1) != ".")
                        {
                            hyperlink = text.Substring(0, text.IndexOf(" "));
                        }
                        else
                        {
                            hyperlink = text.Substring(0, helpindex - 1);
                        }
                    }
                }
                else if (text.IndexOf("\n") != -1)
                {
                    helpindex = text.IndexOf("\n");
                    if (helpindex > 0)
                    {
                        if (text.Substring(helpindex - 1, 1) != ".")
                        {
                            hyperlink = text.Substring(0, text.IndexOf("\n"));
                        }
                        else
                        {
                            hyperlink = text.Substring(0, helpindex - 1);
                        }
                    }
                }
                else hyperlink = text;

                offset = 0;

                if ((openlink) && (helpstring.StartsWith("www.")))
                {
                    hyperlink = "http://" + hyperlink;
                }
            }
            else if ((helpstring.IndexOf("http://") != -1) || (helpstring.IndexOf("https://") != -1) || (helpstring.IndexOf("www.") != -1))
            {
                if (helpstring.IndexOf("http://") != -1)
                {
                    offset = helpstring.IndexOf("http");
                    text = text.Substring(helpstring.IndexOf("http"), text.Length - helpstring.IndexOf("http"));
                }
                else if (helpstring.IndexOf("https://") != -1)
                {
                    offset = text.IndexOf("https");
                    text = text.Substring(helpstring.IndexOf("https"), text.Length - helpstring.IndexOf("https"));
                }
                else if (helpstring.IndexOf("www.") != -1)
                {
                    offset = text.IndexOf("www");
                    text = text.Substring(helpstring.IndexOf("www"), text.Length - helpstring.IndexOf("www"));
                }

                if (text.IndexOf(",") != -1)
                {
                    hyperlink = text.Substring(0, text.IndexOf(","));
                }
                else if (text.IndexOf(" ") != -1)
                {
                    helpindex = text.IndexOf(" ");
                    if (helpindex > 0)
                    {
                        if (text.Substring(helpindex - 1, 1) != ".")
                        {
                            hyperlink = text.Substring(0, text.IndexOf(" "));
                        }
                        else
                        {
                            hyperlink = text.Substring(0, helpindex - 1);
                        }
                    }

                }
                else if (text.IndexOf("\n") != -1)
                {
                    helpindex = text.IndexOf("\n");
                    if (helpindex > 0)
                    {
                        if (text.Substring(helpindex - 1, 1) != ".")
                        {
                            hyperlink = text.Substring(0, text.IndexOf("\n"));
                        }
                        else
                        {
                            hyperlink = text.Substring(0, helpindex - 1);
                        }
                    }

                }
                else hyperlink = text;


            }

            else hyperlink = "";

            if (openlink)
            {
                int index = -1;
                helpstring = hyperlink;
                helpstring = helpstring.ToLower();
                string t = "";
                int len = 0;

                if (helpstring.Contains("http"))
                {
                    index = helpstring.IndexOf("http");

                    t = "http";
                    len = t.Length;
                }
                else if (helpstring.Contains("https"))
                {
                    index = helpstring.IndexOf("https");

                    t = "https";
                    len = t.Length;
                }
                else if (helpstring.Contains("www"))
                {
                    index = helpstring.IndexOf("www");
                    t = "http://" + "www";
                    len = 3;

                }


                if (index != -1)
                {
                    var stringBuilder = new StringBuilder(hyperlink);
                    stringBuilder.Remove(index, len);
                    stringBuilder.Insert(index, t);
                    hyperlink = stringBuilder.ToString();
                }
            }

            return hyperlink;
        }
    }
}
