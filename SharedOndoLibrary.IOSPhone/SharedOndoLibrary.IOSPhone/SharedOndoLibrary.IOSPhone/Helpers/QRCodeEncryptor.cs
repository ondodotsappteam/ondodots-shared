﻿using SharedOndoLibrary.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SharedOndoLibrary.Helpers
{
    public class QRCodeEncryptor
    {
        /// <summary>
        /// Encrypt a string using AES
        /// </summary>
        /// <param name="Str">String to encrypt</param>
        /// <returns>Encrypted string in case of success; otherwise - empty string</returns>
        public string EncryptString(string Str)
        {
            try
            {
                using (Aes aes = new AesManaged())
                {
                    OndoConstants constants = new OndoConstants();
                    Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes(constants.QRCodeKey, Encoding.UTF8.GetBytes(constants.QRCodeSalt));

                    aes.Key = deriveBytes.GetBytes(128 / 8); // new byte[] { 0xA0, 0x2F, 0xC0, 0x8D, 0x1B, 0x7A, 0x5B };
                    aes.IV = aes.Key;
                    using (MemoryStream encryptionStream = new MemoryStream())
                    {
                        using (CryptoStream encrypt = new CryptoStream(encryptionStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            byte[] utfD1 = UTF8Encoding.UTF8.GetBytes(Str);
                            encrypt.Write(utfD1, 0, utfD1.Length);
                            encrypt.FlushFinalBlock();
                        }
                        return Convert.ToBase64String(encryptionStream.ToArray());
                    }
                }
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// Decrypt encrypted string
        /// </summary>
        /// <param name="Str">Encrypted string</param>
        /// <returns>Decrypted string if success; otherwise - empty string</returns>
        public string DecryptString(string Str)
        {
            try
            {
                using (Aes aes = new AesManaged())
                {
                    OndoConstants constants = new OndoConstants();
                    Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes(constants.QRCodeKey, Encoding.UTF8.GetBytes(constants.QRCodeSalt));
                    aes.Key = deriveBytes.GetBytes(128 / 8); // new byte[] { 0xA0, 0x2F, 0xC0, 0x8D, 0x1B, 0x7A, 0x5B };
                    aes.IV = aes.Key;

                    using (MemoryStream decryptionStream = new MemoryStream())
                    {
                        using (CryptoStream decrypt = new CryptoStream(decryptionStream, aes.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            byte[] encryptedData = Convert.FromBase64String(Str);
                            decrypt.Write(encryptedData, 0, encryptedData.Length);
                            decrypt.Flush();
                        }
                        byte[] decryptedData = decryptionStream.ToArray();
                        return UTF8Encoding.UTF8.GetString(decryptedData, 0, decryptedData.Length);
                    }
                }
            }
            catch
            {
                return "";
            }
        }


    }

}
