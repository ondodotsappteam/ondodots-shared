﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Helpers
{
    public class DistanceHelper
    {
        public eFormattedDistance GetFormattedDistance(int distanceInMeters)
        {
            eFormattedDistance rc = eFormattedDistance.MoreThan100Km;

            if (distanceInMeters < 100000)
                rc = eFormattedDistance.Within100Km;
            if (distanceInMeters < 50000)
                rc = eFormattedDistance.Within50Km;
            if (distanceInMeters < 10000)
                rc = eFormattedDistance.Within10Km;
            if (distanceInMeters < 5000)
                rc = eFormattedDistance.Within5Km;
            if (distanceInMeters < 1000)
                rc = eFormattedDistance.Within1Km;
            if (distanceInMeters < 500)
                rc = eFormattedDistance.Within500M;
            if (distanceInMeters < 100)
                rc = eFormattedDistance.Within100M;

            return rc;
        }

        public enum eFormattedDistance
        {
            Within100M = 1,
            Within500M = 2,
            Within1Km = 3,
            Within5Km = 4,
            Within10Km = 5,
            Within50Km = 6,
            Within100Km = 7,
            MoreThan100Km = 8
        }

        public double calcdistance(double latitude1, double longitude1, double latitude2, double longitude2)
        {
            // Returns distance in Meter

            const double PI = 3.1415926538;
            const int earthradius = 6371000; // In meter

            double e = (PI * latitude1 / 180);
            double f = (PI * longitude1 / 180);
            double g = (PI * latitude2 / 180);
            double h = (PI * longitude2 / 180);
            double i = (Math.Cos(e) * Math.Cos(g) * Math.Cos(f) * Math.Cos(h) + Math.Cos(e) * Math.Sin(f) *
                          Math.Cos(g) * Math.Sin(h) + Math.Sin(e) * Math.Sin(g));
            double j = (Math.Acos(i));
            double k = (earthradius * j);

            return k;

        }
    }

   
}
