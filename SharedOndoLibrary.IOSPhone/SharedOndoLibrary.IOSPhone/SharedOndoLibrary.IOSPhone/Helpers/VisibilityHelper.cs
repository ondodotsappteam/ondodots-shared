﻿using SharedOndoLibrary.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Helpers
{
    public class VisibilityHelper
    {
        public void GetVisibilitySettings(OndoXElementHelper.eCommunity OndoCommunity,
                                          OndoXElementHelper.eVisibility OndoVisibility,
                                          out OndoXElementHelper.eCommunity OndoCommunityResult,
                                          out OndoXElementHelper.eVisibility OndoVisibilityResult)
        {
            // default values;
            OndoCommunityResult = OndoCommunity;
            OndoVisibilityResult = OndoVisibility;

            try
            {
                // Get app settings
                OndoStorageHandler handler = new OndoStorageHandler();
                AppSettings appsettings = handler.GetAppSettings();

                if (appsettings.OndoCommunity == null || appsettings.Visibility == null)
                {
                    // use same settings as Ondo
                    return;
                }
                else
                {
                    // User settings can only the same or lower than Ondo settings
                    if (((OndoXElementHelper.eCommunity)(Int32.Parse(appsettings.OndoCommunity))) < OndoCommunity)
                    {
                        OndoCommunityResult = ((OndoXElementHelper.eCommunity)(Int32.Parse(appsettings.OndoCommunity)));
                    }
                    // User settings can only the same or lower than Ondo settings
                    if (((OndoXElementHelper.eVisibility)(Int32.Parse(appsettings.Visibility))) < OndoVisibility)
                    {
                        OndoVisibilityResult = ((OndoXElementHelper.eVisibility)(Int32.Parse(appsettings.Visibility)));
                    }
                }
            }
            catch
            {
            }
        }

        public void GetVisibilitySettings(string OndoCommunity,
                                          string OndoVisibility,
                                          out string OndoCommunityResult,
                                          out string OndoVisibilityResult)
        {
            // default values;
            // default values;
            if (OndoCommunity == null)
                OndoCommunityResult = ((int)OndoXElementHelper.eCommunity.Group).ToString(); 
            else
                OndoCommunityResult = OndoCommunity;

            if (OndoVisibility == null)
                OndoVisibilityResult = ((int)OndoXElementHelper.eVisibility.NotVisible).ToString();
            else
                OndoVisibilityResult = OndoVisibility;

            try
            {
                // Get app settings
                OndoStorageHandler handler = new OndoStorageHandler();
                AppSettings appsettings = handler.GetAppSettings();

                if (appsettings.OndoCommunity == null || appsettings.Visibility == null)
                {
                    // use same settings as Ondo
                    return;
                }
                else
                {
                    // User settings can only the same or lower than Ondo settings
                    if (((OndoXElementHelper.eCommunity)(Int32.Parse(appsettings.OndoCommunity))) < ((OndoXElementHelper.eCommunity)(Int32.Parse(OndoCommunityResult))))
                    {
                        OndoCommunityResult = appsettings.OndoCommunity;
                    }
                    // User settings can only the same or lower than Ondo settings
                    if (((OndoXElementHelper.eVisibility)(Int32.Parse(appsettings.Visibility))) < ((OndoXElementHelper.eVisibility)(Int32.Parse(OndoVisibilityResult)))) 
                    {
                        OndoVisibilityResult = appsettings.Visibility;
                    }
                }
            }
            catch
            {
            }
        }
    }
}
