﻿using System;
using System.Xml;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;
using System.IO;
using System.Collections.Generic;
using SharedOndoLibrary.Data;
using System.Collections.ObjectModel;


#if(WINDOWS_PHONE)
using System.Windows.Media.Imaging;
using SharedOndoLibrary.Utilities;
using System.Collections.ObjectModel;
#endif

#if MONOTOUCH
using System.Text;
using MonoTouch.UIKit;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.ObjectModel;
#endif

#if __ANDROID__
using Android.Graphics;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

using AndroidOs = Android.OS;
#endif

namespace SharedOndoLibrary.Helpers
{
    public class OndoStorageHandler
    {

        public enum eOndoStorage
        {
            Pined = 1,
            Own = 2,
            Participate = 3,
            Pending = 4,
            MyOndos,
            Mine,
            Spotlight
        };

        private string ParseOndoType(eOndoStorage type)
        {
            string ondotype = "";
            if (type == eOndoStorage.Own)
            {
                ondotype = "ownaaltos.xml";
            }
            else if (type == eOndoStorage.Participate)
            {
                ondotype = "participateaaltos.xml";
            }
            else if (type == eOndoStorage.Pined)
            {
                ondotype = "pinedaaltos.xml";
            }
            else if (type == eOndoStorage.Pending)
            {
                ondotype = "pendingaaltos.xml";
            }
            else if (type == eOndoStorage.MyOndos)
            {
                ondotype = "myondos.xml";
            }
            else if (type == eOndoStorage.Mine)
            {
                ondotype = "mine.xml";
            }
            else if (type == eOndoStorage.Spotlight)
            {
                ondotype = "spotlight.xml";
            }
            return ondotype;
        }

        public void DeleteStoredFile(eOndoStorage type)
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile(ParseOndoType(type));
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void logEvent(string text)
        {

            OndoStorageHandler handler = new OndoStorageHandler();

            UserItemsData userData = handler.GetUserData();

               
            try
            {
                using (IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (!file.FileExists("EventLogger.txt"))
                    {
                       using (IsolatedStorageFileStream isoStream =
                       new IsolatedStorageFileStream("EventLogger.txt", FileMode.OpenOrCreate, file))
                       {
                            isoStream.Seek(0, SeekOrigin.End);
                            using (StreamWriter sw = new StreamWriter(isoStream))
                            {
                                sw.WriteLine("Username: " + userData.Username +'\n');

#if __ANDROID__
                                sw.WriteLine("SW version: " + "1.9.5" + '\n');
                                
                                if ((AndroidOs.Build.Manufacturer == null) || (AndroidOs.Build.Manufacturer == ""))
                                {
                                    sw.WriteLine("Manufacturer: " + "Unknown"+'\n');
                                }
                                else
                                {
                                    sw.WriteLine("Manufacturer: " + AndroidOs.Build.Manufacturer+'\n');
                                }
                                
                                
                                if ((AndroidOs.Build.Model == null) || (AndroidOs.Build.Model == ""))
                                {
                                    sw.WriteLine("Model: " + "Unknown"+'\n'); 
                                }
                                else
                                {
           
                                   sw.WriteLine("Model: " + AndroidOs.Build.Model+'\n');
                                }
#endif
                                sw.WriteLine(text+'\n');
                            }
                        }
                    }
                    else
                    {
                       using (IsolatedStorageFileStream isoStream =
                       new IsolatedStorageFileStream("EventLogger.txt", FileMode.Append, file))
                       {
                            isoStream.Seek(0, SeekOrigin.End);
                            using (StreamWriter sw = new StreamWriter(isoStream))
                            {
                               sw.WriteLine(text);
                            }
                        }
                    }
                }
            }
                       
            catch (Exception)
            {

            }
        }

        public string getEventLog()
        {
            try
            {
                string contents = null;
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists("EventLogger.txt"))
                    {
                        using (TextReader reader = new StreamReader(store.OpenFile("EventLogger.txt", FileMode.Open, FileAccess.Read, FileShare.None)))
                        {
                            contents = reader.ReadToEnd();
                        }
                       
                    }
                }
                if (contents != null)
                {
                    return contents;                  
                }
            }
            catch (Exception)
            {
                
            }
            return "";
        }

        public void deleteEventLog()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("EventLogger.txt");

                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public List<OndoData> GetListOfStoredOndos(eOndoStorage type)
        {
            List<OndoData> storedOndos = new List<OndoData>();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));

                        storedOndos = (List<OndoData>)serializer.Deserialize(stream);
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return storedOndos;
        }

        public List<OndoItem> GetListOfMyOndos(eOndoStorage type)
        {
            List<OndoItem> storedOndos = new List<OndoItem>();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoItem>));

                        storedOndos = (List<OndoItem>)serializer.Deserialize(stream);
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return storedOndos;
        }

        public ObservableCollection<MyOndoViewData> GetListOfStoredOndoItems(eOndoStorage type)
        {
            ObservableCollection<MyOndoViewData> storedOndos = new ObservableCollection<MyOndoViewData>();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Open, FileAccess.Read))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<MyOndoViewData>));

                        storedOndos = (ObservableCollection<MyOndoViewData>)serializer.Deserialize(stream);
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                storedOndos = null;
            }
            return storedOndos;
        }

        public void ClearReadFlagInStoredOndo(string ondoId)
        {
            List<OndoData> storedOndos = new List<OndoData>();
            List<OndoData> newStoredOndos = new List<OndoData>();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(eOndoStorage.MyOndos), FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));
                        storedOndos = (List<OndoData>)serializer.Deserialize(stream);
                        foreach (OndoData data in storedOndos)
                        {
                            if (data.OndoId == ondoId)
                            {
                                data.NewPosts = "0";
                            }
                            newStoredOndos.Add(data);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                SaveListOfOndos(newStoredOndos, eOndoStorage.MyOndos);
            }
            catch (Exception)
            {

            }
        }

        public void StoreOndo(string ondoId, OndoItem ondoData)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ondoId + ".xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(OndoItem));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, ondoData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        public bool GetStoredOndo(string ondoid, out OndoItem result)
        {
            result = new OndoItem();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ondoid + ".xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(OndoItem));

                        result = (OndoItem)serializer.Deserialize(stream);

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                return true;
            }
            catch (Exception)
            {
                // Not nessesary to log this, it's ok to returns false if Ondo isn't in storage
                //CrashLogger.ReportException(e, "Reading ondo XML in Isolated storage exception");
                return false;
            }
        }

        public OndoData GetStoredOndos(string ondoid, eOndoStorage type)
        {
            List<OndoData> storedAaltos = new List<OndoData>();

            OndoData ondo = new OndoData();
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));

                        storedAaltos = (List<OndoData>)serializer.Deserialize(stream);
                        foreach (OndoData d in storedAaltos)
                        {
                            if (d.OndoId == ondoid)
                            {
                                ondo = d;
                                break;
                            }
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
            return ondo;
        }

#if __ANDROID__
         public void StoreMapTagItems(List<MapItem> mapTagList)
         {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            List<MapItem> tmpItems = new List<MapItem>();

        
            for (int i=0;i<mapTagList.Count;i++)
            {
                if (mapTagList[i].Type != (int)OndoXElementHelper.eType.Clustered)
                {
                    tmpItems.Add(mapTagList[i]);
                }
            }



            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Ondo ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("maptags.xml", FileMode.Create, FileAccess.Write))
                    {
                        XmlSerializer serializer = new XmlSerializer(tmpItems.GetType());

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, tmpItems);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                throw new System.SystemException(e.Message);
            }
        }

         public List<MapItem> GetListOfMapTags()
         {
             List<MapItem> mapTags = new List<MapItem>();

             // Write to the Isolated Storage
             XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
             xmlWriterSettings.Indent = true;

             try
             {
                 using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                 {
                     using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("maptags.xml", FileMode.Open))
                     {
                         XmlSerializer serializer = new XmlSerializer(typeof(List<MapItem>));

                         mapTags = (List<MapItem>)serializer.Deserialize(stream);
                         stream.Close();
                     }
                     myIsolatedStorage.Dispose();
                 }
             }
             catch (Exception e)
             {
                 return null;
             }
             return mapTags;
         }

         public void deleteMapItems()
         {
             try
             {
                 using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                 {
                     myIsolatedStorage.DeleteFile("maptags.xml");

                     myIsolatedStorage.Dispose();
                 }
             }
             catch (Exception e)
             {

             }
         }

#endif

        public void StoreOndoItems(List<OndoItem> ondoList, eOndoStorage type)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Ondo ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Create, FileAccess.Write))
                    {
                        XmlSerializer serializer = new XmlSerializer(ondoList.GetType());

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, ondoList);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                throw new System.SystemException(e.Message);
            }
        }

        public void StoreOndoItems(ObservableCollection<MyOndoViewData> ondoList, eOndoStorage type)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Ondo ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Create, FileAccess.Write))
                    {
                        XmlSerializer serializer = new XmlSerializer(/*typeof(ObservableCollection<MyOndoViewData>)*/ondoList.GetType());

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, ondoList);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                throw new System.SystemException(e.Message);
            }
        }

        public bool AddOndoToListOfStoredOndos(OndoData Ondo, List<OndoData> storedAaltos, eOndoStorage type, bool newItem)
        {
            bool rc = false;

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));

                        if (newItem)
                            storedAaltos.Insert(0, Ondo);
                        else
                            storedAaltos.Add(Ondo);

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, storedAaltos);
                            rc = true;
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }

            return rc;
        }

        public bool AddOndoToListOfStoredOndos(OndoItem Ondo, List<OndoItem> storedAaltos, eOndoStorage type, bool newItem)
        {
            bool rc = false;

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoItem>));

                        if (newItem)
                            storedAaltos.Insert(0, Ondo);
                        else
                            storedAaltos.Add(Ondo);

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, storedAaltos);
                            rc = true;
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }

            return rc;
        }

        public void DeleteOndoEntryInStorage(string ondoid, eOndoStorage type)
        {
            List<OndoData> storedAaltos = new List<OndoData>();
            List<OndoData> newStoredAaltos = new List<OndoData>();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Open, FileAccess.ReadWrite))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));
                        storedAaltos = (List<OndoData>)serializer.Deserialize(stream);
                        foreach (OndoData data in storedAaltos)
                        {
                            if (data.OndoId != ondoid)
                            {
                                newStoredAaltos.Add(data);
                            }
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                SaveListOfOndos(newStoredAaltos, type);
            }
            catch (Exception)
            {

            }
        }

        public void DeleteOndoEntryInStorage(int ondoid, eOndoStorage type)
        {
            List<OndoItem> storedAaltos = new List<OndoItem>();
            List<OndoItem> newStoredAaltos = new List<OndoItem>();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Open, FileAccess.ReadWrite))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoItem>));
                        storedAaltos = (List<OndoItem>)serializer.Deserialize(stream);
                        foreach (OndoItem data in storedAaltos)
                        {
                            if (data.OndoId != ondoid)
                            {
                                newStoredAaltos.Add(data);
                            }
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                StoreOndoItems(newStoredAaltos, type);
            }
            catch (Exception)
            {

            }
        }

        public void UpdateOndoEntryInStorage(string ondoid, OndoData ondo, eOndoStorage type)
        {
            List<OndoData> storedAaltos = new List<OndoData>();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Open, FileAccess.ReadWrite))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));
                        storedAaltos = (List<OndoData>)serializer.Deserialize(stream);

                        int index = 0;
                        foreach (OndoData data in storedAaltos)
                        {
                            if (data.OndoId == ondoid)
                            {
                                storedAaltos.RemoveAt(index);
                                storedAaltos.Insert(index, ondo);
                                break;
                            }
                            index++;
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                SaveListOfOndos(storedAaltos, type);
            }
            catch (Exception e)
            {

            }
        }

        public void SaveListOfOndos(List<OndoData> Ondos, eOndoStorage type)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));
                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, Ondos);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        public AppSettings GetAppSettings()
        {
            AppSettings appsettings = new AppSettings(); ;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("settings.xml", FileMode.OpenOrCreate))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(AppSettings));
                        appsettings = (AppSettings)serializer.Deserialize(stream);
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
            return appsettings;
        }

        public void SaveAppSettings(AppSettings settings)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("settings.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(AppSettings));
                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, settings);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception exc)
            {
#if (WINDOWS_PHONE)
                    CrashLogger.ReportException(exc, "SaveAppSettings exception");
#endif
            }
        }

        public OndoData GetOndoById(string ondoid, eOndoStorage type)
        {
            List<OndoData> storedAaltos = new List<OndoData>();
            OndoData ondo = new OndoData();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(type), FileMode.Open, FileAccess.ReadWrite))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));
                        storedAaltos = (List<OndoData>)serializer.Deserialize(stream);
                        foreach (OndoData data in storedAaltos)
                        {
                            if (data.OndoId == ondoid)
                            {
                                ondo = data;
                                break;
                            }
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
            return ondo;
        }

        public void DeleteUsersettingsFile()
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    myIsolatedStorage.DeleteFile("usersettings.xml");
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void UserLogOut()
        {
            UserItemsData myreaduserdata = new UserItemsData();
            UserItemsData myloggedoutprofile = new UserItemsData();

            myreaduserdata = GetUserData();


            // Preserve only these 3 properties when logging out

#if MONOTOUCH
            myloggedoutprofile.useremail = myreaduserdata.useremail;
#else
            myloggedoutprofile.UserLogin = myreaduserdata.UserLogin;
#endif
            myloggedoutprofile.UserId = myreaduserdata.UserId;
            //myloggedoutprofile.UserDeviceHWID = myreaduserdata.UserDeviceHWID;

            // Restore this loggedout profile
            StoreUserData(myloggedoutprofile);

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("userimage.jpg");
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }

        }

        public void StoreUserData(UserItemsData userdata)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("usersettings.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(UserItemsData));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, userdata);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

#if MONOTOUCH
        public void StoreUserDataClean(UserItem userdata)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
 
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("usersettings1.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(UserItem));
 
                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, userdata);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
 
            }
        }

        public UserItem GetUserDataClean()
        {
            UserItem userdata = new UserItem();
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("usersettings1.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(UserItem));
 
                            userdata = (UserItem)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                //MessageBox.Show("Please enter your user data before participating in or sharing an Ondo");
            }
            return userdata;
        }
 
#endif

        public UserItemsData GetUserData()
        {
            UserItemsData userdata = new UserItemsData();
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("usersettings.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(UserItemsData));

                            userdata = (UserItemsData)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                //MessageBox.Show("Please enter your user data before participating in or sharing an Ondo");
            }
            return userdata;
        }

        public int GetFavoriteOndo()
        {
            int rc = 0;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("favoriteondo.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(int));

                            rc = (int)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                //MessageBox.Show("Please enter your user data before participating in or sharing an Ondo");
            }
            return rc;
        }

        public void StoreFavoriteOndo(int ondo)
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("favoriteondo.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(int));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, ondo);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        //void SaveInStorage(UserItemsData userdatatostore)
        //{
        //    // Write to the Isolated Storage
        //    XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
        //    xmlWriterSettings.Indent = true;

        //    try
        //    {
        //        using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
        //        {
        //            // Name of file should be Aalto ID
        //            using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("usersettings.xml", FileMode.Create))
        //            {
        //                XmlSerializer serializer = new XmlSerializer(typeof(OndoDots.Data.UserItemsData));

        //                using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
        //                {
        //                    serializer.Serialize(xmlWriter, userdatatostore);
        //                }
        //                stream.Close();
        //            }
        //            myIsolatedStorage.Dispose();
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //}



#if __ANDROID__NOTUSED_AT_THE_MOMENT

        public void StoreUserPicture(Bitmap element)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                element.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                using (var local = new IsolatedStorageFileStream("userimage.jpg", FileMode.Create, IsolatedStorageFile.GetUserStoreForApplication()))
                {
                    local.Write(stream.GetBuffer(), 0, stream.GetBuffer().Length);
                }
                stream.Close();
            }
        }


        public List<OndoData> GetListOfStoredOndos(eOndoStorage type, string path)
        {
            List<OndoData> storedAaltos = new List<OndoData>();

            var file = System.IO.Path.Combine(path, ParseOndoType(type));
            FileStream stream = new FileStream(file, FileMode.Open,  FileAccess.Read);

            
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));

                storedAaltos = (List<OndoData>)serializer.Deserialize(stream);
                stream.Close();
            
            }
            catch (Exception)
            {

            }
            return storedAaltos;
        }
/*
        public List<MyOndoList> GetMyStoredOndoList(string path)
        {

            List<MyOndoList> ondoData = new List<MyOndoList>();

            var file = System.IO.Path.Combine(path, "MyOndolist.xml");
            FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read);

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<MyOndoList>));

                ondoData = (List<MyOndoList>)serializer.Deserialize(stream);
                stream.Close();

            }
            catch (Exception)
            {

            }
            
            return ondoData;
        }


        public void StoreMyOndosList(List<MyOndoList> ondoList, string path)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            var file = System.IO.Path.Combine(path, "MyOndolist.xml");
            FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read);

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(List<MyOndoList>));
                using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                {
                    serializer.Serialize(xmlWriter, ondoList);
                }
                stream.Close();
            }
            catch (Exception e)
            {

            }
        
        }

*/
        public void CleanMyOndos(List<MyOndoList> ondolist, string path)
        {


            try
            {
               
                System.IO.File.Delete(System.IO.Path.Combine(path, "tmpuser.xml"));
                foreach (MyOndoList s in ondolist)
                {
                    System.IO.File.Delete(System.IO.Path.Combine(path, (s.OndoId + ".xml")));
  //                  System.IO.File.Delete(System.IO.Path.Combine(path, (s.OndoId + "OndoActivity" + ".xml")));
  //                  System.IO.File.Delete(System.IO.Path.Combine(path, (s.OndoId + "Initiator" + ".xml")));
                       
                }
/*
                string[] files = myIsolatedStorage.GetFileNames();

                foreach (string t in files)
                {
                    if (t.Contains("Comments"))
                    {
                        myIsolatedStorage.DeleteFile(t);
                    }
                }
*/
            }
            catch (Exception e)
            {

            }
            
  
         }

/*
        public void StoreUserData(UserItemsData userdata, string path)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            var file = System.IO.Path.Combine(path, "usersettings.xml");
            FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read);
         
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(UserItemsData));
                using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                {
                    serializer.Serialize(xmlWriter, userdata);
                }
                stream.Close();
            }
            catch (Exception e)
            {

            }

        }

        public UserItemsData GetUserData(string path)
        {
            UserItemsData userdata = new UserItemsData();


            var file = System.IO.Path.Combine(path, "usersettings.xml");
            FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read);

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(UserItemsData));

                userdata = (UserItemsData)serializer.Deserialize(stream);
                stream.Close();

            }
            catch (Exception)
            {

            }

            
            return userdata;
        }

*/




        public void SetOndoStorageViewToDirty(string path)
        {
            try
            {
                var file = System.IO.Path.Combine(path, "isDirty.txt");
                File.WriteAllText(file, "");
            }
            catch (Exception e)
            {

            }
        }

        public bool IsOndoStorageViewDirty(string path)
        {
            bool rc = false;
            try
            {
                var file = System.IO.Path.Combine(path, "isDirty.txt");
                if (File.Exists(file))
                {
                    rc = true;
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteOndoStorageViewToDirty(string path)
        {

            try
            {
                var file = System.IO.Path.Combine(path, "isDirty.txt");
                System.IO.File.Delete(file);

            }
            catch (Exception e)
            {

            }
        }


        public void SetOndoStorageViewToReload(string path)
        {
            try
            {
               var file = System.IO.Path.Combine(path, "reload.txt");
               File.WriteAllText(file, "");
            }
            catch (Exception e)
            {

            }
        }

        public bool ShouldOndoStorageViewReload(string path)
        {
            bool rc = false;
            try
            {
               var file = System.IO.Path.Combine(path, "reload.txt");
               if (File.Exists(file))
               {
                   rc = true;
               }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteOndoStorageReload(string path)
        {
          
            try
            {
                var file = System.IO.Path.Combine(path, "reload.txt");
                System.IO.File.Delete(file);
              
            }
            catch (Exception e)
            {

            }
        }

        public void SetOndoViewToReload(string path)
        {
            try
            {
               var file = System.IO.Path.Combine(path, "ondoreload.txt");
               File.WriteAllText(file, "");
            }
            catch (Exception e)
            {

            }
        }

        public bool ShouldOndoViewReload(string path)
        {
           bool rc = false;
            try
            {
               var file = System.IO.Path.Combine(path, "ondoreload.txt");
               if (File.Exists(file))
               {
                   rc = true;
               }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteOndoViewToReload(string path)
        {
            try
            {
                var file = System.IO.Path.Combine(path, "ondoreload.txt");
                System.IO.File.Delete(file);
              
            }
            catch (Exception e)
            {

            }
        }

        public void SetOndoStorageViewFullToReload(string path)
        {
            try
            {
                var file = System.IO.Path.Combine(path, "fullreload.txt");
                File.WriteAllText(file, "");
            }
            catch (Exception e)
            {

            }
        }

        public bool ShouldOndoStorageViewFullReload(string path)
        {
            bool rc = false;
            try
            {
                var file = System.IO.Path.Combine(path, "fullreload.txt");
                if (File.Exists(file))
                {
                    rc = true;
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteOndoStorageFullReload(string path)
        {
            try
            {
                var file = System.IO.Path.Combine(path, "fullreload.txt");
                System.IO.File.Delete(file);

            }
            catch (Exception e)
            {

            }
        }

        public bool DeleteMyOndoList(string path)
        {
            bool rc = false;
            try
            {
               var file = System.IO.Path.Combine(path, "MyOndolist.xml");
               System.IO.File.Delete(file);
               rc = true;
  
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteStoredOndos(eOndoStorage type, string path)
        {
            try
            {
                var file = System.IO.Path.Combine(path, ParseOndoType(type));
                System.IO.File.Delete(file);
            }
            catch (Exception e)
            {

            }
        }

        public void DeleteStoredFile(eOndoStorage type, string path)
        {
            try
            {
                var file = System.IO.Path.Combine(path, ParseOndoType(type));
                System.IO.File.Delete(file);
            }
            catch (Exception e)
            {

            }
        }

#else

#if __ANDROID__
        public void StoreUserPicture(Bitmap element)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                element.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                using (var local = new IsolatedStorageFileStream("userimage.jpg", FileMode.Create, IsolatedStorageFile.GetUserStoreForApplication()))
                {
                    local.Write(stream.GetBuffer(), 0, stream.GetBuffer().Length);
                }
                stream.Close();
            }
        }
        
#endif



#if MONOTOUCH
		public void StoreUserPicture(object sample)
			
		{
			
			XmlSerializer serializer = new XmlSerializer(typeof(object));
			
			string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "userimage.jpg");      
			Stream st = new FileStream(path, FileMode.OpenOrCreate);
			
			XmlWriter w = new XmlTextWriter(st, Encoding.UTF8);
			
			serializer.Serialize(w, sample);   
			
			st.Flush(); 
			
			st.Close();  
			
		}
#endif

#if WINDOWS_PHONE
        public void StoreUserPicture(WriteableBitmap element)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                int divider = 1;
                if (element.PixelHeight > 200)
                {
                    divider = (int)element.PixelHeight / 200;
                }
                element.SaveJpeg(stream, (int)element.PixelWidth / divider, (int)element.PixelHeight / divider, 0, 70);
                using (var local = new IsolatedStorageFileStream("userimage.jpg", FileMode.Create, IsolatedStorageFile.GetUserStoreForApplication()))
                {
                    local.Write(stream.GetBuffer(), 0, stream.GetBuffer().Length);
                }
                stream.Close();
            }
        }
#endif

        public List<MyOndoList> GetMyStoredOndoList()
        {
            List<MyOndoList> ondoData = null;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("MyOndolist.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(List<MyOndoList>));

                            ondoData = new List<MyOndoList>();
                            ondoData = (List<MyOndoList>)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return ondoData;
        }

        public void SetOndoStorageViewToDirty()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.OpenFile("isDirty.txt", FileMode.OpenOrCreate);
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public bool IsOndoStorageViewDirty()
        {
            bool rc = false;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (myIsolatedStorage.FileExists("isDirty.txt"))
                    {
                        rc = true;
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteOndoStorageViewToDirty()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("isDirty.txt");
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void SetOndoStorageViewToReload()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.OpenFile("reload.txt", FileMode.OpenOrCreate);
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public bool ShouldOndoStorageViewReload()
        {
            bool rc = false;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (myIsolatedStorage.FileExists("reload.txt"))
                    {
                        rc = true;
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteOndoStorageReload()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("reload.txt");
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void SetOndoViewToReload()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.OpenFile("ondoreload.txt", FileMode.OpenOrCreate);
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                throw new System.SystemException("Unexpected error ondoreload.txt");
            }
        }

        public void DeleteOndoViewToReload()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("ondoreload.txt");
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public bool ShouldOndoViewReload()
        {
            bool rc = false;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (myIsolatedStorage.FileExists("ondoreload.txt"))
                    {
                        rc = true;
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void SetOndoStorageViewFullToReload()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.OpenFile("fullreload.txt", FileMode.OpenOrCreate);
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public bool ShouldOndoStorageViewFullReload()
        {
            bool rc = false;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (myIsolatedStorage.FileExists("fullreload.txt"))
                    {
                        rc = true;
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteOndoStorageFullReload()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("fullreload.txt");
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public bool DeleteMyOndoList()
        {
            bool rc = false;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("MyOndolist.xml");
                    rc = true;
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void DeleteStoredOndos(eOndoStorage type)
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile(ParseOndoType(type));
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void StoreMyOndosList(List<MyOndoList> ondoList)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("MyOndolist.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<MyOndoList>));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, ondoList);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void CleanMyOndos(List<MyOndoList> ondolist)
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("tmpuser.xml");

                    foreach (MyOndoList s in ondolist)
                    {
                        myIsolatedStorage.DeleteFile(s.OndoId + ".xml");
                        myIsolatedStorage.DeleteFile(s.OndoId + "OndoActivity" + ".xml");
                        myIsolatedStorage.DeleteFile(s.OndoId + "Initiator" + ".xml");
                    }

                    string[] files = myIsolatedStorage.GetFileNames();

                    foreach (string t in files)
                    {
                        if (t.Contains("Comments"))
                        {
                            myIsolatedStorage.DeleteFile(t);
                        }
                    }

                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }



#endif



#if __ANDROID__ || MONOTOUCH 
        public void StoreCommentsData(ObservableCollection<CommentsData> commentsData, string foreignId)
#else
        public void StoreCommentsData(List<CommentsData> commentsData, string foreignId)
#endif
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(foreignId + "Comments.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<CommentsData>));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, commentsData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

#if __ANDROID__ || MONOTOUCH 
        public ObservableCollection<CommentsData> GetCommentsData(string foreignId)
        {
            ObservableCollection<CommentsData> commentsData = null;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(foreignId + "Comments.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(List<CommentsData>));

                            commentsData = new ObservableCollection<CommentsData>();
                            commentsData = (ObservableCollection<CommentsData>)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return commentsData;
        }
#else
        public List<CommentsData> GetCommentsData(string foreignId)
        {
            List<CommentsData> commentsData = null;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(foreignId + "Comments.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(List<CommentsData>));

                            commentsData = new List<CommentsData>();
                            commentsData = (List<CommentsData>)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return commentsData;
        }
#endif

        public void StoreOndoActivityData(List<ActivityItem> activityData, string foreignId)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(foreignId + "OndoActivity.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<ActivityItem>));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, activityData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public bool GetOndoActivityData(string foreignId, out List<ActivityItem> activityData)
        {
            activityData = new List<ActivityItem>();
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(foreignId + "OndoActivity.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(List<ActivityItem>));
                            
                            activityData = (List<ActivityItem>)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                return true;
            }
            catch (Exception e)
            {

            }
            return false;
        }

        // List of Ondos

        public void SetMyFirstUserSetupTime(DateTime time)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("MyUserTime.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(DateTime));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, time);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public DateTime GetMyFirstUserSetupTime()
        {
            DateTime rc = new DateTime();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("MyUserTime.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(DateTime));

                        rc = (DateTime)serializer.Deserialize(stream);
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }

        public void StoreOndoInitiatorData(OndoExchangeData initiatorData, string ondoId)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ondoId + "Initiator.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(OndoExchangeData));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, initiatorData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public OndoExchangeData GetOndoInitiatorData(string ondoId)
        {
            OndoExchangeData activityData = new OndoExchangeData();
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ondoId + "Initiator.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(OndoExchangeData));

                            activityData = (OndoExchangeData)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return activityData;
        }

        public void StoreTmpLotteryData(List<UserItemsData> lottery)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("lottery.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<UserItemsData>));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, lottery);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public List<UserItemsData> GetTmpLotteryData()
        {
            List<UserItemsData> lotteryData = new List<UserItemsData>();
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("lottery.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(List<UserItemsData>));

                            lotteryData = (List<UserItemsData>)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
            return lotteryData;
        }

        public void StoreUserIDtoLocalStorage(UserItemsData userdata, UserItemsData readuserdata, int result)
        {

            // Load the usersettings.xml file
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("usersettings.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Data.UserItemsData));

                        readuserdata = (Data.UserItemsData)serializer.Deserialize(stream);

                        readuserdata.UserId = result;

                        userdata.UserId = readuserdata.UserId;

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();

                    // Store the updated usersettings.xml file
                    StoreUserData(readuserdata);
                }
            }
            catch (Exception e)
            {

            }
        }

        public void StoreUserPicturetoLocalStorage(UserItemsData userdata, UserItemsData readuserdata, string result)
        {
            // Load the usersettings.xml file
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("usersettings.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Data.UserItemsData));

                        readuserdata = (Data.UserItemsData)serializer.Deserialize(stream);

                        readuserdata.userpicture = result;

                        userdata.UserId = readuserdata.UserId;

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();

                    // Store the updated usersettings.xml file
                    StoreUserData(readuserdata);
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public void SetMySpotlightOndos(string time)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("SpolightOndos.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(string));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, time);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public string GetMySpotlightOndos()
        {
            string rc = "";

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Aalto ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("SpolightOndos.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(string));

                        rc = (string)serializer.Deserialize(stream);
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return rc;
        }


        public bool AmITheOwnerOfThisOndo(string ondoid, string userid)
        {
            bool rc = false;

            List<OndoData> storedOndos = new List<OndoData>();

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ParseOndoType(eOndoStorage.MyOndos), FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<OndoData>));

                        storedOndos = (List<OndoData>)serializer.Deserialize(stream);

                        foreach (OndoData o in storedOndos)
                        {
                            if (o.OndoId == ondoid)
                            {
                                if (o.OndoInitiator == userid)
                                {
                                    rc = true;
                                }
                                break;
                            }
                        }

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }

            return rc;
        }

#if __ANDROID__ || MONOTOUCH 
        public void StoreActivityData(ObservableCollection<ActivityItem> activityData, string OndoId)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Ondo ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(OndoId + "Activity.xml", FileMode.Create, FileAccess.Write))
                    {
                        XmlSerializer serializer = new XmlSerializer(activityData.GetType());

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, activityData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
              
            }
        }

        public ObservableCollection<ActivityItem> GetActivityData(string ondoId)
        {
            ObservableCollection<ActivityItem> activityData = null;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ondoId + "Activity.xml", FileMode.Open, FileAccess.Read))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<ActivityItem>));

                            activityData = new ObservableCollection<ActivityItem>();
                            activityData = (ObservableCollection<ActivityItem>)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return activityData;
        }

      
#else
        public void StoreActivityData(List<ActivityItem> activityData, string OndoId)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // Name of file should be Ondo ID
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(OndoId + "Activity.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<ActivityItem>));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, activityData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

        public List<ActivityItem> GetActivityData(string ondoId)
        {
            List<ActivityItem> activityData = null;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile(ondoId + "Activity.xml", FileMode.Open))
                    {
                        {
                            XmlSerializer serializer = new XmlSerializer(typeof(List<ActivityItem>));

                            activityData = new List<ActivityItem>();
                            activityData = (List<ActivityItem>)serializer.Deserialize(stream);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
            return activityData;
        }


        public void StoreCurrentMapItems(List<MapItem> mapItems)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("ondopositions.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<MapItem>));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, mapItems);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        public List<MapItem> GetStoredMapItems()
        {
            List<MapItem> result = new List<MapItem>();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("ondopositions.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<MapItem>));

                        result = (List<MapItem>)serializer.Deserialize(stream);

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
            return result;
        }

        public void StoreTmpMapItems(List<MapItem> mapItems)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("tmpondopositions.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<MapItem>));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, mapItems);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        public List<MapItem> GetTmpMapItems()
        {
            List<MapItem> result = new List<MapItem>();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("tmpondopositions.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<MapItem>));

                        result = (List<MapItem>)serializer.Deserialize(stream);

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
            return result;
        }

        public void SetTmpMapItem(int ondoId, bool isMember)
        {
            List<MapItem> result = new List<MapItem>();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("tmpondopositions.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<MapItem>));

                        result = (List<MapItem>)serializer.Deserialize(stream);
                        foreach (MapItem m in result)
                        {
                            if (m.OndoId == ondoId)
                            {
                                m.Role = (int)SharedOndoLibrary.Helpers.OndoXElementHelper.eRole.Participant;
                                break;
                            }
                        }
                        stream.Close();

                        StoreTmpMapItems(result);
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        public void StoreCurrentMapPosition(MapItem mapItems)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("position.xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(MapItem));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, mapItems);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        public MapItem GetMyStoredPosition()
        {
            MapItem result = new MapItem();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("position.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(MapItem));

                        result = (MapItem)serializer.Deserialize(stream);

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
            return result;
        }

        public void DeleteStoredPositions()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile("ondopositions.xml");
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }

#endif

        public void deleteActivityList(string ondoId)
        {

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile(ondoId + "Activity.xml");

                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }
#if __ANDROID__
        public void StoreContent(Bitmap content, string url)
        {
            Regex rgx = new Regex("[^0-9]");
            url = rgx.Replace(url, "");
            
            using (MemoryStream stream = new MemoryStream())
            {
                content.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                using (var local = new IsolatedStorageFileStream(url + "Content.jpg", FileMode.Create, FileAccess.Write, IsolatedStorageFile.GetUserStoreForApplication()))
                {
                    local.Write(stream.GetBuffer(), 0, stream.GetBuffer().Length);
                }
                stream.Close();
            }
        }

        public Bitmap GetContent(string url)
        {
            Regex rgx = new Regex("[^0-9]");
            url = rgx.Replace(url, "");
            
            Bitmap content = null;
            try
            {
                using (IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = isolatedStorage.OpenFile(url + "Content.jpg", FileMode.Open, FileAccess.Read))
                    {
                        content = BitmapFactory.DecodeStream(stream);
                    }

                    isolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }

            return content;
         }

        public void deleteContent(string url)
        {
            Regex rgx = new Regex("[^0-9]");
            url = rgx.Replace(url, "");
            
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile(url + "Content.jpg");
 
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }


        public void DeleteAllContents()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                   
                    string[] files = myIsolatedStorage.GetFileNames();

                    foreach (string t in files)
                    {
                        if (t.Contains("Content"))
                        {
                            myIsolatedStorage.DeleteFile(t);
                        }
                    }

                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }


        public void StoreQR(Bitmap content, string ondoId)
        {
            
            using (MemoryStream stream = new MemoryStream())
            {
                content.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                using (var local = new IsolatedStorageFileStream(ondoId + "QRcode.jpg", FileMode.Create, FileAccess.Write, IsolatedStorageFile.GetUserStoreForApplication()))
                {
                    local.Write(stream.GetBuffer(), 0, stream.GetBuffer().Length);
                }
                stream.Close();
            }
        }

        public Bitmap GetQR(string ondoId)
        {
            
            Bitmap content = null;
            try
            {
                using (IsolatedStorageFile isolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = isolatedStorage.OpenFile(ondoId + "QRcode.jpg", FileMode.Open, FileAccess.Read))
                    {
                        content = BitmapFactory.DecodeStream(stream);
                    }

                    isolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }

            return content;
        }

        public void deleteQR(string ondoId)
        {
            
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    myIsolatedStorage.DeleteFile(ondoId + "QRcode.jpg");

                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }


        public void DeleteAllQR()
        {
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {

                    string[] files = myIsolatedStorage.GetFileNames();

                    foreach (string t in files)
                    {
                        if (t.Contains("QRcode"))
                        {
                            myIsolatedStorage.DeleteFile(t);
                        }
                    }

                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }
        }
#endif


     

        #region wallet
        public void StoreWallet(List<WalletItem> walletList)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("wallet.xml", FileMode.Create, FileAccess.Write))
                    {
                        XmlSerializer serializer = new XmlSerializer(walletList.GetType());

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, walletList);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                throw new System.SystemException(e.Message);
            }
        }


        public List<WalletItem> GetWallet()
        {
            List<WalletItem> items = new List<WalletItem>();

            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("wallet.xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(List<WalletItem>));

                        items = (List<WalletItem>)serializer.Deserialize(stream);
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {
                return null;
            }
            return items;
        }

        public void DeleteWallet()
        {
           
            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {

                    string[] files = myIsolatedStorage.GetFileNames();

                    foreach (string t in files)
                    {
                        if (t.Contains("wallet"))
                        {
                            myIsolatedStorage.DeleteFile(t);
                        }
                    }

                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception e)
            {

            }


        }


#if __ACTIVE__

        public void StoreNetsItem(string cardno, string benefitId, BenefitProgramNetsItem cardData)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("wallet"+cardno + "_" + benefitId + ".xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(BenefitProgramNetsItem));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, cardData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }



        public bool GetNetsItem(string cardno, string benefitId, out BenefitProgramNetsItem result)
        {
            result = new BenefitProgramNetsItem();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("wallet"+ cardno + "_" + benefitId + ".xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(BenefitProgramNetsItem));

                        result = (BenefitProgramNetsItem)serializer.Deserialize(stream);

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                return true;
            }
            catch (Exception)
            {
                // Not nessesary to log this, it's ok to returns false if Ondo isn't in storage
                //CrashLogger.ReportException(e, "Reading ondo XML in Isolated storage exception");
                return false;
            }
        }




        public void StoreBenefitProgramTransactionItem(string benefitId, BenefitProgramTransactionItem cardData)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("wallet" + benefitId + ".xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(BenefitProgramTransactionItem));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, cardData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }



        public bool GetBenefitProgramTransactionItem(string benefitId, out BenefitProgramTransactionItem result)
        {
            result = new BenefitProgramTransactionItem();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("wallet" + benefitId + ".xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(BenefitProgramTransactionItem));

                        result = (BenefitProgramTransactionItem)serializer.Deserialize(stream);

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                return true;
            }
            catch (Exception)
            {
                // Not nessesary to log this, it's ok to returns false if Ondo isn't in storage
                //CrashLogger.ReportException(e, "Reading ondo XML in Isolated storage exception");
                return false;
            }
        }

#endif


        public void StoreTransactionItem<T>(string cardno, string benefitId, T cardData)
        {
            // Write to the Isolated Storage
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("wallet" + cardno + "_" + benefitId + ".xml", FileMode.Create))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(T));

                        using (XmlWriter xmlWriter = XmlWriter.Create(stream, xmlWriterSettings))
                        {
                            serializer.Serialize(xmlWriter, cardData);
                        }
                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
            }
            catch (Exception)
            {

            }
        }

        public bool GetTransactionItem<T>(string cardno, string benefitId, out T result) where T : new()
        {
            result = new T();

            try
            {
                using (IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream stream = myIsolatedStorage.OpenFile("wallet" + cardno + "_" + benefitId + ".xml", FileMode.Open))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(T));

                        result = (T)serializer.Deserialize(stream);

                        stream.Close();
                    }
                    myIsolatedStorage.Dispose();
                }
                return true;
            }
            catch (Exception)
            {
                // Not nessesary to log this, it's ok to returns false if Ondo isn't in storage
                //CrashLogger.ReportException(e, "Reading ondo XML in Isolated storage exception");
                return false;
            }
        }


        #endregion

    }
}
