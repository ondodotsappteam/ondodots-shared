﻿using System;
using System.Windows;
using System.IO;
using System.IO.IsolatedStorage;
#if (WINDOWS_PHONE)
using Microsoft.Phone.Tasks;
#endif
using SharedOndoLibrary.Helpers;
using SharedOndoLibrary.Data;

namespace SharedOndoLibrary.Utilities
{
    public class CrashLogger
    {

#if (WINDOWS_PHONE)

        const string filename = "CrashLog.txt";

        public static void ReportException(Exception ex, string extra)
        {
           
            OndoStorageHandler handler = new OndoStorageHandler();

            UserItemsData userData = handler.GetUserData();

            try
            {
                using (IsolatedStorageFile isoFile = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    using (IsolatedStorageFileStream isoStream =
                    new IsolatedStorageFileStream(filename, FileMode.OpenOrCreate, isoFile))
                    {
                        isoStream.Seek(0, SeekOrigin.End);
                        using (StreamWriter sw = new StreamWriter(isoStream))
                        {
                            sw.WriteLine("Reason: " + extra);
                            sw.WriteLine("Message: " + ex.Message);
                            sw.WriteLine("StackTrace:");
                            sw.WriteLine(ex.StackTrace);

                            if (userData.Username != null)
                            {
                                sw.WriteLine("Username: " + userData.Username);
                            }
                            else
                            {
                                sw.WriteLine("Username unknown");
                            }

                            sw.WriteLine("----------------------------------------------------------------------------------------------------------");
                        }
                    }
                }
            }

            catch (Exception)
            {

            }
        }

        public static void CheckForPreviousException()
        {
                
            try
            {
                string contents = null;
                using (var store = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    if (store.FileExists(filename))
                    {
                        using (TextReader reader = new StreamReader(store.OpenFile(filename, FileMode.Open, FileAccess.Read, FileShare.None)))
                        {
                            contents = reader.ReadToEnd();
                        }
                        SafeDeleteFile(store);
                    }
                }
                if (contents != null)
                {
                    OndoStorageHandler handler = new OndoStorageHandler();
               
                    if (MessageBox.Show("A problem occurred the last time you ran this application. Would you like to send an email to report it?",
                                         "Problem Report", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                    {

                        UserItemsData userData = handler.GetUserData();
                                        
                        EmailComposeTask email = new EmailComposeTask();
                        email.To = "crash@ondodots.com";
                        email.Subject = "Your App auto-generated a problem report";
                        email.Body = contents; 
                        SafeDeleteFile(IsolatedStorageFile.GetUserStoreForApplication()); 
                        email.Show();
                    }  
                }
            }
            catch (Exception)
            {
         
            }
            finally
            {
                SafeDeleteFile(IsolatedStorageFile.GetUserStoreForApplication());
            }
        }
 
        private static void SafeDeleteFile(IsolatedStorageFile store)
        {
            try
            {
                store.DeleteFile(filename);
            }
            catch (Exception ex)
            {

                string str = ex.Message.ToString();
            }
        }
        
#endif
    }
}
 



