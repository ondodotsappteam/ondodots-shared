﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    /*
     - <npsposts xmlns="">
- <Post>
  <Voting>9</Voting> 
  <Text>Greetings from IOS :-)</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>9</Voting> 
  <Text>asdfdsaf</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>9</Voting> 
  <Text>Test Test Test Test igen igen igen...</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>9</Voting> 
  <Text>Tror sørme det virker nu</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>10</Voting> 
  <Text>Natuurlijk bent U ook van harte welkom op de wedstrijddagen van onze senioren, dames, veteranen of de jeugdteams. Kortom volop strijd, passie en vertier op ons mooie sportpark \"de Heikamp\" in Asten - Heusden</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>9</Voting> 
  <Text>Test</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>10</Voting> 
  <Text>Camera</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>10</Voting> 
  <Text>Minus billede</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>10</Voting> 
  <Text>Hvid Påske</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>9</Voting> 
  <Text>Ole</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>9</Voting> 
  <Text>Ole5</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>10</Voting> 
  <Text>15MB scaled down</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>10</Voting> 
  <Text>Ok</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
- <Post>
  <Voting>10</Voting> 
  <Text>Hr Hørmer er nu med i Ondoen</Text> 
  <Picture>http://ondodotsservice.blob.core.windows.net/profile/527b3168-9600-4852-a26e-6fcd004122e3.jpg</Picture> 
  <UserName>Hr Hørmer</UserName> 
  <NumberOfComments>0</NumberOfComments> 
  </Post>
  </npsposts>
     * 
     */

    public class NPSPostItem
    {
        public string Voting
        {
            get;
            set;
        }

        public string Text
        {
            get;
            set;
        }

        public string Time
        {
            get;
            set;
        }

        public string Picture
        {
            get;
            set;
        }

        public string CommentId
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public string NumberOfComments
        {
            get;
            set;
        }

    }
}
