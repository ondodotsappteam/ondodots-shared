﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
           /*
         <statistics xmlns="">
          <lasttime>
            <total>6</total>
            <new>17</new>
            <left>0</left>
            <date>15. April</date>
          </lasttime>
          <now>
            <total>7</total>
            <new>8</new>
            <left>1</left>
            <date>15. May</date>
          </now>
          <Statistic>
            <Detract>9</Detract>
            <Passive>4</Passive>
            <Promo>7</Promo>
          </Statistic>
        </statistics>
        */

    public class OndoOverviewItem
    {
        public string PreviousTotal
        {
            get;
            set;
        }

        public string PreviousNew
        {
            get;
            set;
        }

        public string PreviousLeft
        {
            get;
            set;
        }

        public string PreviousDate
        {
            get;
            set;
        }

        public string NowTotal
        {
            get;
            set;
        }

        public string NowNew
        {
            get;
            set;
        }

        public string NowLeft
        {
            get;
            set;
        }

        public string NowDate
        {
            get;
            set;
        }

        public string Detract
        {
            get;
            set;
        }

        public string Passive
        {
            get;
            set;
        }

        public string Promo
        {
            get;
            set;
        }
    }
}
