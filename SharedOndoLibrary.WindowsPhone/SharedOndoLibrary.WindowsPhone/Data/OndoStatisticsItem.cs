﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SharedOndoLibrary.Data
{
    public class OndoStatisticsItem
    {
        public string Detract
        {
            get;
            set;
        }

        public string Passive
        {
            get;
            set;
        }

        public string Promo
        {
            get;
            set;
        }

        public string Unit
        {
            get;
            set;
        }
    }
}
